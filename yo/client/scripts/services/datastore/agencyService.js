(function () {
'use strict'
var app = angular.module('app');
app.service('agencyService', ['$http', function($http){
	return{
		addAgency : function (agency){
			return $http({
				method:'POST',
				url: '/api/secured/agency/add',
				data: agency
			})
		},
		updateAgency : function (agencyId, agency){
			return $http({
				method:'POST',
				url: '/api/secured/agency/update/' + agencyId,
				data: agency
			})
		},
		getAgencyById : function (agencyId){
			return $http({
				method:'GET',
				url: '/api/secured/agency/id/' + agencyId
			})
		},
		listAgency : function (){
			return $http({
				method:'GET',
				url: '/api/secured/agency/list'
			})
		},
		deleteAgency : function (agencyId){
			return $http({
				method:'DELETE',
				url: '/api/secured/agency/' + agencyId
			})
		}
	}
}])
})();