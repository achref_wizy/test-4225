(function () {
'use strict'
var app = angular.module('app');
app.service('datastoreMembership', ['$http', function ($http){
	return{
		getUserList: function(domainId ){
	    	return $http({
		    	url: '/secured/api/domains/memberships/list/'+domainId
	    	})
	    },
	    addUser:function(domainId, Email, role, name){
	    	return $http({
		    	method:'POST',
		    	url:'/secured/api/users/invite/user/'+ Email+'/domain/'+domainId+'/role/'+role+'/name/'+name
	    	})

	    },
	    getUser:function(Email){
	    	return $http({

		    	url:'/secured/api/users/'+Email
	    	})

	    },
	    deleteUser:function(domainId, email){
	    	return $http({
		    	method:'GET',
		    	url:'/secured/api/domains/delete/domain/'+domainId+'/user/'+email
	    	})
	    },
	    updateUser:function(domainId, payload){

	    	return $http({
		    	method:'POST',
		    	url:'/secured/api/domains/memberships/update/'+domainId,
		    	data: payload
	    	})
	    },
	    deleteAccount:function(userId){
	    	return $http({
		    	method:'DELETE',
		    	url:'/secured/api/domains/account/'+userId
	    	})
	    },

        updatedDomaine:function(domainId, payload){

	    	return $http({
		    	method:'POST',
		    	url:'/secured/api/domains/update/'+domainId,
		    	data: payload
	    	})
		},


}
}])
})();
