(function () {
'use strict'
var app = angular.module('app');
app.service('userService', ['$http', function($http){
	return{
		getUserList: function(){
			return $http({
	    		method:'GET',
		    	url: '/api/secured/users'
	    	})

		},
		addUser: function(payload){
			return $http({
	    		method:'POST',
		    	url: '/api/secured/users/invite',
		    	data: payload
	    	})

		},
	    addUserNut:function(payload, id, clinicID){
	    	return $http({
		    	method:'POST',
		    	url:'/api/secured/users/invite/admin/' + id + '/clinic/' + clinicID,
		    	data: payload
	    	})

	    },
		updateUser: function(payload){
			return $http({
	    		method:'POST',
		    	url: '/api/secured/users/update',
		    	data: payload
	    	})
		},
		deleteUser: function(id){
			return $http({
	    		method:'DELETE',
		    	url: '/api/secured/users/id/' + id
	    	})

		}
	}
}])
})();