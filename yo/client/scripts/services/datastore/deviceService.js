(function () {
'use strict'
var app = angular.module('app');
app.service('deviceService', ['$http', function($http){
	return{
		saveOrUpdateDevice: function(device){
			return $http({
	    		method:'POST',
		    	url: '/api/secured/device/add',
		    	data: device
	    	})
		},
		getDevice : function(deviceId){
			return $http({
	    		method:'GET',
		    	url: '/api/secured/device/id/' + deviceId
	    	})
		},
		listDevices : function(){
			return $http({
				method:'GET',
				url: '/api/secured/device' 
			})
		},
		listDevicesPagination : function(max, pagetoken){
			return $http({
				method:'GET',
				url: '/api/secured/device/pagination?maxResults=' + max + '&pageToken='+ pagetoken 
			})
		},
		listDevicesByFilter : function(nameField, valueField){
			return $http({
				method:'GET',
				url: '/api/secured/device/filter/name/' + nameField + '/value/' + valueField 
			})
		},
		countAllDevices : function(){
			return $http({
				method:'GET',
				url: '/api/secured/device/countall'
			})
		},
		patchDevice : function(device, deviceId){
			return $http({
				method:'POST',
				url: '/api/secured/device/update/' + deviceId,
				data: device 
			})
		},
		deleteDevice : function (deviceId){
			return $http({
				method:'DELETE',
				url: '/api/secured/device/id/' + deviceId
			})
		},
		changeDeviceListStatus:function (status, deviceIds){
			return $http({
				method:'POST',
				url: '/api/secured/device/list/patch/status/' + status,
				data: deviceIds
			})
		}
	}
}])
})();