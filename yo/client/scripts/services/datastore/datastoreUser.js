(function() {
    'use strict'

    angular.module('app')
        .service('datastoreUser', ['$http', function($http) {
            return {
                LoginUser: function(email) {
                    return $http({
                        method: 'GET',
                        url: '/api/secured/users/login/' + email
                    });
                },
                chekUserExist: function(email) {
                    return $http({
                        method: 'GET',
                        url: '/secured/api/users/' + email
                    });
                },
                createTrailAccount: function(email, name) {
                    return $http({
                        method: 'GET',
                        url: '/secured/api/users/create/freetrial/'+ email +'/freemium/'+ name
                    });
                }

            }
        }])
})();