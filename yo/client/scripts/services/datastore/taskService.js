(function () {
'use strict'
var app = angular.module('app');
app.service('taskService', ['$http', function($http){
	return{
		getTaskById: function(taskId){
			return $http({
	    		method:'GET',
		    	url: '/api/secured/task' + taskId
	    	})
		},
		UpdateTaskStatusById : function(taskId, status){
			return $http({
	    		method:'GET',
		    	url: '/id/' + taskId + '/status/' + status
	    	})
		},
		addNewTask : function(deviceId, action, msg){
			return $http({
				method:'POST',
				url: '/api/secured/task/add/id/' + deviceId + '/' + action,
				data: msg
			})
		},
		saveOrUpdateTask : function (task){
			return $http({
				method:'POST',
				url: '/api/secured/task/',
				data: task
			})
		},
		listTask : function (){
			return $http({
				method:'GET',
				url: '/api/secured/task'
			})
		},
		deleteTask : function (taskId, deviceId){
			return $http({
				method:'DELETE',
				url: '/api/secured/task/id/' + taskId + '/device/' + deviceId
			})
		},
		sendTaskToMultiDevicesByRegistrationIds : function (action, payload){
			return $http({
				method:'POST',
				url: '/api/secured/task/add/ids/' + action,
				data: payload
			})
		},
		addTaskToAllDevice : function (action, msg){
			return $http({
				method:'POST',
				url: '/api/secured/task/add/all/' + action,
				data: msg
			})
		},
		GetAllTaskByDeviceIMEI : function (imei){
			return $http({
				method:'GET',
				url: '/api/secured/task/imei/' + imei
			})
		}
	}
}])
})();