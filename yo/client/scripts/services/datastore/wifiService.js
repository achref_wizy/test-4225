(function () {
'use strict'
var app = angular.module('app');
app.service('wifiService', ['$http', function($http){
	return{
		addWifi : function (wifi){
			return $http({
				method:'POST',
				url: '/api/secured/network/wifi/add',
				data: wifi
			})
		},
		updateWifi : function (wifiId, wifi){
			return $http({
				method:'POST',
				url: '/api/secured/network/wifi/update/' + wifiId,
				data: wifi
			})
		},
		getWifiById : function (wifiId){
			return $http({
				method:'GET',
				url: '/api/secured/network/wifi/' + wifiId
			})
		},
		listAllWifi : function (){
			return $http({
				method:'GET',
				url: '/api/secured/network/list'
			})
		},
		listWifiByAgencyId : function (agencyId){
			return $http({
				method:'GET',
				url: '/api/secured/network/list/agency/' + agencyId
			})
		},
		deleteWifi : function (wifiId){
			return $http({
				method:'DELETE',
				url: '/api/secured/network/' + wifiId
			})
		}
	}
}])
})();