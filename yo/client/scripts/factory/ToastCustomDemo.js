(function() {
    'use strict';
    angular.module('app')
        .controller('ToastCustomDemo', ['$scope', '$mdToast', 'message', 'myClass', function ($scope, $mdToast, message, myClass) {
            $scope.message = message;
            $scope.myClass = myClass;
        }])
})();