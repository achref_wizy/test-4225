(function () {
  'use strict';

  angular.module('app')
  .factory('userSession', function () {

   var user="";
   var domain="";
   var patientId="";
   var exist="";
   var domainMembership="";
   var patientList =[];
   var imageUrl="";

   return {
    setImage: function (imageUr) {
      imageUrl = imageUr;
    },
    getImage: function () {
      return imageUrl;
    },
    setDomainMembershipJson: function (domainMembershipJsoni) {

      domainMembership = domainMembershipJsoni;
    },
    getDomainMembershipJson: function () {

      return domainMembership;
    },
    setExist: function (userObj) {

      exist = userObj;
    },
    getExist: function () {

      return exist;
    },
    setUser: function (userObj) {

      user = userObj;
    },
    getUser: function () {

      return user;
    },
    setDomain: function (domainObj) {

      domain= domainObj;

    },
    getDomain: function () {

      return domain;
    },
    setPatientId: function (patientObj) {

      patientId = patientObj;
    },
    getPatientId: function () {

      return patientId;
    },
    setPatientList: function(patientsList){
      patientList = projectsList;
    },
    getPatientList: function(){
      return patientList;
    }

  }

})
})();
