(function () {
'use strict';
angular.module('app')

.factory ('sandbox', ['$mdToast', '$rootScope', function ($mdToast, $rootScope){
	return{
		showSimpleToast: function (message, delay, myClass) {
			
	        $mdToast.show({
                templateUrl: 'views/templates/toast-template.html',
                controller:  'ToastCustomDemo',
               	position:    'bottom left',
                hideDelay:    delay,
                locals:{
                	'message': message,
                	'myClass':   myClass
                }
	                
	        });
	        $rootScope.showProgressBar = false;
        },
		convertRole:function(role){
			if(role == "ADMIN"){
				return "Admin";
			}else if(role == "MANAGER"){
				return "Admin";
			}else if(role == "USER"){
				return "End-User";
			}else{
				return "Nutritionist";
			}
		},
		convertPlan:function(Status,Plan){
				return "Free trial";
		}
	}

}])
})(); 
