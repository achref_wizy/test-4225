(function () {
  'use strict';
  angular.module('app')
  .controller('agencyAddCtrl', ['$scope', '$filter', '$rootScope', '$q', '$timeout', 'userService', '$location', 'userSession', 'sandbox', '$mdDialog', '$stateParams', 'agencyService', function ($scope, $filter, $rootScope, $q, $timeout, userService, $location, userSession, sandbox, $mdDialog, $stateParams, agencyService){
        
          
        var init;
        
        var agency = {};
        $scope.animationsEnabled = true;
        
       
        //ADD AGENCY
        $scope.addAgency = function(agency){
            agencyService.addAgency(agency).then(function(result){
                if(result.data == "BAD_REQUEST"){
                    sandbox.showSimpleToast("Code agence Null",3000,'error-toast');
                }
                if(result.data == "FOUND"){
                    sandbox.showSimpleToast("Agence existe déjà",3000,'error-toast');
                }
                if(result.data == "OK"){
                    sandbox.showSimpleToast("Agence ajoutée",3000,'success-toast');
                    $location.path('/agencyList');
                }
                if(result.data == "INTERNAL_SERVER_ERROR"){
                    sandbox.showSimpleToast("Erreur",3000,'error-toast');
                }
            });

        }        
       
        $scope.reset = function(device){
            $scope.device = {};
        }

        function select(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        };

        function onFilterChange() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        function onNumPerPageChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        function onOrderChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        function search() {
            $scope.filteredStores = $filter('filter')($scope.treatmentList, $scope.searchKeywords);
            return $scope.onFilterChange();
        };

        function order(rowName) {
            if ($scope.row === rowName) {
            return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.treatmentList, rowName);
            return $scope.onOrderChange();
        };

        init = function() {
            $scope.search();
            return $scope.select($scope.currentPage);
        };

  }])
})();