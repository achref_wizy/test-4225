(function () {
  'use strict';
  angular.module('app')
  .controller('tasksListCtrl', ['$scope', '$filter', '$rootScope', '$q', '$timeout', 'userService', '$location', 'userSession', 'sandbox', '$mdDialog', 'deviceService', 'taskService', function ($scope, $filter, $rootScope, $q, $timeout, userService, $location, userSession, sandbox, $mdDialog, deviceService, taskService){

        var init;
        getTasks();

        $scope.sortType     = 'status'; // set the default sort type
        $scope.sortReverse  = false;  // set the default sort order
 
        $scope.searchKeywords = '';
        $scope.filteredStores = [];
        $scope.row = '';
        $scope.select = select;
        $scope.onFilterChange = onFilterChange;
        $scope.onNumPerPageChange = onNumPerPageChange;
        $scope.onOrderChange = onOrderChange;
        $scope.search = search;
        $scope.order = order;
        $scope.numPerPageOpt = [3, 5, 10, 20];
        $scope.numPerPage = $scope.numPerPageOpt[2];
        $scope.currentPage = 1;
        $scope.currentPage = [];
        $scope.animationsEnabled = true;

        $scope.getTasksRefresh = function(){
            getTasks();
        }

        function getTasks(){
            taskService.listTask().then(function(result){
                if(result.status == 200){
                    for (var i=0; i<result.data.length; i++){
                        if(result.data[i].action == "block"){
                            result.data[i].action  = "Bloquer";
                        }else if(result.data[i].action == "locate"){
                            result.data[i].action  = "Localiser";
                        }else if(result.data[i].action == "update"){
                            result.data[i].action  = "Mettre à jour";
                        }else if(result.data[i].action == "message"){
                            result.data[i].action  = "Message";
                        }else if(result.data[i].action == "lock"){
                            result.data[i].action  = "Fermer";
                        }

                        if(result.data[i].status == "done"){
                            result.data[i].status  = "Terminée";
                        }else if(result.data[i].status == "requested"){
                            result.data[i].status  = "Demandée";
                        }else if(result.data[i].status == "taken"){
                            result.data[i].status  = "Poussée";
                        }
                    }
                    $scope.tasksList = result.data;
                }
            })
        }

        $scope.deleteTask = function(task) {
            console.log(task)
            taskService.deleteTask(task.id, task.device).then(function(result){
                if(result.status == 200){
                    getTasks();
                    sandbox.showSimpleToast("Task deleted",3000,'success-toast');
                }
            })
        };

        function select(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        };

        function onFilterChange() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        function onNumPerPageChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        function onOrderChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        function search() {
            $scope.filteredStores = $filter('filter')($scope.treatmentList, $scope.searchKeywords);
            return $scope.onFilterChange();
        };

        function order(rowName) {
            if ($scope.row === rowName) {
            return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.treatmentList, rowName);
            return $scope.onOrderChange();
        };

        init = function() {
            $scope.search();
            return $scope.select($scope.currentPage);
        };

  }])
})();