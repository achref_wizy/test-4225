(function () {
  'use strict';
  angular.module('app')
  .controller('agencyCtrl', ['$scope', '$rootScope', '$q', '$timeout', 'userService', '$location', '$mdToast', 'sandbox', '$mdDialog', 'agencyService', function ($scope, $rootScope, $q, $timeout, userService, $location, $mdToast, sandbox, $mdDialog, agencyService){
     
        $scope.progressCircularUpload = false;
        $scope.paramFile = false;

        getAgencyList();

        $scope.csv = {
                content: null,
                header: true,
                headerVisible: true,
                separator: ',',
                separatorVisible: true,
                result: null,
                encoding: 'ISO-8859-1',
                encodingVisible: true,
                accept:".csv"
        };

        //GET AGENCY LIST
        function getAgencyList(){
            agencyService.listAgency().then(function(result){
                if(result.status == 200){
                    $scope.agencyList = result.data;
                }
            })
        }

        //ADD NEW AGENCY
        $scope.addAgency = function(){
            $location.path('/agencyAdd');
        }
        
        //DELETE AGENCY
        $scope.deleteAgency = function(index){
            agencyService.deleteAgency(index).then(function(result){
                if(result.status == 200){
                    sandbox.showSimpleToast("Agence supprimée", 3000, 'success-toast');
                    getAgencyList(); 
                }
            })
        }

        //REDIRECT TO AGENCY
        $scope.goToAgencyDetails = function(agencyId){
            $location.path('/agencyDetails/' + agencyId + '/update');
        }
           
        $scope.importConfigFile = function() {
                $scope.progressCircularUpload = true;
                if ($scope.csv.result != undefined && $scope.csv.result != null) {

                    configResource.importConfigFile($scope.csv.result).then(function(result) {
                        $scope.paramFile = true;
                        sandbox.showSimpleToast("Configuration File successfully updated", 3000, 'success-toast'); 
                        $rootScope.displaymyprogressbar = false;
                        $scope.progressCircularUpload = false;
                        $scope.csvResult = result.data.genotypeList;
                        getAgencyList();

                    });
                } else {
                    console.log("error");
                }
        }


        
  }])
})();