(function () {
'use strict';

angular.module('app')
.controller('userManagementCtrl', ['$timeout','$scope', '$filter', '$mdDialog','$log' , 'userService', 'userSession', 'sandbox', '$rootScope', function ($timeout,$scope,$filter,$mdDialog, $log, userService, userSession,sandbox, $rootScope) {
    var init;
    $scope.users = "";
    getUserList();
    function getUserList(){
        //$rootScope.showProgressBar = true
        userService.getUserList().then(function(result){
            console.log(result)
            if(result.status == 200){
                console.log(result)
                $scope.users = result.data;
            }else{
                sandbox.showSimpleToast('An error occured while loading data',3000,'error-toast');
            }
            init();
        }),function(error){
            sandbox.showSimpleToast('An error occured while loading data',3000,'error-toast');
        }
    }

    $scope.searchKeywords = '';
    $scope.filteredUsers = [];
    $scope.row = '';
    $scope.select = select;
    $scope.onFilterChange = onFilterChange;
    $scope.onNumPerPageChange = onNumPerPageChange;
    $scope.onOrderChange = onOrderChange;
    $scope.search = search;
    $scope.order = order;
    $scope.numPerPageOpt = [3, 5, 10, 20];
    $scope.numPerPage = $scope.numPerPageOpt[2];
    $scope.currentPage = 1;
    $scope.currentPage = [];
    $scope.animationsEnabled = true;

    $scope.addUser = function () {

        var modalInstance = $mdDialog.show({
            templateUrl: 'views/modals/addUsers.html',
            controller: 'addUserController'
        });

        modalInstance.then(function(newuser) {
            addUsertoDb(newuser[0], newuser[1]);
        }, function (error) {
        });
    };

    function addUsertoDb(newuser, clinicID){

        userService.addUser(newuser).then(function(result){
            if(result.data == "OK"){
                sandbox.showSimpleToast('The user '+ newuser.userId +' have been successfully added',3000,'success-toast')
                getUserList();
                init();
            }else{
                sandbox.showSimpleToast("You don't have sufficiant licensies to invite the user "+newuser.email,3000,'warning-toast');
            }

        }),function(error){
            sandbox.showSimpleToast('An error occured while adding user',3000,'error-toast');
        }

    }


    $scope.toggleAnimation = function () {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    };
    ////

    $scope.updateUser = function (User) {
        console.log(User)
        var modalInstance = $mdDialog.show({
            templateUrl: 'views/modals/updateUser.html',
            controller: 'updateUserController',
            locals: {
                userInfo: User
            }
        });

        modalInstance.then(function (user) {
            console.log(user);
            updateUserDetails(user)

        }, function () {

        });
    };
    
    $scope.convertRole = function (userRole){
        if (userRole == "Manager"){
            return "Manager";
        }else if(userRole == "Admin"){
            return "Admin";
        }else if(userRole == "User"){
            return "Utilisateur";
        }
    }

    function updateUserDetails(user){
        console.log(user)
        userService.updateUser(user).then(function(result){
            console.log(result)
            if(result.data == "OK"){
                sandbox.showSimpleToast('The user '+ user.email +' have been successfully updated',3000,'success-toast')
                getUserList();
                init();
            }else{
                sandbox.showSimpleToast('An error occured while updating the user' + user.userEmail,3000,'warning-toast');
            }
        }),function(error){
            sandbox.showSimpleToast('An error occured while updating user',3000,'error-toast');
        }
    }



    $scope.deleteUser = function (User) {
    var confirm = $mdDialog.confirm()
          .title('Warning!')
          .textContent('Are you sure you want to delete the user '+User.id)
          .ariaLabel('Confirm')
          .ok('Confirm')
          .cancel('Cancel');
    $mdDialog.show(confirm).then(function() {
        deleteItem(User)
    }, function() {

    });

    };


    function deleteItem(user){
        userService.deleteUser(user.id).then(function(result){
            if(result.data != "NOT_FOUND"){
                sandbox.showSimpleToast('The user '+ user.id +' have been successfully deleted',3000,'success-toast')
            }else{
               sandbox.showSimpleToast('An error occured while deleting user',3000,'error-toast');
            }
            getUserList();
            init();
        }),function(error){
            sandbox.showSimpleToast('An error occured',3000,'error-toast');
        }
    }





    function select(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        return $scope.currentPageUsers = $scope.filteredUsers.slice(start, end);
    };

    function onFilterChange() {
        $scope.select(1);
        $scope.currentPage = 1;
        return $scope.row = '';
    };

    function onNumPerPageChange() {
        $scope.select(1);
        return $scope.currentPage = 1;
    };

    function onOrderChange() {
        $scope.select(1);
        return $scope.currentPage = 1;
    };

    function search() {
        $scope.filteredUsers = $filter('filter')($scope.users, $scope.searchKeywords);
        return $scope.onFilterChange();
    };

    function order(rowName) {
        if ($scope.row === rowName) {
        return;
        }
        $scope.row = rowName;
        $scope.filteredUsers = $filter('orderBy')($scope.users, rowName);
        return $scope.onOrderChange();
    };

    init = function() {
        $scope.search();
        return $scope.select($scope.currentPage);
    };
}])
})();
