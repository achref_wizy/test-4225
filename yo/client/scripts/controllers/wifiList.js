(function () {
  'use strict';
  angular.module('app')
  .controller('wifiListCtrl', ['$scope', '$rootScope', '$q', '$timeout', 'userService', '$location', '$mdToast', 'sandbox', '$mdDialog', 'wifiService', function ($scope, $rootScope, $q, $timeout, userService, $location, $mdToast, sandbox, $mdDialog, wifiService){
     
        $scope.progressCircularUpload = false;
        $scope.paramFile = false;

        getWifiList();

        $scope.csv = {
                content: null,
                header: true,
                headerVisible: true,
                separator: ',',
                separatorVisible: true,
                result: null,
                encoding: 'ISO-8859-1',
                encodingVisible: true,
                accept:".csv"
        };

        //EDIT WIFI POP-UP
        $scope.editWifi = function(wifiId){
            var modalInstance = $mdDialog.show({
                templateUrl: 'views/modals/editWifi.html',
                controller: 'editWifiCtrl',
                data: {
                    wifiId : wifiId
                }
            });

            modalInstance.then(function(wifi) {
                if (wifi == "OK"){
                    sandbox.showSimpleToast("Wifi modifié avec succès",3000,'success-toast');
                    getWifiList();
                }else{
                    sandbox.showSimpleToast("Erreur",3000,'error-toast');
                }
            }, function (error) {
            });
        }


        //GET AGENCY LIST
        function getWifiList(){
            wifiService.listAllWifi().then(function(result){
                if(result.status == 200){
                    $scope.wifiList = result.data;
                }
            })
        }

        //ADD NEW AGENCY
        $scope.addAgency = function(){
            $location.path('/agencyAdd');
        }
        
        //DELETE AGENCY
        $scope.deleteWifi = function(index){
            wifiService.deleteWifi(index).then(function(result){
                if(result.status == 200){
                    getWifiList();
                    sandbox.showSimpleToast("Wifi supprimé avec succès",3000,'success-toast');
                }else{
                    sandbox.showSimpleToast("Erreur",3000,'error-toast');
                }
            })
        }

        //REDIRECT TO AGENCY
        $scope.goToAgencyDetails = function(agencyId){
            $location.path('/agencyDetails/' + agencyId + '/update');
        }
           
        $scope.importConfigFile = function() {
                $scope.progressCircularUpload = true;
                if ($scope.csv.result != undefined && $scope.csv.result != null) {

                    configResource.importConfigFile($scope.csv.result).then(function(result) {
                        $scope.paramFile = true;
                        sandbox.showSimpleToast("Configuration File successfully updated", 3000, 'success-toast'); 
                        $rootScope.displaymyprogressbar = false;
                        $scope.progressCircularUpload = false;
                        $scope.csvResult = result.data.genotypeList;
                        getAgencyList();

                    });
                } else {
                    console.log("error");
                }
        }


        
  }])
})();