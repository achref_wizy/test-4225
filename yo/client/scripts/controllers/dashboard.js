(function () {
  'use strict';
  angular.module('app')
  .controller('dashboardCtrl', ['$scope', '$filter', '$rootScope', '$q', '$timeout', 'userService', '$location', 'userSession', 'sandbox', '$mdDialog', 'deviceService', '$stateParams', 'taskService', function ($scope, $filter, $rootScope, $q, $timeout, userService, $location, userSession, sandbox, $mdDialog, deviceService, $stateParams, taskService){
        
          
        var init;
        var locations = [];
        getDevices();
        var device = {};
        $scope.searchKeywords = '';
        $scope.filteredStores = [];
        $scope.row = '';
        $scope.select = select;
        $scope.onFilterChange = onFilterChange;
        $scope.onNumPerPageChange = onNumPerPageChange;
        $scope.onOrderChange = onOrderChange;
        $scope.search = search;
        $scope.order = order;
        $scope.numPerPageOpt = [3, 5, 10, 20];
        $scope.numPerPage = $scope.numPerPageOpt[2];
        $scope.currentPage = 1;
        $scope.currentPage = [];
        $scope.animationsEnabled = true;           
        
        //GETTING ALL DEVICES
        function getDevices(){
            deviceService.listDevices().then(function(result){
                if(result.status == 200){
                    $scope.devicesList = result.data;
                    for (var i = 0; i < $scope.devicesList.length; i++){
                        locations.push([$scope.devicesList[i].imei, parseFloat($scope.devicesList[i].latitude), parseFloat($scope.devicesList[i].longitude), i+1])
                    }
                    initMap(locations);
                }
            })
        }

        //MAP LOCATION OF ALL DEVICES
        function initMap(locations) {
            var boolPosition = false;
            for (i = 0; i < locations.length; i++) {  
                if ((locations[i][1] == undefined)||(locations[i][2] == undefined)&&(boolPosition == false)){
                    var latitudeInit = 36.8064950;
                    var longitudeInit= 10.1815320;
                }else{
                    var latitudeInit = locations[i][1];
                    var longitudeInit= locations[i][2];
                    boolPosition     = true;
                }
            }
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 10,
              center: new google.maps.LatLng(latitudeInit, longitudeInit),
              mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();

            var marker, i;

            for (i = 0; i < locations.length; i++) {  
              marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
              });

              google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                  infowindow.setContent(locations[i][0]);
                  infowindow.open(map, marker);
                }
              })(marker, i));
            }

        }

       
        $scope.reset = function(device){
            $scope.device = {};
        }

        function select(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        };

        function onFilterChange() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        function onNumPerPageChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        function onOrderChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        function search() {
            $scope.filteredStores = $filter('filter')($scope.treatmentList, $scope.searchKeywords);
            return $scope.onFilterChange();
        };

        function order(rowName) {
            if ($scope.row === rowName) {
            return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.treatmentList, rowName);
            return $scope.onOrderChange();
        };

        init = function() {
            $scope.search();
            return $scope.select($scope.currentPage);
        };

  }])
})();