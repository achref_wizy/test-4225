(function () {
  'use strict';

  angular.module('app')
  .controller('sendMessageController',['$scope', '$mdDialog', '$mdToast', function ($scope, $mdDialog, $mdToast) {
    
    $scope.msg = {};
    $scope.type = [
      "Question",
      "Information",
      "Alerte"
    ]

    $scope.cancel = function() {
      $mdDialog.cancel();
    };
    $scope.sendMessage = function() {
      $mdDialog.hide($scope.msg);
    }


  }])
})(); 
