(function () {
    'use strict';

    angular.module('app')
        .controller('updateUserController', ['$scope', '$mdDialog', '$mdToast', 'userInfo' , function ($scope, $mdDialog,$mdToast, userInfo) {

            $scope.role = userInfo.role

            $scope.user = userInfo;
            var last = {
                bottom: false,
                top: true,
                left: false,
                right: true
            };

            function sanitizePosition() {
                var current = $scope.toastPosition;
                if ( current.bottom && last.top ) current.top = false;
                if ( current.top && last.bottom ) current.bottom = false;
                if ( current.right && last.left ) current.left = false;
                if ( current.left && last.right ) current.right = false;
                last = angular.extend({},current);
            }
              $scope.cancel = function() {
                $mdDialog.cancel();
              };

              $scope.updateUser = function() {
                userInfo.userEmail =   $scope.user.email;
                userInfo.role =        $scope.user.role;
                userInfo.displayName = $scope.user.displayName;
                $mdDialog.hide(userInfo);

              }


              $scope.rolesObj = [
                {
                  name: "User",
                  value: "User"
                },
                {
                  name: "Admin",
                  value: "Admin"
                },
                {
                  name: "Manager",
                  value: "Manager"
                }
              ]

            $scope.toastPosition = angular.extend({},last);
            $scope.getToastPosition = function() {
                sanitizePosition();
                return Object.keys($scope.toastPosition)
                    .filter(function(pos) { return $scope.toastPosition[pos]; })
                    .join(' ');
            };


}])
})(); 