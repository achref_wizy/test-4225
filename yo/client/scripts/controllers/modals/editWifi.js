(function () {
  'use strict';

  angular.module('app')
  .controller('editWifiCtrl',['$scope', '$mdDialog', '$mdToast', 'data', 'wifiService', function ($scope, $mdDialog, $mdToast, data, wifiService) {
    
    $scope.wifi = {};
    getWifi();

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    function getWifi(){
      wifiService.getWifiById(data.wifiId).then(function(result){
          if(result.status == 200){
            $scope.wifi = result.data;
          }
      });
    }

    $scope.editWifi = function() {
      wifiService.updateWifi($scope.wifi.id, $scope.wifi).then(function(result){
          $mdDialog.hide(result.data);
      });
    }


  }])
})(); 
