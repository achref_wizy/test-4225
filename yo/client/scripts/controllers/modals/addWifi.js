(function () {
  'use strict';

  angular.module('app')
  .controller('addWifiCtrl',['$scope', '$mdDialog', '$mdToast', 'data', 'wifiService', function ($scope, $mdDialog, $mdToast, data, wifiService) {
    
    $scope.wifi = {};
    console.log(data)

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.addWifi = function() {
      $scope.wifi.agencyId = data.agencyId;
      $scope.wifi.agencyName = data.agencyName;
      wifiService.addWifi($scope.wifi).then(function(result){
          $mdDialog.hide(result.data);
          console.log(result)
      });
    }


  }])
})(); 
