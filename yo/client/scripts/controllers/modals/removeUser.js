(function () {
'use strict';

angular.module('app')
    .controller('removeUserController', ['$scope', '$mdDialog', '$mdToast', 'userInfo' , 'serviceController', function ($scope, $mdDialog,$mdToast, userInfo ,serviceController) {

        console.log(userInfo)
        $scope.user = {
            name: userInfo.name,
            email:userInfo.email,
            role: userInfo.role
        };

        var last = {
            bottom: false,
            top: true,
            left: false,
            right: true
        };

        function sanitizePosition() {
            var current = $scope.toastPosition;
            if ( current.bottom && last.top ) current.top = false;
            if ( current.top && last.bottom ) current.bottom = false;
            if ( current.right && last.left ) current.left = false;
            if ( current.left && last.right ) current.right = false;
            last = angular.extend({},current);
        }
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
        $scope.removeUser = function(store) {
            /*var pinTo = $scope.getToastPosition();
             console.log($scope.user)

             serviceController.adduser($scope.user);


             console.log( serviceController.selectuser())

             $mdToast.show(
             $mdToast.simple()
             .textContent('The user : '+ $scope.user.name +'was successfully Added')
             .position(pinTo )
             .hideDelay(3000)
             );*/



            $mdDialog.hide($scope.user);

        }

        $scope.rolesObj = [
            {
                name: "User",
                value: "User"
            },
            {
                name: "Manager",
                value: "Manager"
            }
        ]

        $scope.toastPosition = angular.extend({},last);
        $scope.getToastPosition = function() {
            sanitizePosition();
            return Object.keys($scope.toastPosition)
                .filter(function(pos) { return $scope.toastPosition[pos]; })
                .join(' ');
        };


    }])
})(); 

