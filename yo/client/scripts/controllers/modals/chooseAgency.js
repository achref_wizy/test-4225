(function () {
  'use strict';

  angular.module('app')
  .controller('chooseAgencyCtrl',['$scope', '$mdDialog', '$mdToast', 'agencyService',function ($scope, $mdDialog, $mdToast, agencyService) {
    
    getAgencyList();
    $scope.agencyList = [];

    function getAgencyList(){
        agencyService.listAgency().then(function(result){
            if(result.status == 200){
              $scope.agencyList = result.data;
            }
        })
    }

    $scope.cancel = function() {
      $mdDialog.cancel();
    };
    $scope.chooseAgency = function() {
      
      agencyService.getAgencyById($scope.agency.id).then(function(result){
        if (result.data.status == "OK"){
          $scope.agency.name = result.data.agency.name;
        }
      })

      $mdDialog.hide($scope.agency);
    }


  }])
})(); 