(function () {
  'use strict';

  angular.module('app')
  .controller('addUserController',['$scope', '$mdDialog', '$mdToast', 'userService', 'agencyService', function ($scope, $mdDialog, $mdToast, userService, agencyService) {
    
    $scope.user = {};
    getAgencyList();

    //GET AGENCY LIST
    function getAgencyList(){
        agencyService.listAgency().then(function(result){
            if(result.status == 200){
                $scope.agencyList = result.data;
            }
        })
    }

    $scope.cancel = function() {
      $mdDialog.cancel();
    };
    $scope.adduser = function() {
      var userArray = [
        $scope.user
      ]
      $mdDialog.hide(userArray);
    }


  }])
})(); 
