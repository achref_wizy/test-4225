(function () {
  'use strict';
  angular.module('app')
  .controller('devicesListCtrl', ['$scope', '$filter', '$rootScope', '$q', '$timeout', 'userService', '$location', 'userSession', 'sandbox', '$mdDialog', 'deviceService', 'taskService', function ($scope, $filter, $rootScope, $q, $timeout, userService, $location, userSession, sandbox, $mdDialog, deviceService, taskService){

        var init;
        
        var inputPerPage = 2;
        getDevices(inputPerPage, "");

        $scope.sortType       = 'status'; // set the default sort type
        $scope.sortReverse    = false;  // set the default sort order
        $scope.showTable      = true;
        $scope.devicesList    = [];
        $scope.searchKeywords = '';
        $scope.filteredDevicesList = [];
        $scope.row = '';
        $scope.select = select;
        $scope.onFilterChange = onFilterChange;
        $scope.onNumPerPageChange = onNumPerPageChange;
        $scope.onOrderChange = onOrderChange;
        $scope.search = search;
        $scope.order = order;
        $scope.numPerPageOpt = [inputPerPage];
        $scope.numPerPage = $scope.numPerPageOpt[0];
        $scope.currentPage = 1;
        $scope.currentPage = [];
        $scope.animationsEnabled = true;
        var selectedItemArray = [];
        var indexTokenPage = 0;
        var tokensPages = [];
        $scope.nextButton = false;
        $scope.previousButton = true;
        $scope.filterByStatus = "";
        $scope.selectedDevicesActionButton = false;
        $scope.selectedDevicesNumber = 0;
        $scope.filterResultCount = 0;
        $scope.isAllDevicesSelected = false;
        var numberOfTotalDevices = 0;
        var currentCountDevices = 0;
        var msg = {};

        //ADD DEVICE REDIRECT
        $scope.AddDevice = function(){
            $location.path('/deviceAdd');
        }

        //GET COUNT DEVICES
        function getCountDevices(){
            deviceService.countAllDevices().then(function(result){
                if(result.statusText == "OK"){
                    numberOfTotalDevices = result.data;
                }
            })
        }

        //REFRESH BUTTON
        $scope.getDevicesRefresh = function(){
            getDevices(inputPerPage, "");
        }

        //SELECT ALL DEVICES IN DATABASE
        $scope.toggleAllDevices = function(){
            var toggleStatus = $scope.isAllDevicesSelected;
            angular.forEach($scope.devicesList, function(itm){ itm.selected = $scope.isAllDevicesSelected; });
            if (toggleStatus == true){
                $scope.selectedDevicesNumber = numberOfTotalDevices;
            }else{
                $scope.selectedDevicesNumber = 0;
            }

            if (toggleStatus == true){
                $scope.selectedDevicesActionButton = true;
            }else{
                $scope.selectedDevicesActionButton = false;
            }
        }

        //SELECT ALL CURRENT PAGE DEVICES
        $scope.toggleAll = function() {
            var toggleStatus = $scope.isAllSelected;
            angular.forEach($scope.devicesList, function(itm){ itm.selected = toggleStatus; });
            selectAllPageDevices(toggleStatus);
        }   

        //SELECT ALL CURRENT PAGE DEVICES (NOT COMPLETED)
        function selectAllPageDevices(toggleStatus){

            for(var i = 0; i< $scope.devicesList.length; i++){
                $scope.updateSelection($scope.devicesList[i].imei,toggleStatus);
            }
            $scope.selectedDevicesNumber = selectedItemArray.length;

            if ((selectedItemArray.length >= 1)||($scope.isAllDevicesSelected == true)){
                $scope.selectedDevicesActionButton = true;
            }else{
                $scope.selectedDevicesActionButton = false;
            }
        }

        //DELETE DUPLICATE FROM ARRAY
        function cleanArray(array) {
          var i, j, len = array.length, out = [], obj = {};
          for (i = 0; i < len; i++) {
            obj[array[i]] = 0;
          }
          for (j in obj) {
            out.push(j);
          }
          return out;
        } 

        //ON CHANGE CHECKBOX FILL THE ARRAY OF IMEI
        $scope.updateSelection = function(imei, checked){
            if (checked == false){
                for (var i = 0; i < selectedItemArray.length; i++){
                    if (selectedItemArray[i] == imei){
                        selectedItemArray.splice(i,1);
                    }
                }
            }else{   
                selectedItemArray.push(imei);
                selectedItemArray=cleanArray(selectedItemArray);
            }
            
            $scope.selectedDevicesNumber = selectedItemArray.length;

            if ((selectedItemArray.length >= 1)||($scope.isAllDevicesSelected == true)){
                $scope.selectedDevicesActionButton = true;
            }else{
                $scope.selectedDevicesActionButton = false;
            }
        }

        //GET DEVICES BY FILTER
        $scope.getDevicesByFilter = function(){
            var fieldValue = $scope.filterByStatus;
            deviceService.listDevicesByFilter("status", fieldValue).then(function(result){
                if(result.status == 200){
                    $scope.devicesList = result.data;
                    translateStatus ($scope.devicesList);
                    $scope.filterResultCount = $scope.devicesList.length;
                }
            })
            
        } 

        //GET DEVICES LIST
        function getDevices(maxPerPage, token){
            deviceService.listDevicesPagination(maxPerPage, token).then(function(result){
                if(result.status == 200){
                    $scope.devicesList = result.data.items;
                    translateStatus ($scope.devicesList);
                    for(var i = 0; i<selectedItemArray.length; i++){
                        for(var j = 0; j<$scope.devicesList.length; j++){
                            if(selectedItemArray[i] == $scope.devicesList[j].imei){
                                $scope.devicesList[j].selected = true;
                            }
                        }
                    }
                    tokensPages[indexTokenPage] = result.data.nextPageToken;

                    //CHECK IF WE ARE IN THE LAST PAGE
                    deviceService.countAllDevices().then(function(result){
                        if(result.status == 200){
                            numberOfTotalDevices = result.data;
                            currentCountDevices += $scope.devicesList.length;
                            if(currentCountDevices >= numberOfTotalDevices){
                                $scope.nextButton = true;
                            }else{
                                $scope.nextButton = false;
                            }
                        }
                        
                    });
                    
                }
            })
        }               

        //NEXT PAGE FUNCTION 
        $scope.nextPage = function(){

            deviceService.listDevicesPagination(2, tokensPages[indexTokenPage]).then(function(result){
                if(result.status == 200){
                    $scope.devicesList = result.data.items;
                    translateStatus ($scope.devicesList);
                    for(var i = 0; i<selectedItemArray.length; i++){
                        for(var j = 0; j<$scope.devicesList.length; j++){
                            if(selectedItemArray[i] == $scope.devicesList[j].imei){
                                $scope.devicesList[j].selected = true;
                            }
                        }
                    }
                    indexTokenPage++;
                    tokensPages[indexTokenPage] = result.data.nextPageToken;
                    $scope.previousButton = false;

                    //CHECK IF WE ARE IN THE LAST PAGE
                    currentCountDevices += $scope.devicesList.length;
                    if(currentCountDevices >= numberOfTotalDevices){
                        $scope.nextButton = true;
                    }else{
                        $scope.nextButton = false;
                    }

                    //CHECKBOX CONDITIONS
                    if ($scope.isAllDevicesSelected == true) {
                        $scope.toggleAllDevices();
                    }
                    $scope.isAllSelected = false;
                }
            })
        }

        //PREVIOUS PAGE FUNCTION
        $scope.previousPage = function(){
            //CHECK IF WE ARE IN THE LAST PAGE
            currentCountDevices -= $scope.devicesList.length;
            if(currentCountDevices >= numberOfTotalDevices){
                $scope.nextButton = true;
            }else{
                $scope.nextButton = false;
            }

            if(indexTokenPage < 2){
                
                indexTokenPage --;
                //GET DEVICES OF FIRST PAGE
                deviceService.listDevicesPagination(inputPerPage, "").then(function(result){
                    if(result.status == 200){
                        $scope.devicesList = result.data.items;
                        translateStatus ($scope.devicesList);
                        for(var i = 0; i<selectedItemArray.length; i++){
                            for(var j = 0; j<$scope.devicesList.length; j++){
                                if(selectedItemArray[i] == $scope.devicesList[j].imei){
                                    $scope.devicesList[j].selected = true;
                                }
                            }
                        }
                        tokensPages[indexTokenPage] = result.data.nextPageToken;

                        //CHECKBOX CONDITIONS 
                        if ($scope.isAllDevicesSelected == true) {
                            $scope.toggleAllDevices();
                        }
                        $scope.isAllSelected = false;
                    }
                });
                
                $scope.previousButton = true;

                

            }else{
                
                indexTokenPage--;
                deviceService.listDevicesPagination(2, tokensPages[indexTokenPage-1]).then(function(result){
                    if(result.status == 200){
                        $scope.devicesList = result.data.items;
                        translateStatus ($scope.devicesList);
                        for(var i = 0; i<selectedItemArray.length; i++){
                            for(var j = 0; j<$scope.devicesList.length; j++){
                                if(selectedItemArray[i] == $scope.devicesList[j].imei){
                                    $scope.devicesList[j].selected = true;
                                }
                            }
                        }
                        tokensPages[indexTokenPage] = result.data.nextPageToken;

                        //CHECKBOX CONDITIONS 
                        if ($scope.isAllDevicesSelected == true) {
                            $scope.toggleAllDevices();
                        }

                        $scope.isAllSelected = false;
                    }
                })
            }
        }
        
        $scope.chooseDisplayMode = function (){
            if ($scope.showTable == true ){
                $scope.showTable = false;
            }else{
                $scope.showTable = true;
            }
        }

        //SEND MESSAGE TASK
        $scope.sendMessage = function(){
            var modalInstance = $mdDialog.show({
                templateUrl: 'views/modals/sendMessage.html',
                controller: 'sendMessageController'
            });

            modalInstance.then(function(msg) {
                $scope.sendMsgSelectedDevicesTask("message", msg);

            }, function (error) {
            });
        }

        //ASSIGN TASK TO SELECTED DEVICES
        $scope.updateSelectedDevicesTask = function(action){
            
            if($scope.isAllDevicesSelected == true){
                taskService.addTaskToAllDevice(action,msg).then(function(result){
                 if(result.status == 200){
                        sandbox.showSimpleToast("Tâche assignée",3000,'success-toast');
                    }
                })
            }else{

                var payload = {
                    ids:selectedItemArray
                }
                taskService.sendTaskToMultiDevicesByRegistrationIds(action, payload).then(function(result){
                 if(result.data.status == "OK"){
                        sandbox.showSimpleToast("Tâches assignées",3000,'success-toast');
                    }else if(result.data.status == "PARTIAL_CONTENT"){
                        sandbox.showSimpleToast("Tâches n'ont pas été assignées pour tout les Terminaux séléctionnés",3000,'warning-toast');
                    }else if(result.data.status == "CONFLICT"){
                        sandbox.showSimpleToast("Tâches n'ont pas été assignées",3000,'error-toast');
                    }
                })

            }
        }

        //ASSIGN TASK TO SELECTED DEVICES
        $scope.sendMsgSelectedDevicesTask = function(action, msg){
            
             if($scope.isAllDevicesSelected == true){
                taskService.addTaskToAllDevice(action,msg).then(function(result){
                 if(result.data.status == "OK"){
                        sandbox.showSimpleToast("Tâche assignée",3000,'success-toast');
                    }
                })
            }else{
                var payload = {
                    ids:selectedItemArray,
                    message: msg
                }
                taskService.sendTaskToMultiDevicesByRegistrationIds(action, payload).then(function(result){
                   
                  if(result.data.status == "OK"){
                        sandbox.showSimpleToast("Tâche assignée",3000,'success-toast');
                    }
                })
            }
        }

        //REDIRECT TO DEVICE
        $scope.goToDeviceDetails = function(deviceId){
            $location.path('/deviceDetails/' + deviceId + '/update');
        }
        //TRANSLATE STATUS VALUE 
        function translateStatus (devicesList){
            for(var l = 0; l < devicesList.length; l++){
                if (devicesList[l].status == "active"){
                    devicesList[l].status = "Actif";
                }
                if (devicesList[l].status == "inactive"){
                    devicesList[l].status = "Inactif";
                }
                if (devicesList[l].status == "masterised"){
                    devicesList[l].status = "Masterisé";
                }
                if (devicesList[l].status == "transit"){
                    devicesList[l].status = "En transit";
                }
                if (devicesList[l].status == "registered"){
                    devicesList[l].status = "Enregistré";
                }
            }
        }

        //REFRESH PAGE 
        $scope.getDevicesRefresh = function(){

           deviceService.listDevicesPagination(inputPerPage, "").then(function(result){
                if(result.status == 200){
                    $scope.devicesList = result.data.items;
                                        
                    translateStatus ($scope.devicesList);

                    for(var i = 0; i<selectedItemArray.length; i++){
                        for(var j = 0; j<$scope.devicesList.length; j++){
                            if(selectedItemArray[i] == $scope.devicesList[j].imei){
                                $scope.devicesList[j].selected = true;
                            }
                        }
                    }
                    tokensPages[indexTokenPage] = result.data.nextPageToken;

                    //CHECK IF WE ARE IN THE LAST PAGE
                    deviceService.countAllDevices().then(function(result){
                        if(result.status == 200){
                            numberOfTotalDevices = result.data;
                            currentCountDevices += $scope.devicesList.length;
                            if(currentCountDevices >= numberOfTotalDevices){
                                $scope.nextButton = true;
                            }else{
                                $scope.nextButton = false;
                            }
                        }
                        
                    });
                    
                }
            })
        }

        $scope.deleteDevice = function(device) {
            deviceService.deleteDevice(device.imei).then(function(result){
                if(result.status == 200){
                    sandbox.showSimpleToast("Device deleted",3000,'success-toast');
                    getDevices(inputPerPage, "");
                }
            })
        };

        function select(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            return $scope.currentPageDevices = $scope.filteredDevicesList.slice(start, end);
        };

        function onFilterChange() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        function onNumPerPageChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        function onOrderChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        function search() {
            $scope.filteredDevicesList = $filter('filter')($scope.devicesList, $scope.searchKeywords);
            return $scope.onFilterChange();
        };

        function order(rowName) {
            if ($scope.row === rowName) {
            return;
            }
            $scope.row = rowName;
            $scope.filteredDevicesList = $filter('orderBy')($scope.devicesList, rowName);
            return $scope.onOrderChange();
        };

        init = function() {
            $scope.search();
            return $scope.select($scope.currentPage);
        };

        

  }])
})();