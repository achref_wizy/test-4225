(function () {
  'use strict';
  angular.module('app')
  .controller('devicesAddCtrl', ['$scope', '$filter', '$rootScope', '$q', '$timeout', 'userService', '$location', 'userSession', 'sandbox', '$mdDialog', 'deviceService', function ($scope, $filter, $rootScope, $q, $timeout, userService, $location, userSession, sandbox, $mdDialog, deviceService){

        var init;

        $scope.searchKeywords = '';
        $scope.filteredStores = [];
        $scope.row = '';
        $scope.select = select;
        $scope.onFilterChange = onFilterChange;
        $scope.onNumPerPageChange = onNumPerPageChange;
        $scope.onOrderChange = onOrderChange;
        $scope.search = search;
        $scope.order = order;
        $scope.numPerPageOpt = [3, 5, 10, 20];
        $scope.numPerPage = $scope.numPerPageOpt[2];
        $scope.currentPage = 1;
        $scope.currentPage = [];
        $scope.animationsEnabled = true;

        $scope.saveDevice = function(device){
            device.status = "registered";
            deviceService.saveOrUpdateDevice(device).then(function(result){
                if(result.status == 200){
                    console.log(result)
                    $location.path('/devices');
                }

            })
        }

        $scope.reset = function(device){
            $scope.device = {};
        }

        function select(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        };

        function onFilterChange() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        function onNumPerPageChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        function onOrderChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        function search() {
            $scope.filteredStores = $filter('filter')($scope.treatmentList, $scope.searchKeywords);
            return $scope.onFilterChange();
        };

        function order(rowName) {
            if ($scope.row === rowName) {
            return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.treatmentList, rowName);
            return $scope.onOrderChange();
        };

        init = function() {
            $scope.search();
            return $scope.select($scope.currentPage);
        };

  }])
})();