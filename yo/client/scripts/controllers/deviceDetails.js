(function () {
  'use strict';
  angular.module('app')
  .controller('devicesDetailsCtrl', ['$scope', '$filter', '$rootScope', '$q', '$timeout', 'userService', '$location', 'userSession', 'sandbox', '$mdDialog', 'deviceService', '$stateParams', 'taskService', function ($scope, $filter, $rootScope, $q, $timeout, userService, $location, userSession, sandbox, $mdDialog, deviceService, $stateParams, taskService){
        
          
        var init;

        getDeviceDetails ($stateParams.deviceID);
        var device = {};
        $scope.searchKeywords = '';
        $scope.filteredStores = [];
        $scope.row = '';
        $scope.select = select;
        $scope.onFilterChange = onFilterChange;
        $scope.onNumPerPageChange = onNumPerPageChange;
        $scope.onOrderChange = onOrderChange;
        $scope.search = search;
        $scope.order = order;
        $scope.numPerPageOpt = [3, 5, 10, 20];
        $scope.numPerPage = $scope.numPerPageOpt[2];
        $scope.currentPage = 1;
        $scope.currentPage = [];
        $scope.animationsEnabled = true;
        $scope.msg = {}; 
        $scope.showWifiReseauxInfo = false;
        $scope.showOtherConnexionReseauxInfo = false;

        $scope.saveDevice = function(device){
            console.log(device)

            deviceService.patchDevice(device, device.id).then(function(result){
                if(result.status == 200){
                     sandbox.showSimpleToast("Device Updated",3000,'success-toast');
                     getDeviceDetails (device.imei);
                }
            });

        }

        $scope.sendMessage = function(){
            var modalInstance = $mdDialog.show({
                templateUrl: 'views/modals/sendMessage.html',
                controller: 'sendMessageController'
            });

            modalInstance.then(function(msg) {
                $scope.addTaskDevice("message", msg);

            }, function (error) {
            });
        }

        $scope.addTaskDevice = function(action, msg){

            if(msg == null){ 
                msg = {}
            }

            if(action == "unlock"){
                msg = {
                    title: action,
                    type:"pin",
                    content:""
                }
            }

            if($scope.device.status == "inactive"){

                sandbox.showSimpleToast("Le status doit être activé",3000,'Error-toast');
            
            }else{

                taskService.addNewTask($stateParams.deviceID, action, msg).then(function(result){
                    if(result.status == 200){
                        if (result.data.status == "TOO_MANY_REQUESTS"){
                            sandbox.showSimpleToast("Tâche déjà assignée",3000,'Error-toast');
                        }else{
                            sandbox.showSimpleToast("Tâche ajoutée",3000,'success-toast');
                        }
                    }else{
                        sandbox.showSimpleToast("Erreur",3000,'Error-toast');
                    }
                });

                deviceService.saveOrUpdateDevice(device).then(function(result){
                    if(result.status == 200){
                         getDeviceDetails ($stateParams.deviceID);
                    }
                });

            }
            
        }


        function getDeviceDetails (deviceID){

            deviceService.getDevice(deviceID).then(function(result){
                if(result.status == 200){
                    $scope.device = result.data.device;
                    
                    if ($scope.device.connexionType == "wifi"){
                        $scope.showWifiReseauxInfo = true;
                        $scope.showOtherConnexionReseauxInfo = false;
                    }else{
                        $scope.showWifiReseauxInfo = false;
                        $scope.showOtherConnexionReseauxInfo = true;
                    }

                    if(!isNaN(parseFloat($scope.device.latitude)) && isFinite($scope.device.latitude)){
                        initMap(JSON.parse($scope.device.longitude), JSON.parse($scope.device.latitude), $scope.device.name)
                    }else{
                        $("#map").hide();
                    }
                    
                }
            });
            
        }

        function initMap(lng, lat, name) {
          var myLatLng = {lat: lat, lng: lng};

          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: myLatLng
          });

          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: name
          });
        }

       
        $scope.reset = function(device){
            $scope.device = {};
        }

        function select(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        };

        function onFilterChange() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        function onNumPerPageChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        function onOrderChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        function search() {
            $scope.filteredStores = $filter('filter')($scope.treatmentList, $scope.searchKeywords);
            return $scope.onFilterChange();
        };

        function order(rowName) {
            if ($scope.row === rowName) {
            return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.treatmentList, rowName);
            return $scope.onOrderChange();
        };

        init = function() {
            $scope.search();
            return $scope.select($scope.currentPage);
        };

  }])
})();