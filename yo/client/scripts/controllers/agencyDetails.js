(function () {
  'use strict';
  angular.module('app')
  .controller('agencyDetailsCtrl', ['$scope', '$filter', '$rootScope', '$q', '$timeout', 'userService', '$location', 'userSession', 'sandbox', '$mdDialog', '$stateParams', 'agencyService', 'wifiService', function ($scope, $filter, $rootScope, $q, $timeout, userService, $location, userSession, sandbox, $mdDialog, $stateParams, agencyService, wifiService){
        
          
        var init;
        getAgencyDetails ($stateParams.agencyID);
        listAllWifiConfiguration($stateParams.agencyID);

        var agency = {};
        $scope.searchKeywords = '';
        $scope.filteredStores = [];
        $scope.row = '';
        $scope.select = select;
        $scope.onFilterChange = onFilterChange;
        $scope.onNumPerPageChange = onNumPerPageChange;
        $scope.onOrderChange = onOrderChange;
        $scope.search = search;
        $scope.order = order;
        $scope.numPerPageOpt = [3, 5, 10, 20];
        $scope.numPerPage = $scope.numPerPageOpt[2];
        $scope.currentPage = 1;
        $scope.currentPage = [];
        $scope.animationsEnabled = true;

        //GET ALL AGENCY DETAILS
        function getAgencyDetails (agencyId){
            agencyService.getAgencyById(agencyId).then(function(result){
                if(result.data.status == "OK"){
                    $scope.agency = result.data.agency;
                    if(!isNaN(parseFloat($scope.agency.latitude)) && isFinite($scope.agency.latitude)){
                        initMap(JSON.parse($scope.agency.longitude), JSON.parse($scope.agency.latitude), $scope.agency.name)
                    }else{
                        $("#map").hide();
                    }
                }
            });            
        }

        //UPDATE AGENCY INFORMATIONS
        $scope.updateAgency = function(agency){
            agencyService.updateAgency(agency.id, agency).then(function(result){
                if(result.status == 200){
                     sandbox.showSimpleToast("Agence Modifiée",3000,'success-toast');
                     getAgencyDetails (agency.id)
                }
            });

        }        

        function initMap(lng, lat, name) {
          var myLatLng = {lat: lat, lng: lng};

          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: myLatLng
          });

          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: name
          });
        }


        //EDIT WIFI POP-UP
        $scope.editWifi = function(wifiId){
            var modalInstance = $mdDialog.show({
                templateUrl: 'views/modals/editWifi.html',
                controller: 'editWifiCtrl',
                data: {
                    wifiId : wifiId
                }
            });

            modalInstance.then(function(wifi) {
                if (wifi == "OK"){
                    sandbox.showSimpleToast("Wifi modifié avec succès",3000,'success-toast');
                    listAllWifiConfiguration($stateParams.agencyID);
                }else{
                    sandbox.showSimpleToast("Erreur",3000,'error-toast');
                }
            }, function (error) {
            });
        }

        //ADD WIFI POP-UP
        $scope.addWifi = function(){
            var modalInstance = $mdDialog.show({
                templateUrl: 'views/modals/addWifi.html',
                controller: 'addWifiCtrl',
                data: {
                    agencyId : $stateParams.agencyID,
                    agencyName : $scope.agency.name
                }
            });

            modalInstance.then(function(wifi) {
                if (wifi == "OK"){
                    sandbox.showSimpleToast("Wifi ajouté avec succès",3000,'success-toast');
                    listAllWifiConfiguration($stateParams.agencyID);
                }else{
                    sandbox.showSimpleToast("Erreur",3000,'error-toast');
                }
            }, function (error) {
            });
        }
        

        //LIST ALL WIFI OF THE AGENCY
        function listAllWifiConfiguration(agencyId){
            wifiService.listWifiByAgencyId(agencyId).then(function(result){
                $scope.wifiConfig = result.data;
            });
        }

        //DELETE A WIFI
        $scope.deleteWifi = function (wifiId){
            wifiService.deleteWifi(wifiId).then(function(result){
                if (result.data == "OK"){
                    sandbox.showSimpleToast("Wifi supprimé avec succès",3000,'success-toast');
                    listAllWifiConfiguration($stateParams.agencyID);
                }else{
                    sandbox.showSimpleToast("Erreur",3000,'error-toast');
                }
            });
        }

        $scope.reset = function(device){
            $scope.device = {};
        }

        function select(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        };

        function onFilterChange() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        function onNumPerPageChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        function onOrderChange() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        function search() {
            $scope.filteredStores = $filter('filter')($scope.treatmentList, $scope.searchKeywords);
            return $scope.onFilterChange();
        };

        function order(rowName) {
            if ($scope.row === rowName) {
            return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.treatmentList, rowName);
            return $scope.onOrderChange();
        };

        init = function() {
            $scope.search();
            return $scope.select($scope.currentPage);
        };

  }])
})();