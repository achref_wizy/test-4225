(function() {
    'use strict';

    var app = angular.module('app', [
        'ngMaterial',
        'mgo-angular-wizard',
        'ui.tree',
        'textAngular',
        'ngAnimate',
        'ngAria',
        'ngMessages',
        'app.i18n',
        'ui.router',
        'ui.bootstrap',
        'duScroll',
        'ngRoute',
        'angular.filter',
        'ngCookies',
        'ngecharts',
        'ngMap',
        'ngCsvImport'
    ])
    app.filter('num', function() {
        return function(input) {
            return parseInt(input, 10);
        }
    });

    app.factory('appConfig', [function() {
        var pageTransitionOpts = [{
            name: 'Fade up',
            "class": 'animate-fade-up'
        }, {
            name: 'Scale up',
            "class": 'ainmate-scale-up'
        }, {
            name: 'Slide in from right',
            "class": 'ainmate-slide-in-right'
        }, {
            name: 'Flip Y',
            "class": 'animate-flip-y'
        }];
        var date = new Date();
        var year = date.getFullYear();
        var main = {
            brand: 'Nutgen',
            year: year,
            layout: 'wide', // 'boxed', 'wide'
            menu: 'vertical', // 'horizontal', 'vertical'
            isMenuCollapsed: false, // true, false
            fixedHeader: true, // true, false
            fixedSidebar: true, // true, false
            pageTransition: pageTransitionOpts[0], // 0, 1, 2, 3... and build your own
            skin: '12' // 11,12,13,14,15,16; 21,22,23,24,25,26; 31,32,33,34,35,36
        };
        var color = {
            primary: '#007ac7',
            success: '#8BC34A',
            info: '#00BCD4',
            infoAlt: '#7E57C2',
            warning: '#FFCA28',
            danger: '#F44336',
            text: '#007ac7',
            gray: '#EDF0F1'
        };

        return {
            pageTransitionOpts: pageTransitionOpts,
            main: main,
            color: color
        }
    }]);

    app.config(['$mdThemingProvider', '$stateProvider', '$urlRouterProvider', function($mdThemingProvider, $stateProvider, $urlRouterProvider) {

        var pinkAlt = $mdThemingProvider.extendPalette('blue', {
            'contrastLightColors': 'A200 A700',
            'contrastStrongLightColors': 'A200 A700'
        })
        var purpleAlt = $mdThemingProvider.extendPalette('purple', {
            'contrastLightColors': '700 800 900',
            'contrastStrongLightColors': '700 800 900'
        })

        $mdThemingProvider
            .definePalette('pinkAlt', pinkAlt)
            .definePalette('purpleAlt', purpleAlt);


        $mdThemingProvider.theme('default')
            .primaryPalette('pinkAlt', {
                'default': 'A200'
            })
            .accentPalette('purpleAlt', {
                'default': '700'
            })
            .warnPalette('red', {
                'default': '500'
            })
            .backgroundPalette('grey');


        $urlRouterProvider
            .when('/', '/dashboard')
            .when('', '/dashboard')
            //  $urlRouterProvider.otherwise('/page/signin');

        $stateProvider
            .state('/page/signin', {
                url: '/page/signin',
                templateUrl: 'views/page/signin.html'
            })
            .state('devices', {
                url: '/devices',
                templateUrl: 'views/devices.html',
                data: {
                    requiresLogin: true
                }
            })
            .state('accountManagement', {
                url: "/accountManagement",
                templateUrl: "views/accountManagement.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('userManagement', {
                url: "/userManagement",
                templateUrl: "views/userManagement.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('deviceAdd', {
                url: "/deviceAdd",
                templateUrl: "views/deviceAdd.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('deviceDetails', {
                url: "/deviceDetails/:deviceID/update",
                templateUrl: "views/deviceDetails.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('tasks', {
                url: "/tasks",
                templateUrl: "views/tasks.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('dashboard', {
                url: "/dashboard",
                templateUrl: "views/dashboard.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('fleetList', {
                url: "/fleetList",
                templateUrl: "views/fleetList.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('demandRma', {
                url: "/demandRma",
                templateUrl: "views/demandRma.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('otherFleet', {
                url: "/otherFleet",
                templateUrl: "views/otherFleet.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('fleetConfig', {
                url: "/fleetConfig",
                templateUrl: "views/fleetConfig.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('agencyList', {
                url: "/agencyList",
                templateUrl: "views/agencyList.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('agencyDetails', {
                url: "/agencyDetails/:agencyID/update",
                templateUrl: "views/agencyDetails.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('agencyAdd', {
                url: "/agencyAdd",
                templateUrl: "views/agencyAdd.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('statistics', {
                url: "/statistics",
                templateUrl: "views/statistics.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('wifiList', {
                url: "/wifiList",
                templateUrl: "views/wifiList.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('404', {
                url: "/404",
                templateUrl: "views/page/404.html",
                data: {
                    requiresLogin: true
                }
            })
            .state('500', {
                url: "/500",
                templateUrl: "views/page/500.html",
                data: {
                    requiresLogin: true
                }
            });
    }]);

    app.run(['$rootScope', '$location', 'datastoreUser', 'userSession', 'sandbox', function($rootScope, $location, datastoreUser, userSession, sandbox) {


        var path = $location.path();
        //||  path == "/account"
   /*     if (path == "/loading" || path == "/registration" || path == "/account") {
            path = "/";
        } */
        console.log("test");

        //OAUTH 2 ATHENTIFICATION
        datastoreUser.LoginUser("me").then(function(result) {

            console.log('API OAuth2 response : User is authenticated');
            console.log(result);

           if (result.data.status != "OK") {
                    $location.path('/page/signin');
            } else {

                    userSession.setUser(result.data.user);
                    var user = result.data.user;
                    userSession.setExist('true');
                    $rootScope.connecteduser = {
                        name: /*displayName*/ user.email,
                        id: user.id,
                        image: "https://lh3.googleusercontent.com/-uhoWxz5n6Jc/AAAAAAAAAAI/AAAAAAAAAAA/TM_L-7sYrYU/s60-p-rw-no/photo.jpg",
                        role: user.role
                    }
                    $rootScope.domainexsistance = true;
                    $rootScope.convertRole = sandbox.convertRole;
                    $('#loader-container').fadeOut("slow");
                }
            
        }, function(data) {
            console.log('API OAuth2 response : User is not authenticated');
            console.log(data);
            window.location.href = '/oauth2login' + path;
        });

    }]);

    app.controller('LoaderCtrl', ['$scope', '$rootScope', LoaderCtrl])
    
    function LoaderCtrl($scope, $rootScope) {
        $scope.start = function() {
            $rootScope.$broadcast('preloader:active');
        }
        $scope.complete = function() {
            $rootScope.$broadcast('preloader:hide');
        }
    }


    app.controller('logoutCtrl', ['$scope', '$http', '$location', logoutCtrl]);

    function logoutCtrl($scope, $http, $location) {
        $scope.logout = function() {
            $location.path('/page/signin');
        }
    }

})();