package com.wizy.restx.Utils;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.Utils.GAEModule;


@Machine
public class GAEModuleFactoryMachine extends DefaultFactoryMachine {
    private static final GAEModule module = new GAEModule();

    public GAEModuleFactoryMachine() {
        super(0, new MachineEngine[] {
            new StdMachineEngine<java.lang.String>(Name.of(java.lang.String.class, "restx.activation::restx.security.RestxSessionCookieFilter::RestxSessionCookieFilter"), 0, BoundlessComponentBox.FACTORY) {
        
                @Override
                public BillOfMaterials getBillOfMaterial() {
                    return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(
        
                    ));
                }

                @Override
                public java.lang.String doNewComponent(SatisfiedBOM satisfiedBOM) {
	                
	                    return module.deactivateCookieSessionFilter(
	        
	                    );
	                
                }
            },
            new StdMachineEngine<java.lang.String>(Name.of(java.lang.String.class, "restx.activation::restx.security.RestxSessionBareFilter::RestxSessionBareFilter"), 0, BoundlessComponentBox.FACTORY) {
        
                @Override
                public BillOfMaterials getBillOfMaterial() {
                    return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(
        
                    ));
                }

                @Override
                public java.lang.String doNewComponent(SatisfiedBOM satisfiedBOM) {
	                
	                    return module.activateBareSessionFilter(
	        
	                    );
	                
                }
            },
            new StdMachineEngine<java.lang.String>(Name.of(java.lang.String.class, "restx.admin.password"), 0, BoundlessComponentBox.FACTORY) {
        
                @Override
                public BillOfMaterials getBillOfMaterial() {
                    return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(
        
                    ));
                }

                @Override
                public java.lang.String doNewComponent(SatisfiedBOM satisfiedBOM) {
	                
	                    return module.restxAdminPassword(
	        
	                    );
	                
                }
            },
            new StdMachineEngine<restx.servlet.ServletPrincipalConverter>(Name.of(restx.servlet.ServletPrincipalConverter.class, "ServletPrincipalConverter"), 0, BoundlessComponentBox.FACTORY) {
        
                @Override
                public BillOfMaterials getBillOfMaterial() {
                    return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(
        
                    ));
                }

                @Override
                public restx.servlet.ServletPrincipalConverter doNewComponent(SatisfiedBOM satisfiedBOM) {
	                
	                    return module.gaeServletPrincipalConverter(
	        
	                    );
	                
                }
            },
        });
}
}
