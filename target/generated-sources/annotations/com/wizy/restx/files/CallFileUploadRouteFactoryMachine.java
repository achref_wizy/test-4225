package com.wizy.restx.files;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.files.CallFileUploadRoute;

@Machine
public class CallFileUploadRouteFactoryMachine extends SingleNameFactoryMachine<CallFileUploadRoute> {
    public static final Name<CallFileUploadRoute> NAME = Name.of(CallFileUploadRoute.class, "CallFileUploadRoute");

    public CallFileUploadRouteFactoryMachine() {
        super(0, new StdMachineEngine<CallFileUploadRoute>(NAME, 0, BoundlessComponentBox.FACTORY) {


            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(

                ));
            }

            @Override
            protected CallFileUploadRoute doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new CallFileUploadRoute(

                );
            }
        });
    }

}
