package com.wizy.restx.files;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.files.CallFileManagerResources;

@Machine
public class CallFileManagerResourcesFactoryMachine extends SingleNameFactoryMachine<CallFileManagerResources> {
    public static final Name<CallFileManagerResources> NAME = Name.of(CallFileManagerResources.class, "CallFileManagerResources");

    public CallFileManagerResourcesFactoryMachine() {
        super(0, new StdMachineEngine<CallFileManagerResources>(NAME, 0, BoundlessComponentBox.FACTORY) {


            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(

                ));
            }

            @Override
            protected CallFileManagerResources doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new CallFileManagerResources(

                );
            }
        });
    }

}
