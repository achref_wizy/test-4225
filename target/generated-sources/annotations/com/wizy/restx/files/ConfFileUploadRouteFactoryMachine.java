package com.wizy.restx.files;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.files.ConfFileUploadRoute;

@Machine
public class ConfFileUploadRouteFactoryMachine extends SingleNameFactoryMachine<ConfFileUploadRoute> {
    public static final Name<ConfFileUploadRoute> NAME = Name.of(ConfFileUploadRoute.class, "ConfFileUploadRoute");

    public ConfFileUploadRouteFactoryMachine() {
        super(0, new StdMachineEngine<ConfFileUploadRoute>(NAME, 0, BoundlessComponentBox.FACTORY) {


            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(

                ));
            }

            @Override
            protected ConfFileUploadRoute doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new ConfFileUploadRoute(

                );
            }
        });
    }

}
