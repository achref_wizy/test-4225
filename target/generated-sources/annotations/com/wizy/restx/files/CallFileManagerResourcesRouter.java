package com.wizy.restx.files;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;

import restx.common.Types;
import restx.*;
import restx.entity.*;
import restx.http.*;
import restx.factory.*;
import restx.security.*;
import static restx.security.Permissions.*;
import restx.description.*;
import restx.converters.MainStringConverter;
import static restx.common.MorePreconditions.checkPresent;

import javax.validation.Validator;
import static restx.validation.Validations.checkValid;

import java.io.IOException;
import java.io.PrintWriter;

@Component(priority = 0)

public class CallFileManagerResourcesRouter extends RestxRouter {

    public CallFileManagerResourcesRouter(
                    final CallFileManagerResources resource,
                    final EntityRequestBodyReaderRegistry readerRegistry,
                    final EntityResponseWriterRegistry writerRegistry,
                    final MainStringConverter converter,
                    final Validator validator,
                    final RestxSecurityManager securityManager) {
        super(
            "default", "CallFileManagerResourcesRouter", new RestxRoute[] {
        new StdEntityRoute<Void, com.wizy.json.response.gcs.CallFileResponse>("default#CallFileManagerResources#downloadFile",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.gcs.CallFileResponse>build(com.wizy.json.response.gcs.CallFileResponse.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/file/call/download/{userid}/file/{fileid}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.gcs.CallFileResponse> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.downloadFile(
                        /* [PATH] userid */ match.getPathParam("userid"),
                        /* [PATH] fileid */ match.getPathParam("fileid")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription userid = new OperationParameterDescription();
                userid.name = "userid";
                userid.paramType = OperationParameterDescription.ParamType.path;
                userid.dataType = "string";
                userid.schemaKey = "";
                userid.required = true;
                operation.parameters.add(userid);

                OperationParameterDescription fileid = new OperationParameterDescription();
                fileid.name = "fileid";
                fileid.paramType = OperationParameterDescription.ParamType.path;
                fileid.dataType = "string";
                fileid.schemaKey = "";
                fileid.required = true;
                operation.parameters.add(fileid);


                operation.responseClass = "CallFileResponse";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.gcs.CallFileResponse";
                operation.sourceLocation = "com.wizy.restx.files.CallFileManagerResources#downloadFile(java.lang.String,java.lang.String)";
            }
        },
        new StdEntityRoute<Void, java.util.List<com.wizy.model.gcs.GcsCallFile>>("default#CallFileManagerResources#getFileListByPatient",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<java.util.List<com.wizy.model.gcs.GcsCallFile>>build(Types.newParameterizedType(java.util.List.class, com.wizy.model.gcs.GcsCallFile.class), Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/file/call/patient/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<java.util.List<com.wizy.model.gcs.GcsCallFile>> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.getFileListByPatient(
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "GcsCallFile>";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.files.CallFileManagerResources#getFileListByPatient(java.lang.String)";
            }
        },
        new StdEntityRoute<com.wizy.model.gcs.GcsCallFile, restx.http.HttpStatus>("default#CallFileManagerResources#UpdateFile",
                readerRegistry.<com.wizy.model.gcs.GcsCallFile>build(com.wizy.model.gcs.GcsCallFile.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/file/call/user/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.gcs.GcsCallFile body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.UpdateFile(
                        /* [PATH] id */ match.getPathParam("id"),
                        /* [BODY] file */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);

                OperationParameterDescription file = new OperationParameterDescription();
                file.name = "file";
                file.paramType = OperationParameterDescription.ParamType.body;
                file.dataType = "GcsCallFile";
                file.schemaKey = "com.wizy.model.gcs.GcsCallFile";
                file.required = true;
                operation.parameters.add(file);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "com.wizy.model.gcs.GcsCallFile";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.files.CallFileManagerResources#UpdateFile(java.lang.String,com.wizy.model.gcs.GcsCallFile)";
            }
        },
        new StdEntityRoute<Void, restx.http.HttpStatus>("default#CallFileManagerResources#deleteFile",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("DELETE", "/secured/file/call/{fileid}/user/{userid}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.deleteFile(
                        /* [PATH] fileid */ match.getPathParam("fileid"),
                        /* [PATH] userid */ match.getPathParam("userid")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription fileid = new OperationParameterDescription();
                fileid.name = "fileid";
                fileid.paramType = OperationParameterDescription.ParamType.path;
                fileid.dataType = "string";
                fileid.schemaKey = "";
                fileid.required = true;
                operation.parameters.add(fileid);

                OperationParameterDescription userid = new OperationParameterDescription();
                userid.name = "userid";
                userid.paramType = OperationParameterDescription.ParamType.path;
                userid.dataType = "string";
                userid.schemaKey = "";
                userid.required = true;
                operation.parameters.add(userid);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.files.CallFileManagerResources#deleteFile(java.lang.String,java.lang.String)";
            }
        },
        });
    }

}
