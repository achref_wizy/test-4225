package com.wizy.restx.files;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.files.ConfigFileManagerResources;

@Machine
public class ConfigFileManagerResourcesFactoryMachine extends SingleNameFactoryMachine<ConfigFileManagerResources> {
    public static final Name<ConfigFileManagerResources> NAME = Name.of(ConfigFileManagerResources.class, "ConfigFileManagerResources");

    public ConfigFileManagerResourcesFactoryMachine() {
        super(0, new StdMachineEngine<ConfigFileManagerResources>(NAME, 0, BoundlessComponentBox.FACTORY) {


            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(

                ));
            }

            @Override
            protected ConfigFileManagerResources doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new ConfigFileManagerResources(

                );
            }
        });
    }

}
