package com.wizy.restx.files;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.files.ConfigFileManagerResourcesRouter;

@Machine
public class ConfigFileManagerResourcesRouterFactoryMachine extends SingleNameFactoryMachine<ConfigFileManagerResourcesRouter> {
    public static final Name<ConfigFileManagerResourcesRouter> NAME = Name.of(ConfigFileManagerResourcesRouter.class, "ConfigFileManagerResourcesRouter");

    public ConfigFileManagerResourcesRouterFactoryMachine() {
        super(0, new StdMachineEngine<ConfigFileManagerResourcesRouter>(NAME, 0, BoundlessComponentBox.FACTORY) {
private final Factory.Query<com.wizy.restx.files.ConfigFileManagerResources> resource = Factory.Query.byClass(com.wizy.restx.files.ConfigFileManagerResources.class).mandatory();
private final Factory.Query<restx.entity.EntityRequestBodyReaderRegistry> readerRegistry = Factory.Query.byClass(restx.entity.EntityRequestBodyReaderRegistry.class).mandatory();
private final Factory.Query<restx.entity.EntityResponseWriterRegistry> writerRegistry = Factory.Query.byClass(restx.entity.EntityResponseWriterRegistry.class).mandatory();
private final Factory.Query<restx.converters.MainStringConverter> converter = Factory.Query.byClass(restx.converters.MainStringConverter.class).mandatory();
private final Factory.Query<javax.validation.Validator> validator = Factory.Query.byClass(javax.validation.Validator.class).mandatory();
private final Factory.Query<restx.security.RestxSecurityManager> securityManager = Factory.Query.byClass(restx.security.RestxSecurityManager.class).mandatory();

            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(
resource,
readerRegistry,
writerRegistry,
converter,
validator,
securityManager
                ));
            }

            @Override
            protected ConfigFileManagerResourcesRouter doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new ConfigFileManagerResourcesRouter(
satisfiedBOM.getOne(resource).get().getComponent(),
satisfiedBOM.getOne(readerRegistry).get().getComponent(),
satisfiedBOM.getOne(writerRegistry).get().getComponent(),
satisfiedBOM.getOne(converter).get().getComponent(),
satisfiedBOM.getOne(validator).get().getComponent(),
satisfiedBOM.getOne(securityManager).get().getComponent()
                );
            }
        });
    }

}
