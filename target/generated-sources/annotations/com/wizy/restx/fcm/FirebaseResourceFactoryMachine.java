package com.wizy.restx.fcm;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.fcm.FirebaseResource;

@Machine
public class FirebaseResourceFactoryMachine extends SingleNameFactoryMachine<FirebaseResource> {
    public static final Name<FirebaseResource> NAME = Name.of(FirebaseResource.class, "FirebaseResource");

    public FirebaseResourceFactoryMachine() {
        super(0, new StdMachineEngine<FirebaseResource>(NAME, 0, BoundlessComponentBox.FACTORY) {


            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(

                ));
            }

            @Override
            protected FirebaseResource doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new FirebaseResource(

                );
            }
        });
    }

}
