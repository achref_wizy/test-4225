package com.wizy.restx.fcm;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;

import restx.common.Types;
import restx.*;
import restx.entity.*;
import restx.http.*;
import restx.factory.*;
import restx.security.*;
import static restx.security.Permissions.*;
import restx.description.*;
import restx.converters.MainStringConverter;
import static restx.common.MorePreconditions.checkPresent;

import javax.validation.Validator;
import static restx.validation.Validations.checkValid;

import java.io.IOException;
import java.io.PrintWriter;

@Component(priority = 0)

public class FirebaseResourceRouter extends RestxRouter {

    public FirebaseResourceRouter(
                    final FirebaseResource resource,
                    final EntityRequestBodyReaderRegistry readerRegistry,
                    final EntityResponseWriterRegistry writerRegistry,
                    final MainStringConverter converter,
                    final Validator validator,
                    final RestxSecurityManager securityManager) {
        super(
            "default", "FirebaseResourceRouter", new RestxRoute[] {
        new StdEntityRoute<com.wizy.model.mdm.Message, restx.http.HttpStatus>("default#FirebaseResource#sendTaskToOneDeviceByToken",
                readerRegistry.<com.wizy.model.mdm.Message>build(com.wizy.model.mdm.Message.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/fcm/device/{imei}/token/{token}/action/{action}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.Message body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.sendTaskToOneDeviceByToken(
                        /* [PATH] imei */ match.getPathParam("imei"),
                        /* [PATH] token */ match.getPathParam("token"),
                        /* [PATH] action */ match.getPathParam("action"),
                        /* [BODY] message */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription imei = new OperationParameterDescription();
                imei.name = "imei";
                imei.paramType = OperationParameterDescription.ParamType.path;
                imei.dataType = "string";
                imei.schemaKey = "";
                imei.required = true;
                operation.parameters.add(imei);

                OperationParameterDescription token = new OperationParameterDescription();
                token.name = "token";
                token.paramType = OperationParameterDescription.ParamType.path;
                token.dataType = "string";
                token.schemaKey = "";
                token.required = true;
                operation.parameters.add(token);

                OperationParameterDescription action = new OperationParameterDescription();
                action.name = "action";
                action.paramType = OperationParameterDescription.ParamType.path;
                action.dataType = "string";
                action.schemaKey = "";
                action.required = true;
                operation.parameters.add(action);

                OperationParameterDescription message = new OperationParameterDescription();
                message.name = "message";
                message.paramType = OperationParameterDescription.ParamType.body;
                message.dataType = "Message";
                message.schemaKey = "com.wizy.model.mdm.Message";
                message.required = true;
                operation.parameters.add(message);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.Message";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.fcm.FirebaseResource#sendTaskToOneDeviceByToken(java.lang.String,java.lang.String,java.lang.String,com.wizy.model.mdm.Message)";
            }
        },
        new StdEntityRoute<com.wizy.json.response.twoObjects.MessageMultipleDevice, restx.http.HttpStatus>("default#FirebaseResource#sendTaskToMultiDevicesByRegistrationIds",
                readerRegistry.<com.wizy.json.response.twoObjects.MessageMultipleDevice>build(com.wizy.json.response.twoObjects.MessageMultipleDevice.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/fcm/devices/action/{action}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.json.response.twoObjects.MessageMultipleDevice body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.sendTaskToMultiDevicesByRegistrationIds(
                        /* [PATH] action */ match.getPathParam("action"),
                        /* [BODY] payload */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription action = new OperationParameterDescription();
                action.name = "action";
                action.paramType = OperationParameterDescription.ParamType.path;
                action.dataType = "string";
                action.schemaKey = "";
                action.required = true;
                operation.parameters.add(action);

                OperationParameterDescription payload = new OperationParameterDescription();
                payload.name = "payload";
                payload.paramType = OperationParameterDescription.ParamType.body;
                payload.dataType = "MessageMultipleDevice";
                payload.schemaKey = "com.wizy.json.response.twoObjects.MessageMultipleDevice";
                payload.required = true;
                operation.parameters.add(payload);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "com.wizy.json.response.twoObjects.MessageMultipleDevice";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.fcm.FirebaseResource#sendTaskToMultiDevicesByRegistrationIds(java.lang.String,com.wizy.json.response.twoObjects.MessageMultipleDevice)";
            }
        },
        new StdEntityRoute<com.wizy.model.mdm.Message, restx.http.HttpStatus>("default#FirebaseResource#sendTaskToAllDevicesByFilter",
                readerRegistry.<com.wizy.model.mdm.Message>build(com.wizy.model.mdm.Message.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/fcm/devices/filter/{name}/value/{value}/action/{action}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.Message body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.sendTaskToAllDevicesByFilter(
                        /* [PATH] name */ match.getPathParam("name"),
                        /* [PATH] value */ match.getPathParam("value"),
                        /* [PATH] action */ match.getPathParam("action"),
                        /* [BODY] message */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription name = new OperationParameterDescription();
                name.name = "name";
                name.paramType = OperationParameterDescription.ParamType.path;
                name.dataType = "string";
                name.schemaKey = "";
                name.required = true;
                operation.parameters.add(name);

                OperationParameterDescription value = new OperationParameterDescription();
                value.name = "value";
                value.paramType = OperationParameterDescription.ParamType.path;
                value.dataType = "string";
                value.schemaKey = "";
                value.required = true;
                operation.parameters.add(value);

                OperationParameterDescription action = new OperationParameterDescription();
                action.name = "action";
                action.paramType = OperationParameterDescription.ParamType.path;
                action.dataType = "string";
                action.schemaKey = "";
                action.required = true;
                operation.parameters.add(action);

                OperationParameterDescription message = new OperationParameterDescription();
                message.name = "message";
                message.paramType = OperationParameterDescription.ParamType.body;
                message.dataType = "Message";
                message.schemaKey = "com.wizy.model.mdm.Message";
                message.required = true;
                operation.parameters.add(message);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.Message";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.fcm.FirebaseResource#sendTaskToAllDevicesByFilter(java.lang.String,java.lang.String,java.lang.String,com.wizy.model.mdm.Message)";
            }
        },
        new StdEntityRoute<com.wizy.model.mdm.Message, restx.http.HttpStatus>("default#FirebaseResource#sendTaskToMultiDevicesByTopic",
                readerRegistry.<com.wizy.model.mdm.Message>build(com.wizy.model.mdm.Message.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/fcm/devices/topic/{topic}/action/{action}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.Message body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.sendTaskToMultiDevicesByTopic(
                        /* [PATH] topic */ match.getPathParam("topic"),
                        /* [PATH] action */ match.getPathParam("action"),
                        /* [BODY] message */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription topic = new OperationParameterDescription();
                topic.name = "topic";
                topic.paramType = OperationParameterDescription.ParamType.path;
                topic.dataType = "string";
                topic.schemaKey = "";
                topic.required = true;
                operation.parameters.add(topic);

                OperationParameterDescription action = new OperationParameterDescription();
                action.name = "action";
                action.paramType = OperationParameterDescription.ParamType.path;
                action.dataType = "string";
                action.schemaKey = "";
                action.required = true;
                operation.parameters.add(action);

                OperationParameterDescription message = new OperationParameterDescription();
                message.name = "message";
                message.paramType = OperationParameterDescription.ParamType.body;
                message.dataType = "Message";
                message.schemaKey = "com.wizy.model.mdm.Message";
                message.required = true;
                operation.parameters.add(message);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.Message";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.fcm.FirebaseResource#sendTaskToMultiDevicesByTopic(java.lang.String,java.lang.String,com.wizy.model.mdm.Message)";
            }
        },
        });
    }

}
