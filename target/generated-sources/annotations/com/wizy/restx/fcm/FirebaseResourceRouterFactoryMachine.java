package com.wizy.restx.fcm;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.fcm.FirebaseResourceRouter;

@Machine
public class FirebaseResourceRouterFactoryMachine extends SingleNameFactoryMachine<FirebaseResourceRouter> {
    public static final Name<FirebaseResourceRouter> NAME = Name.of(FirebaseResourceRouter.class, "FirebaseResourceRouter");

    public FirebaseResourceRouterFactoryMachine() {
        super(0, new StdMachineEngine<FirebaseResourceRouter>(NAME, 0, BoundlessComponentBox.FACTORY) {
private final Factory.Query<com.wizy.restx.fcm.FirebaseResource> resource = Factory.Query.byClass(com.wizy.restx.fcm.FirebaseResource.class).mandatory();
private final Factory.Query<restx.entity.EntityRequestBodyReaderRegistry> readerRegistry = Factory.Query.byClass(restx.entity.EntityRequestBodyReaderRegistry.class).mandatory();
private final Factory.Query<restx.entity.EntityResponseWriterRegistry> writerRegistry = Factory.Query.byClass(restx.entity.EntityResponseWriterRegistry.class).mandatory();
private final Factory.Query<restx.converters.MainStringConverter> converter = Factory.Query.byClass(restx.converters.MainStringConverter.class).mandatory();
private final Factory.Query<javax.validation.Validator> validator = Factory.Query.byClass(javax.validation.Validator.class).mandatory();
private final Factory.Query<restx.security.RestxSecurityManager> securityManager = Factory.Query.byClass(restx.security.RestxSecurityManager.class).mandatory();

            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(
resource,
readerRegistry,
writerRegistry,
converter,
validator,
securityManager
                ));
            }

            @Override
            protected FirebaseResourceRouter doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new FirebaseResourceRouter(
satisfiedBOM.getOne(resource).get().getComponent(),
satisfiedBOM.getOne(readerRegistry).get().getComponent(),
satisfiedBOM.getOne(writerRegistry).get().getComponent(),
satisfiedBOM.getOne(converter).get().getComponent(),
satisfiedBOM.getOne(validator).get().getComponent(),
satisfiedBOM.getOne(securityManager).get().getComponent()
                );
            }
        });
    }

}
