package com.wizy.restx.mdm;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.mdm.NetworkResources;

@Machine
public class NetworkResourcesFactoryMachine extends SingleNameFactoryMachine<NetworkResources> {
    public static final Name<NetworkResources> NAME = Name.of(NetworkResources.class, "NetworkResources");

    public NetworkResourcesFactoryMachine() {
        super(0, new StdMachineEngine<NetworkResources>(NAME, 0, BoundlessComponentBox.FACTORY) {


            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(

                ));
            }

            @Override
            protected NetworkResources doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new NetworkResources(

                );
            }
        });
    }

}
