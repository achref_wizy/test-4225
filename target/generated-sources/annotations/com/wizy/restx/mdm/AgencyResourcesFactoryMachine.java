package com.wizy.restx.mdm;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.mdm.AgencyResources;

@Machine
public class AgencyResourcesFactoryMachine extends SingleNameFactoryMachine<AgencyResources> {
    public static final Name<AgencyResources> NAME = Name.of(AgencyResources.class, "AgencyResources");

    public AgencyResourcesFactoryMachine() {
        super(0, new StdMachineEngine<AgencyResources>(NAME, 0, BoundlessComponentBox.FACTORY) {


            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(

                ));
            }

            @Override
            protected AgencyResources doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new AgencyResources(

                );
            }
        });
    }

}
