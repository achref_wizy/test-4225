package com.wizy.restx.mdm;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;

import restx.common.Types;
import restx.*;
import restx.entity.*;
import restx.http.*;
import restx.factory.*;
import restx.security.*;
import static restx.security.Permissions.*;
import restx.description.*;
import restx.converters.MainStringConverter;
import static restx.common.MorePreconditions.checkPresent;

import javax.validation.Validator;
import static restx.validation.Validations.checkValid;

import java.io.IOException;
import java.io.PrintWriter;

@Component(priority = 0)

public class NetworkResourcesRouter extends RestxRouter {

    public NetworkResourcesRouter(
                    final NetworkResources resource,
                    final EntityRequestBodyReaderRegistry readerRegistry,
                    final EntityResponseWriterRegistry writerRegistry,
                    final MainStringConverter converter,
                    final Validator validator,
                    final RestxSecurityManager securityManager) {
        super(
            "default", "NetworkResourcesRouter", new RestxRoute[] {
        new StdEntityRoute<com.wizy.model.mdm.agency.Wifi, restx.http.HttpStatus>("default#NetworkResources#addWifi",
                readerRegistry.<com.wizy.model.mdm.agency.Wifi>build(com.wizy.model.mdm.agency.Wifi.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/network/wifi/add"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.agency.Wifi body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.addWifi(
                        /* [BODY] wifi */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription wifi = new OperationParameterDescription();
                wifi.name = "wifi";
                wifi.paramType = OperationParameterDescription.ParamType.body;
                wifi.dataType = "Wifi";
                wifi.schemaKey = "com.wizy.model.mdm.agency.Wifi";
                wifi.required = true;
                operation.parameters.add(wifi);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.agency.Wifi";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.NetworkResources#addWifi(com.wizy.model.mdm.agency.Wifi)";
            }
        },
        new StdEntityRoute<com.wizy.model.mdm.agency.Wifi, restx.http.HttpStatus>("default#NetworkResources#updateWifi",
                readerRegistry.<com.wizy.model.mdm.agency.Wifi>build(com.wizy.model.mdm.agency.Wifi.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/network/wifi/update/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.agency.Wifi body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.updateWifi(
                        /* [PATH] id */ match.getPathParam("id"),
                        /* [BODY] wifi */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);

                OperationParameterDescription wifi = new OperationParameterDescription();
                wifi.name = "wifi";
                wifi.paramType = OperationParameterDescription.ParamType.body;
                wifi.dataType = "Wifi";
                wifi.schemaKey = "com.wizy.model.mdm.agency.Wifi";
                wifi.required = true;
                operation.parameters.add(wifi);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.agency.Wifi";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.NetworkResources#updateWifi(java.lang.String,com.wizy.model.mdm.agency.Wifi)";
            }
        },
        new StdEntityRoute<Void, com.wizy.model.mdm.agency.Wifi>("default#NetworkResources#getWifiById",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.model.mdm.agency.Wifi>build(com.wizy.model.mdm.agency.Wifi.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/network/wifi/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.model.mdm.agency.Wifi> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.getWifiById(
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "Wifi";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.model.mdm.agency.Wifi";
                operation.sourceLocation = "com.wizy.restx.mdm.NetworkResources#getWifiById(java.lang.String)";
            }
        },
        new StdEntityRoute<Void, java.util.List<com.wizy.model.mdm.agency.Wifi>>("default#NetworkResources#listAllWifi",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<java.util.List<com.wizy.model.mdm.agency.Wifi>>build(Types.newParameterizedType(java.util.List.class, com.wizy.model.mdm.agency.Wifi.class), Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/network/list"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<java.util.List<com.wizy.model.mdm.agency.Wifi>> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.listAllWifi(
                        
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                

                operation.responseClass = "Wifi>";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.mdm.NetworkResources#listAllWifi()";
            }
        },
        new StdEntityRoute<Void, java.util.List<com.wizy.model.mdm.agency.Wifi>>("default#NetworkResources#listWifiByAgencyId",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<java.util.List<com.wizy.model.mdm.agency.Wifi>>build(Types.newParameterizedType(java.util.List.class, com.wizy.model.mdm.agency.Wifi.class), Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/network/list/agency/{agencyid}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<java.util.List<com.wizy.model.mdm.agency.Wifi>> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.listWifiByAgencyId(
                        /* [PATH] agencyid */ match.getPathParam("agencyid")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription agencyid = new OperationParameterDescription();
                agencyid.name = "agencyid";
                agencyid.paramType = OperationParameterDescription.ParamType.path;
                agencyid.dataType = "string";
                agencyid.schemaKey = "";
                agencyid.required = true;
                operation.parameters.add(agencyid);


                operation.responseClass = "Wifi>";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.mdm.NetworkResources#listWifiByAgencyId(java.lang.String)";
            }
        },
        new StdEntityRoute<Void, restx.http.HttpStatus>("default#NetworkResources#deleteWifi",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("DELETE", "/secured/network/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.deleteWifi(
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.NetworkResources#deleteWifi(java.lang.String)";
            }
        },
        });
    }

}
