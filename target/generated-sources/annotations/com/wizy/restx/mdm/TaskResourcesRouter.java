package com.wizy.restx.mdm;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;

import restx.common.Types;
import restx.*;
import restx.entity.*;
import restx.http.*;
import restx.factory.*;
import restx.security.*;
import static restx.security.Permissions.*;
import restx.description.*;
import restx.converters.MainStringConverter;
import static restx.common.MorePreconditions.checkPresent;

import javax.validation.Validator;
import static restx.validation.Validations.checkValid;

import java.io.IOException;
import java.io.PrintWriter;

@Component(priority = 0)

public class TaskResourcesRouter extends RestxRouter {

    public TaskResourcesRouter(
                    final TaskResources resource,
                    final EntityRequestBodyReaderRegistry readerRegistry,
                    final EntityResponseWriterRegistry writerRegistry,
                    final MainStringConverter converter,
                    final Validator validator,
                    final RestxSecurityManager securityManager) {
        super(
            "default", "TaskResourcesRouter", new RestxRoute[] {
        new StdEntityRoute<com.wizy.model.mdm.Message, com.wizy.json.response.oneObject.TaskWithHttpStatus>("default#TaskResources#addTaskToOneDevice",
                readerRegistry.<com.wizy.model.mdm.Message>build(com.wizy.model.mdm.Message.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.TaskWithHttpStatus>build(com.wizy.json.response.oneObject.TaskWithHttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/task/add/id/{id}/{action}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.TaskWithHttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.Message body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.addTaskToOneDevice(
                        /* [PATH] id */ match.getPathParam("id"),
                        /* [PATH] action */ match.getPathParam("action"),
                        /* [BODY] message */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);

                OperationParameterDescription action = new OperationParameterDescription();
                action.name = "action";
                action.paramType = OperationParameterDescription.ParamType.path;
                action.dataType = "string";
                action.schemaKey = "";
                action.required = true;
                operation.parameters.add(action);

                OperationParameterDescription message = new OperationParameterDescription();
                message.name = "message";
                message.paramType = OperationParameterDescription.ParamType.body;
                message.dataType = "Message";
                message.schemaKey = "com.wizy.model.mdm.Message";
                message.required = true;
                operation.parameters.add(message);


                operation.responseClass = "TaskWithHttpStatus";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.Message";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.TaskWithHttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.TaskResources#addTaskToOneDevice(java.lang.String,java.lang.String,com.wizy.model.mdm.Message)";
            }
        },
        new StdEntityRoute<com.wizy.json.response.twoObjects.MessageMultipleDevice, com.wizy.json.response.twoObjects.MultipleTaskListResp>("default#TaskResources#addTaskToMultiDevicesByIds",
                readerRegistry.<com.wizy.json.response.twoObjects.MessageMultipleDevice>build(com.wizy.json.response.twoObjects.MessageMultipleDevice.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.twoObjects.MultipleTaskListResp>build(com.wizy.json.response.twoObjects.MultipleTaskListResp.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/task/add/ids/{action}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.twoObjects.MultipleTaskListResp> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.json.response.twoObjects.MessageMultipleDevice body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.addTaskToMultiDevicesByIds(
                        /* [PATH] action */ match.getPathParam("action"),
                        /* [BODY] payload */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription action = new OperationParameterDescription();
                action.name = "action";
                action.paramType = OperationParameterDescription.ParamType.path;
                action.dataType = "string";
                action.schemaKey = "";
                action.required = true;
                operation.parameters.add(action);

                OperationParameterDescription payload = new OperationParameterDescription();
                payload.name = "payload";
                payload.paramType = OperationParameterDescription.ParamType.body;
                payload.dataType = "MessageMultipleDevice";
                payload.schemaKey = "com.wizy.json.response.twoObjects.MessageMultipleDevice";
                payload.required = true;
                operation.parameters.add(payload);


                operation.responseClass = "MultipleTaskListResp";
                operation.inEntitySchemaKey = "com.wizy.json.response.twoObjects.MessageMultipleDevice";
                operation.outEntitySchemaKey = "com.wizy.json.response.twoObjects.MultipleTaskListResp";
                operation.sourceLocation = "com.wizy.restx.mdm.TaskResources#addTaskToMultiDevicesByIds(java.lang.String,com.wizy.json.response.twoObjects.MessageMultipleDevice)";
            }
        },
        new StdEntityRoute<com.wizy.model.mdm.Message, com.wizy.json.response.twoObjects.MultipleTaskListResp>("default#TaskResources#addTaskToAllDevice",
                readerRegistry.<com.wizy.model.mdm.Message>build(com.wizy.model.mdm.Message.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.twoObjects.MultipleTaskListResp>build(com.wizy.json.response.twoObjects.MultipleTaskListResp.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/task/add/all/{action}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.twoObjects.MultipleTaskListResp> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.Message body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.addTaskToAllDevice(
                        /* [PATH] action */ match.getPathParam("action"),
                        /* [BODY] message */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription action = new OperationParameterDescription();
                action.name = "action";
                action.paramType = OperationParameterDescription.ParamType.path;
                action.dataType = "string";
                action.schemaKey = "";
                action.required = true;
                operation.parameters.add(action);

                OperationParameterDescription message = new OperationParameterDescription();
                message.name = "message";
                message.paramType = OperationParameterDescription.ParamType.body;
                message.dataType = "Message";
                message.schemaKey = "com.wizy.model.mdm.Message";
                message.required = true;
                operation.parameters.add(message);


                operation.responseClass = "MultipleTaskListResp";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.Message";
                operation.outEntitySchemaKey = "com.wizy.json.response.twoObjects.MultipleTaskListResp";
                operation.sourceLocation = "com.wizy.restx.mdm.TaskResources#addTaskToAllDevice(java.lang.String,com.wizy.model.mdm.Message)";
            }
        },
        new StdEntityRoute<Void, com.wizy.json.mdm.TaskJson>("default#TaskResources#getTaskById",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.mdm.TaskJson>build(com.wizy.json.mdm.TaskJson.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/task/{taskid}/device/{deviceid}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.mdm.TaskJson> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.getTaskById(
                        /* [PATH] taskid */ match.getPathParam("taskid"),
                        /* [PATH] deviceid */ match.getPathParam("deviceid")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription taskid = new OperationParameterDescription();
                taskid.name = "taskid";
                taskid.paramType = OperationParameterDescription.ParamType.path;
                taskid.dataType = "string";
                taskid.schemaKey = "";
                taskid.required = true;
                operation.parameters.add(taskid);

                OperationParameterDescription deviceid = new OperationParameterDescription();
                deviceid.name = "deviceid";
                deviceid.paramType = OperationParameterDescription.ParamType.path;
                deviceid.dataType = "string";
                deviceid.schemaKey = "";
                deviceid.required = true;
                operation.parameters.add(deviceid);


                operation.responseClass = "TaskJson";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.mdm.TaskJson";
                operation.sourceLocation = "com.wizy.restx.mdm.TaskResources#getTaskById(java.lang.String,java.lang.String)";
            }
        },
        new StdEntityRoute<Void, restx.http.HttpStatus>("default#TaskResources#updateTaskStatusById",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/task/id/{id}/device/{deviceid}/status/{status}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.updateTaskStatusById(
                        /* [PATH] id */ match.getPathParam("id"),
                        /* [PATH] deviceid */ match.getPathParam("deviceid"),
                        /* [PATH] status */ match.getPathParam("status")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);

                OperationParameterDescription deviceid = new OperationParameterDescription();
                deviceid.name = "deviceid";
                deviceid.paramType = OperationParameterDescription.ParamType.path;
                deviceid.dataType = "string";
                deviceid.schemaKey = "";
                deviceid.required = true;
                operation.parameters.add(deviceid);

                OperationParameterDescription status = new OperationParameterDescription();
                status.name = "status";
                status.paramType = OperationParameterDescription.ParamType.path;
                status.dataType = "string";
                status.schemaKey = "";
                status.required = true;
                operation.parameters.add(status);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.TaskResources#updateTaskStatusById(java.lang.String,java.lang.String,java.lang.String)";
            }
        },
        new StdEntityRoute<Void, java.util.List<com.wizy.json.mdm.TaskJson>>("default#TaskResources#listTasks",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<java.util.List<com.wizy.json.mdm.TaskJson>>build(Types.newParameterizedType(java.util.List.class, com.wizy.json.mdm.TaskJson.class), Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/task"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<java.util.List<com.wizy.json.mdm.TaskJson>> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.listTasks(
                        
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                

                operation.responseClass = "TaskJson>";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.mdm.TaskResources#listTasks()";
            }
        },
        new StdEntityRoute<Void, restx.http.HttpStatus>("default#TaskResources#deleteTask",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("DELETE", "/secured/task/id/{taskid}/device/{device}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.deleteTask(
                        /* [PATH] taskid */ match.getPathParam("taskid"),
                        /* [PATH] device */ match.getPathParam("device")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription taskid = new OperationParameterDescription();
                taskid.name = "taskid";
                taskid.paramType = OperationParameterDescription.ParamType.path;
                taskid.dataType = "string";
                taskid.schemaKey = "";
                taskid.required = true;
                operation.parameters.add(taskid);

                OperationParameterDescription device = new OperationParameterDescription();
                device.name = "device";
                device.paramType = OperationParameterDescription.ParamType.path;
                device.dataType = "string";
                device.schemaKey = "";
                device.required = true;
                operation.parameters.add(device);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.TaskResources#deleteTask(java.lang.String,java.lang.String)";
            }
        },
        new StdEntityRoute<Void, com.wizy.json.response.twoObjects.TaskDeviceHttpstatusResp>("default#TaskResources#getAllTaskByDeviceId",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.twoObjects.TaskDeviceHttpstatusResp>build(com.wizy.json.response.twoObjects.TaskDeviceHttpstatusResp.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/task/device/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.twoObjects.TaskDeviceHttpstatusResp> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.getAllTaskByDeviceId(
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "TaskDeviceHttpstatusResp";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.twoObjects.TaskDeviceHttpstatusResp";
                operation.sourceLocation = "com.wizy.restx.mdm.TaskResources#getAllTaskByDeviceId(java.lang.String)";
            }
        },
        new StdEntityRoute<Void, restx.http.HttpStatus>("default#TaskResources#updateAllDevices",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/task/all/action/{action}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.updateAllDevices(
                        /* [PATH] action */ match.getPathParam("action"),
                        /* [QUERY] message */ converter.convert(checkPresent(request.getQueryParam("message"), "query param message is required"), com.wizy.model.mdm.Message.class)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription action = new OperationParameterDescription();
                action.name = "action";
                action.paramType = OperationParameterDescription.ParamType.path;
                action.dataType = "string";
                action.schemaKey = "";
                action.required = true;
                operation.parameters.add(action);

                OperationParameterDescription message = new OperationParameterDescription();
                message.name = "message";
                message.paramType = OperationParameterDescription.ParamType.query;
                message.dataType = "Message";
                message.schemaKey = "com.wizy.model.mdm.Message";
                message.required = true;
                operation.parameters.add(message);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.TaskResources#updateAllDevices(java.lang.String,com.wizy.model.mdm.Message)";
            }
        },
        });
    }

}
