package com.wizy.restx.mdm;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.mdm.DeviceResources;

@Machine
public class DeviceResourcesFactoryMachine extends SingleNameFactoryMachine<DeviceResources> {
    public static final Name<DeviceResources> NAME = Name.of(DeviceResources.class, "DeviceResources");

    public DeviceResourcesFactoryMachine() {
        super(0, new StdMachineEngine<DeviceResources>(NAME, 0, BoundlessComponentBox.FACTORY) {


            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(

                ));
            }

            @Override
            protected DeviceResources doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new DeviceResources(

                );
            }
        });
    }

}
