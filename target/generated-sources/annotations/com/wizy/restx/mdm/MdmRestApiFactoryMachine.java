package com.wizy.restx.mdm;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.mdm.MdmRestApi;

@Machine
public class MdmRestApiFactoryMachine extends SingleNameFactoryMachine<MdmRestApi> {
    public static final Name<MdmRestApi> NAME = Name.of(MdmRestApi.class, "MdmRestApi");

    public MdmRestApiFactoryMachine() {
        super(0, new StdMachineEngine<MdmRestApi>(NAME, 0, BoundlessComponentBox.FACTORY) {


            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(

                ));
            }

            @Override
            protected MdmRestApi doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new MdmRestApi(

                );
            }
        });
    }

}
