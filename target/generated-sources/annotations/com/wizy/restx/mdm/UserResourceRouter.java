package com.wizy.restx.mdm;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;

import restx.common.Types;
import restx.*;
import restx.entity.*;
import restx.http.*;
import restx.factory.*;
import restx.security.*;
import static restx.security.Permissions.*;
import restx.description.*;
import restx.converters.MainStringConverter;
import static restx.common.MorePreconditions.checkPresent;

import javax.validation.Validator;
import static restx.validation.Validations.checkValid;

import java.io.IOException;
import java.io.PrintWriter;

@Component(priority = 0)

public class UserResourceRouter extends RestxRouter {

    public UserResourceRouter(
                    final UserResource resource,
                    final EntityRequestBodyReaderRegistry readerRegistry,
                    final EntityResponseWriterRegistry writerRegistry,
                    final MainStringConverter converter,
                    final Validator validator,
                    final RestxSecurityManager securityManager) {
        super(
            "default", "UserResourceRouter", new RestxRoute[] {
        new StdEntityRoute<Void, com.wizy.json.response.oneObject.UserResponse>("default#UserResource#getUser",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.UserResponse>build(com.wizy.json.response.oneObject.UserResponse.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/users/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.UserResponse> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.getUser(
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "UserResponse";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.UserResponse";
                operation.sourceLocation = "com.wizy.restx.mdm.UserResource#getUser(java.lang.String)";
            }
        },
        new StdEntityRoute<com.wizy.model.admin.User, restx.http.HttpStatus>("default#UserResource#saveOrUpdateUser",
                readerRegistry.<com.wizy.model.admin.User>build(com.wizy.model.admin.User.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/users"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.admin.User body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.saveOrUpdateUser(
                        /* [BODY] user */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription user = new OperationParameterDescription();
                user.name = "user";
                user.paramType = OperationParameterDescription.ParamType.body;
                user.dataType = "User";
                user.schemaKey = "com.wizy.model.admin.User";
                user.required = true;
                operation.parameters.add(user);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "com.wizy.model.admin.User";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.UserResource#saveOrUpdateUser(com.wizy.model.admin.User)";
            }
        },
        new StdEntityRoute<com.wizy.json.admin.UserJson, restx.http.HttpStatus>("default#UserResource#updateUserJson",
                readerRegistry.<com.wizy.json.admin.UserJson>build(com.wizy.json.admin.UserJson.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/users/update"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.json.admin.UserJson body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.updateUserJson(
                        /* [BODY] user */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription user = new OperationParameterDescription();
                user.name = "user";
                user.paramType = OperationParameterDescription.ParamType.body;
                user.dataType = "UserJson";
                user.schemaKey = "com.wizy.json.admin.UserJson";
                user.required = true;
                operation.parameters.add(user);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "com.wizy.json.admin.UserJson";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.UserResource#updateUserJson(com.wizy.json.admin.UserJson)";
            }
        },
        new StdEntityRoute<Void, java.util.List<com.wizy.json.admin.UserJson>>("default#UserResource#listUsers",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<java.util.List<com.wizy.json.admin.UserJson>>build(Types.newParameterizedType(java.util.List.class, com.wizy.json.admin.UserJson.class), Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/users"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<java.util.List<com.wizy.json.admin.UserJson>> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.listUsers(
                        
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                

                operation.responseClass = "UserJson>";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.mdm.UserResource#listUsers()";
            }
        },
        new StdEntityRoute<Void, com.wizy.json.JsonCollection<com.wizy.model.admin.User>>("default#UserResource#listUsersPagination",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.JsonCollection<com.wizy.model.admin.User>>build(Types.newParameterizedType(com.wizy.json.JsonCollection.class, com.wizy.model.admin.User.class), Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/users/all/pagination"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.JsonCollection<com.wizy.model.admin.User>> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.listUsersPagination(
                        /* [QUERY] maxResults */ converter.convert(request.getQueryParam("maxResults"), java.lang.Integer.class),
                        /* [QUERY] pageToken */ request.getQueryParam("pageToken")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription maxResults = new OperationParameterDescription();
                maxResults.name = "maxResults";
                maxResults.paramType = OperationParameterDescription.ParamType.query;
                maxResults.dataType = "int";
                maxResults.schemaKey = "";
                maxResults.required = false;
                operation.parameters.add(maxResults);

                OperationParameterDescription pageToken = new OperationParameterDescription();
                pageToken.name = "pageToken";
                pageToken.paramType = OperationParameterDescription.ParamType.query;
                pageToken.dataType = "string";
                pageToken.schemaKey = "";
                pageToken.required = false;
                operation.parameters.add(pageToken);


                operation.responseClass = "User>";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.JsonCollection<com.wizy.model.admin.User>";
                operation.sourceLocation = "com.wizy.restx.mdm.UserResource#listUsersPagination(com.google.common.base.Optional<java.lang.Integer>,com.google.common.base.Optional<java.lang.String>)";
            }
        },
        new StdEntityRoute<Void, com.wizy.json.response.oneObject.LoginResponse>("default#UserResource#login",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.LoginResponse>build(com.wizy.json.response.oneObject.LoginResponse.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/users/login/{email}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.LoginResponse> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.login(
                        /* [PATH] email */ match.getPathParam("email")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription email = new OperationParameterDescription();
                email.name = "email";
                email.paramType = OperationParameterDescription.ParamType.path;
                email.dataType = "string";
                email.schemaKey = "";
                email.required = true;
                operation.parameters.add(email);


                operation.responseClass = "LoginResponse";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.LoginResponse";
                operation.sourceLocation = "com.wizy.restx.mdm.UserResource#login(java.lang.String)";
            }
        },
        new StdEntityRoute<com.wizy.restx.mdm.UserResource.Invitation, restx.http.HttpStatus>("default#UserResource#inviteUser",
                readerRegistry.<com.wizy.restx.mdm.UserResource.Invitation>build(com.wizy.restx.mdm.UserResource.Invitation.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/users/invite"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.restx.mdm.UserResource.Invitation body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.inviteUser(
                        /* [BODY] invit */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription invit = new OperationParameterDescription();
                invit.name = "invit";
                invit.paramType = OperationParameterDescription.ParamType.body;
                invit.dataType = "Invitation";
                invit.schemaKey = "com.wizy.restx.mdm.UserResource.Invitation";
                invit.required = true;
                operation.parameters.add(invit);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "com.wizy.restx.mdm.UserResource.Invitation";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.UserResource#inviteUser(com.wizy.restx.mdm.UserResource.Invitation)";
            }
        },
        new StdEntityRoute<Void, restx.http.HttpStatus>("default#UserResource#deleteUser",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("DELETE", "/secured/users/id/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.deleteUser(
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.UserResource#deleteUser(java.lang.String)";
            }
        },
        });
    }

}
