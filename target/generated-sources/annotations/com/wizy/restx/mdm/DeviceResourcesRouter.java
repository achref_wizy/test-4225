package com.wizy.restx.mdm;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;

import restx.common.Types;
import restx.*;
import restx.entity.*;
import restx.http.*;
import restx.factory.*;
import restx.security.*;
import static restx.security.Permissions.*;
import restx.description.*;
import restx.converters.MainStringConverter;
import static restx.common.MorePreconditions.checkPresent;

import javax.validation.Validator;
import static restx.validation.Validations.checkValid;

import java.io.IOException;
import java.io.PrintWriter;

@Component(priority = 0)

public class DeviceResourcesRouter extends RestxRouter {

    public DeviceResourcesRouter(
                    final DeviceResources resource,
                    final EntityRequestBodyReaderRegistry readerRegistry,
                    final EntityResponseWriterRegistry writerRegistry,
                    final MainStringConverter converter,
                    final Validator validator,
                    final RestxSecurityManager securityManager) {
        super(
            "default", "DeviceResourcesRouter", new RestxRoute[] {
        new StdEntityRoute<com.wizy.model.mdm.device.Device, com.wizy.json.response.oneObject.HttpStatusResponse>("default#DeviceResources#addDevice",
                readerRegistry.<com.wizy.model.mdm.device.Device>build(com.wizy.model.mdm.device.Device.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.HttpStatusResponse>build(com.wizy.json.response.oneObject.HttpStatusResponse.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/device/add"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.HttpStatusResponse> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.device.Device body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.addDevice(
                        /* [BODY] device */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription device = new OperationParameterDescription();
                device.name = "device";
                device.paramType = OperationParameterDescription.ParamType.body;
                device.dataType = "Device";
                device.schemaKey = "com.wizy.model.mdm.device.Device";
                device.required = true;
                operation.parameters.add(device);


                operation.responseClass = "HttpStatusResponse";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.device.Device";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.HttpStatusResponse";
                operation.sourceLocation = "com.wizy.restx.mdm.DeviceResources#addDevice(com.wizy.model.mdm.device.Device)";
            }
        },
        new StdEntityRoute<java.util.List<com.wizy.model.mdm.device.Device>, com.wizy.json.response.twoObjects.MultipleDeviceListResp>("default#DeviceResources#addListDevice",
                readerRegistry.<java.util.List<com.wizy.model.mdm.device.Device>>build(Types.newParameterizedType(java.util.List.class, com.wizy.model.mdm.device.Device.class), Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.twoObjects.MultipleDeviceListResp>build(com.wizy.json.response.twoObjects.MultipleDeviceListResp.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/device/add/list"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.twoObjects.MultipleDeviceListResp> doRoute(RestxRequest request, RestxRequestMatch match, java.util.List<com.wizy.model.mdm.device.Device> body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.addListDevice(
                        /* [BODY] devices */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription devices = new OperationParameterDescription();
                devices.name = "devices";
                devices.paramType = OperationParameterDescription.ParamType.body;
                devices.dataType = "Device>";
                devices.schemaKey = "";
                devices.required = true;
                operation.parameters.add(devices);


                operation.responseClass = "MultipleDeviceListResp";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.twoObjects.MultipleDeviceListResp";
                operation.sourceLocation = "com.wizy.restx.mdm.DeviceResources#addListDevice(java.util.List<com.wizy.model.mdm.device.Device>)";
            }
        },
        new StdEntityRoute<Void, com.wizy.json.response.oneObject.DeviceWithStatus>("default#DeviceResources#getDeviceById",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.DeviceWithStatus>build(com.wizy.json.response.oneObject.DeviceWithStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/device/id/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.DeviceWithStatus> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.getDeviceById(
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "DeviceWithStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.DeviceWithStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.DeviceResources#getDeviceById(java.lang.String)";
            }
        },
        new StdEntityRoute<java.util.List<java.lang.String>, com.wizy.json.response.oneObject.DeviceListWithStatus>("default#DeviceResources#getDeviceByListId",
                readerRegistry.<java.util.List<java.lang.String>>build(Types.newParameterizedType(java.util.List.class, java.lang.String.class), Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.DeviceListWithStatus>build(com.wizy.json.response.oneObject.DeviceListWithStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/device/ids"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.DeviceListWithStatus> doRoute(RestxRequest request, RestxRequestMatch match, java.util.List<java.lang.String> body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.getDeviceByListId(
                        /* [BODY] ids */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription ids = new OperationParameterDescription();
                ids.name = "ids";
                ids.paramType = OperationParameterDescription.ParamType.body;
                ids.dataType = "String>";
                ids.schemaKey = "";
                ids.required = true;
                operation.parameters.add(ids);


                operation.responseClass = "DeviceListWithStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.DeviceListWithStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.DeviceResources#getDeviceByListId(java.util.List<java.lang.String>)";
            }
        },
        new StdEntityRoute<Void, java.util.List<com.wizy.model.mdm.device.Device>>("default#DeviceResources#listDevices",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<java.util.List<com.wizy.model.mdm.device.Device>>build(Types.newParameterizedType(java.util.List.class, com.wizy.model.mdm.device.Device.class), Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/device"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<java.util.List<com.wizy.model.mdm.device.Device>> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.listDevices(
                        
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                

                operation.responseClass = "Device>";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.mdm.DeviceResources#listDevices()";
            }
        },
        new StdEntityRoute<Void, java.util.List<com.wizy.model.mdm.device.Device>>("default#DeviceResources#listDevicesByFilter",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<java.util.List<com.wizy.model.mdm.device.Device>>build(Types.newParameterizedType(java.util.List.class, com.wizy.model.mdm.device.Device.class), Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/device/filter/name/{name}/value/{value}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<java.util.List<com.wizy.model.mdm.device.Device>> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.listDevicesByFilter(
                        /* [PATH] name */ match.getPathParam("name"),
                        /* [PATH] value */ match.getPathParam("value")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription name = new OperationParameterDescription();
                name.name = "name";
                name.paramType = OperationParameterDescription.ParamType.path;
                name.dataType = "string";
                name.schemaKey = "";
                name.required = true;
                operation.parameters.add(name);

                OperationParameterDescription value = new OperationParameterDescription();
                value.name = "value";
                value.paramType = OperationParameterDescription.ParamType.path;
                value.dataType = "string";
                value.schemaKey = "";
                value.required = true;
                operation.parameters.add(value);


                operation.responseClass = "Device>";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.mdm.DeviceResources#listDevicesByFilter(java.lang.String,java.lang.String)";
            }
        },
        new StdEntityRoute<Void, java.util.List<com.wizy.model.mdm.device.Device>>("default#DeviceResources#listDevicesBy2Filters",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<java.util.List<com.wizy.model.mdm.device.Device>>build(Types.newParameterizedType(java.util.List.class, com.wizy.model.mdm.device.Device.class), Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/device/filter/name1/{name1}/value1/{value1}/name2/{name2}/value2/{value2}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<java.util.List<com.wizy.model.mdm.device.Device>> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.listDevicesBy2Filters(
                        /* [PATH] name1 */ match.getPathParam("name1"),
                        /* [PATH] value1 */ match.getPathParam("value1"),
                        /* [PATH] name2 */ match.getPathParam("name2"),
                        /* [PATH] value2 */ match.getPathParam("value2")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription name1 = new OperationParameterDescription();
                name1.name = "name1";
                name1.paramType = OperationParameterDescription.ParamType.path;
                name1.dataType = "string";
                name1.schemaKey = "";
                name1.required = true;
                operation.parameters.add(name1);

                OperationParameterDescription value1 = new OperationParameterDescription();
                value1.name = "value1";
                value1.paramType = OperationParameterDescription.ParamType.path;
                value1.dataType = "string";
                value1.schemaKey = "";
                value1.required = true;
                operation.parameters.add(value1);

                OperationParameterDescription name2 = new OperationParameterDescription();
                name2.name = "name2";
                name2.paramType = OperationParameterDescription.ParamType.path;
                name2.dataType = "string";
                name2.schemaKey = "";
                name2.required = true;
                operation.parameters.add(name2);

                OperationParameterDescription value2 = new OperationParameterDescription();
                value2.name = "value2";
                value2.paramType = OperationParameterDescription.ParamType.path;
                value2.dataType = "string";
                value2.schemaKey = "";
                value2.required = true;
                operation.parameters.add(value2);


                operation.responseClass = "Device>";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.mdm.DeviceResources#listDevicesBy2Filters(java.lang.String,java.lang.String,java.lang.String,java.lang.String)";
            }
        },
        new StdEntityRoute<Void, java.lang.Integer>("default#DeviceResources#countAllDevices",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<java.lang.Integer>build(java.lang.Integer.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/device/countall"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<java.lang.Integer> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.countAllDevices(
                        
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                

                operation.responseClass = "int";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.mdm.DeviceResources#countAllDevices()";
            }
        },
        new StdEntityRoute<Void, com.wizy.json.JsonCollection<com.wizy.model.mdm.device.Device>>("default#DeviceResources#listDevicesPagination",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.JsonCollection<com.wizy.model.mdm.device.Device>>build(Types.newParameterizedType(com.wizy.json.JsonCollection.class, com.wizy.model.mdm.device.Device.class), Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/device/pagination"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.JsonCollection<com.wizy.model.mdm.device.Device>> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.listDevicesPagination(
                        /* [QUERY] maxResults */ converter.convert(request.getQueryParam("maxResults"), java.lang.Integer.class),
                        /* [QUERY] pageToken */ request.getQueryParam("pageToken")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription maxResults = new OperationParameterDescription();
                maxResults.name = "maxResults";
                maxResults.paramType = OperationParameterDescription.ParamType.query;
                maxResults.dataType = "int";
                maxResults.schemaKey = "";
                maxResults.required = false;
                operation.parameters.add(maxResults);

                OperationParameterDescription pageToken = new OperationParameterDescription();
                pageToken.name = "pageToken";
                pageToken.paramType = OperationParameterDescription.ParamType.query;
                pageToken.dataType = "string";
                pageToken.schemaKey = "";
                pageToken.required = false;
                operation.parameters.add(pageToken);


                operation.responseClass = "Device>";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.JsonCollection<com.wizy.model.mdm.device.Device>";
                operation.sourceLocation = "com.wizy.restx.mdm.DeviceResources#listDevicesPagination(com.google.common.base.Optional<java.lang.Integer>,com.google.common.base.Optional<java.lang.String>)";
            }
        },
        new StdEntityRoute<Void, com.wizy.json.JsonCollection<com.wizy.model.mdm.device.Device>>("default#DeviceResources#listDevicesByFilterPagination",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.JsonCollection<com.wizy.model.mdm.device.Device>>build(Types.newParameterizedType(com.wizy.json.JsonCollection.class, com.wizy.model.mdm.device.Device.class), Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/device/filter/name/{name}/value/{value}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.JsonCollection<com.wizy.model.mdm.device.Device>> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.listDevicesByFilterPagination(
                        /* [PATH] name */ match.getPathParam("name"),
                        /* [PATH] value */ match.getPathParam("value"),
                        /* [QUERY] maxResults */ converter.convert(request.getQueryParam("maxResults"), java.lang.Integer.class),
                        /* [QUERY] pageToken */ request.getQueryParam("pageToken")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription name = new OperationParameterDescription();
                name.name = "name";
                name.paramType = OperationParameterDescription.ParamType.path;
                name.dataType = "string";
                name.schemaKey = "";
                name.required = true;
                operation.parameters.add(name);

                OperationParameterDescription value = new OperationParameterDescription();
                value.name = "value";
                value.paramType = OperationParameterDescription.ParamType.path;
                value.dataType = "string";
                value.schemaKey = "";
                value.required = true;
                operation.parameters.add(value);

                OperationParameterDescription maxResults = new OperationParameterDescription();
                maxResults.name = "maxResults";
                maxResults.paramType = OperationParameterDescription.ParamType.query;
                maxResults.dataType = "int";
                maxResults.schemaKey = "";
                maxResults.required = false;
                operation.parameters.add(maxResults);

                OperationParameterDescription pageToken = new OperationParameterDescription();
                pageToken.name = "pageToken";
                pageToken.paramType = OperationParameterDescription.ParamType.query;
                pageToken.dataType = "string";
                pageToken.schemaKey = "";
                pageToken.required = false;
                operation.parameters.add(pageToken);


                operation.responseClass = "Device>";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.JsonCollection<com.wizy.model.mdm.device.Device>";
                operation.sourceLocation = "com.wizy.restx.mdm.DeviceResources#listDevicesByFilterPagination(java.lang.String,java.lang.String,com.google.common.base.Optional<java.lang.Integer>,com.google.common.base.Optional<java.lang.String>)";
            }
        },
        new StdEntityRoute<com.wizy.model.mdm.device.Device, com.wizy.json.response.oneObject.HttpStatusResponse>("default#DeviceResources#updateDevice",
                readerRegistry.<com.wizy.model.mdm.device.Device>build(com.wizy.model.mdm.device.Device.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.HttpStatusResponse>build(com.wizy.json.response.oneObject.HttpStatusResponse.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/device/update/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.HttpStatusResponse> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.device.Device body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.updateDevice(
                        /* [BODY] device */ checkValid(validator, body),
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription device = new OperationParameterDescription();
                device.name = "device";
                device.paramType = OperationParameterDescription.ParamType.body;
                device.dataType = "Device";
                device.schemaKey = "com.wizy.model.mdm.device.Device";
                device.required = true;
                operation.parameters.add(device);

                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "HttpStatusResponse";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.device.Device";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.HttpStatusResponse";
                operation.sourceLocation = "com.wizy.restx.mdm.DeviceResources#updateDevice(com.wizy.model.mdm.device.Device,java.lang.String)";
            }
        },
        new StdEntityRoute<com.wizy.model.mdm.device.Device, com.wizy.json.response.oneObject.HttpStatusResponse>("default#DeviceResources#patchDevice",
                readerRegistry.<com.wizy.model.mdm.device.Device>build(com.wizy.model.mdm.device.Device.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.HttpStatusResponse>build(com.wizy.json.response.oneObject.HttpStatusResponse.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/device/patch/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.HttpStatusResponse> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.device.Device body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.patchDevice(
                        /* [BODY] device */ checkValid(validator, body),
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription device = new OperationParameterDescription();
                device.name = "device";
                device.paramType = OperationParameterDescription.ParamType.body;
                device.dataType = "Device";
                device.schemaKey = "com.wizy.model.mdm.device.Device";
                device.required = true;
                operation.parameters.add(device);

                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "HttpStatusResponse";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.device.Device";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.HttpStatusResponse";
                operation.sourceLocation = "com.wizy.restx.mdm.DeviceResources#patchDevice(com.wizy.model.mdm.device.Device,java.lang.String)";
            }
        },
        new StdEntityRoute<com.wizy.model.mdm.device.Device, restx.http.HttpStatus>("default#DeviceResources#AcitivateDevice",
                readerRegistry.<com.wizy.model.mdm.device.Device>build(com.wizy.model.mdm.device.Device.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/device/register/agency/{agencycode}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.device.Device body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.AcitivateDevice(
                        /* [PATH] agencycode */ match.getPathParam("agencycode"),
                        /* [BODY] device */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription agencycode = new OperationParameterDescription();
                agencycode.name = "agencycode";
                agencycode.paramType = OperationParameterDescription.ParamType.path;
                agencycode.dataType = "string";
                agencycode.schemaKey = "";
                agencycode.required = true;
                operation.parameters.add(agencycode);

                OperationParameterDescription device = new OperationParameterDescription();
                device.name = "device";
                device.paramType = OperationParameterDescription.ParamType.body;
                device.dataType = "Device";
                device.schemaKey = "com.wizy.model.mdm.device.Device";
                device.required = true;
                operation.parameters.add(device);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.device.Device";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.DeviceResources#AcitivateDevice(java.lang.String,com.wizy.model.mdm.device.Device)";
            }
        },
        new StdEntityRoute<java.util.List<java.lang.String>, restx.http.HttpStatus>("default#DeviceResources#changeDeviceListStatus",
                readerRegistry.<java.util.List<java.lang.String>>build(Types.newParameterizedType(java.util.List.class, java.lang.String.class), Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/device/list/patch/status/{status}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, java.util.List<java.lang.String> body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.changeDeviceListStatus(
                        /* [PATH] status */ match.getPathParam("status"),
                        /* [BODY] ids */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription status = new OperationParameterDescription();
                status.name = "status";
                status.paramType = OperationParameterDescription.ParamType.path;
                status.dataType = "string";
                status.schemaKey = "";
                status.required = true;
                operation.parameters.add(status);

                OperationParameterDescription ids = new OperationParameterDescription();
                ids.name = "ids";
                ids.paramType = OperationParameterDescription.ParamType.body;
                ids.dataType = "String>";
                ids.schemaKey = "";
                ids.required = true;
                operation.parameters.add(ids);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.DeviceResources#changeDeviceListStatus(java.lang.String,java.util.List<java.lang.String>)";
            }
        },
        new StdEntityRoute<Void, restx.http.HttpStatus>("default#DeviceResources#deleteDevice",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("DELETE", "/secured/device/id/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.deleteDevice(
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.DeviceResources#deleteDevice(java.lang.String)";
            }
        },
        });
    }

}
