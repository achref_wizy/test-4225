package com.wizy.restx.mdm;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;

import restx.common.Types;
import restx.*;
import restx.entity.*;
import restx.http.*;
import restx.factory.*;
import restx.security.*;
import static restx.security.Permissions.*;
import restx.description.*;
import restx.converters.MainStringConverter;
import static restx.common.MorePreconditions.checkPresent;

import javax.validation.Validator;
import static restx.validation.Validations.checkValid;

import java.io.IOException;
import java.io.PrintWriter;

@Component(priority = 0)

public class MdmRestApiRouter extends RestxRouter {

    public MdmRestApiRouter(
                    final MdmRestApi resource,
                    final EntityRequestBodyReaderRegistry readerRegistry,
                    final EntityResponseWriterRegistry writerRegistry,
                    final MainStringConverter converter,
                    final Validator validator,
                    final RestxSecurityManager securityManager) {
        super(
            "default", "MdmRestApiRouter", new RestxRoute[] {
        new StdEntityRoute<Void, com.wizy.json.response.multiObjects.DeviceAgencyWifiResp>("default#MdmRestApi#synchronizeByDeviceId",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.multiObjects.DeviceAgencyWifiResp>build(com.wizy.json.response.multiObjects.DeviceAgencyWifiResp.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/keyaccess/mdm/synchronize/device/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.multiObjects.DeviceAgencyWifiResp> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.synchronizeByDeviceId(
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "DeviceAgencyWifiResp";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.multiObjects.DeviceAgencyWifiResp";
                operation.sourceLocation = "com.wizy.restx.mdm.MdmRestApi#synchronizeByDeviceId(java.lang.String)";
            }
        },
        new StdEntityRoute<Void, restx.http.HttpStatus>("default#MdmRestApi#InviteUser",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/keyaccess/mdm/admin/{email}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.InviteUser(
                        /* [PATH] email */ match.getPathParam("email")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription email = new OperationParameterDescription();
                email.name = "email";
                email.paramType = OperationParameterDescription.ParamType.path;
                email.dataType = "string";
                email.schemaKey = "";
                email.required = true;
                operation.parameters.add(email);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.MdmRestApi#InviteUser(java.lang.String)";
            }
        },
        new StdEntityRoute<Void, com.wizy.json.response.oneObject.DeviceWithStatus>("default#MdmRestApi#getDeviceById",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.DeviceWithStatus>build(com.wizy.json.response.oneObject.DeviceWithStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/keyaccess/mdm/device/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.DeviceWithStatus> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.getDeviceById(
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "DeviceWithStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.DeviceWithStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.MdmRestApi#getDeviceById(java.lang.String)";
            }
        },
        new StdEntityRoute<Void, com.wizy.json.response.oneObject.StringToJsonResp>("default#MdmRestApi#getAgencyIdByDeviceId",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.StringToJsonResp>build(com.wizy.json.response.oneObject.StringToJsonResp.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/keyaccess/mdm/device/{id}/agencyid"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.StringToJsonResp> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.getAgencyIdByDeviceId(
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "StringToJsonResp";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.StringToJsonResp";
                operation.sourceLocation = "com.wizy.restx.mdm.MdmRestApi#getAgencyIdByDeviceId(java.lang.String)";
            }
        },
        new StdEntityRoute<com.wizy.model.mdm.device.Device, com.wizy.json.response.oneObject.HttpStatusResponse>("default#MdmRestApi#masteriseDevice",
                readerRegistry.<com.wizy.model.mdm.device.Device>build(com.wizy.model.mdm.device.Device.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.HttpStatusResponse>build(com.wizy.json.response.oneObject.HttpStatusResponse.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/keyaccess/mdm/device/masterise"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.HttpStatusResponse> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.device.Device body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.masteriseDevice(
                        /* [BODY] device */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription device = new OperationParameterDescription();
                device.name = "device";
                device.paramType = OperationParameterDescription.ParamType.body;
                device.dataType = "Device";
                device.schemaKey = "com.wizy.model.mdm.device.Device";
                device.required = true;
                operation.parameters.add(device);


                operation.responseClass = "HttpStatusResponse";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.device.Device";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.HttpStatusResponse";
                operation.sourceLocation = "com.wizy.restx.mdm.MdmRestApi#masteriseDevice(com.wizy.model.mdm.device.Device)";
            }
        },
        new StdEntityRoute<com.wizy.model.mdm.device.Device, com.wizy.json.response.oneObject.HttpStatusResponse>("default#MdmRestApi#patchDevice",
                readerRegistry.<com.wizy.model.mdm.device.Device>build(com.wizy.model.mdm.device.Device.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.HttpStatusResponse>build(com.wizy.json.response.oneObject.HttpStatusResponse.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/keyaccess/mdm/device/patch/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.HttpStatusResponse> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.device.Device body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.patchDevice(
                        /* [BODY] device */ checkValid(validator, body),
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription device = new OperationParameterDescription();
                device.name = "device";
                device.paramType = OperationParameterDescription.ParamType.body;
                device.dataType = "Device";
                device.schemaKey = "com.wizy.model.mdm.device.Device";
                device.required = true;
                operation.parameters.add(device);

                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "HttpStatusResponse";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.device.Device";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.HttpStatusResponse";
                operation.sourceLocation = "com.wizy.restx.mdm.MdmRestApi#patchDevice(com.wizy.model.mdm.device.Device,java.lang.String)";
            }
        },
        new StdEntityRoute<com.wizy.model.mdm.device.Device, com.wizy.json.response.oneObject.HttpStatusResponse>("default#MdmRestApi#patchDeviceAndUpdateStatus",
                readerRegistry.<com.wizy.model.mdm.device.Device>build(com.wizy.model.mdm.device.Device.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.HttpStatusResponse>build(com.wizy.json.response.oneObject.HttpStatusResponse.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/keyaccess/mdm/device/update/{id}/task/{task}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.HttpStatusResponse> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.device.Device body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.patchDeviceAndUpdateStatus(
                        /* [BODY] device */ checkValid(validator, body),
                        /* [PATH] id */ match.getPathParam("id"),
                        /* [PATH] task */ match.getPathParam("task")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription device = new OperationParameterDescription();
                device.name = "device";
                device.paramType = OperationParameterDescription.ParamType.body;
                device.dataType = "Device";
                device.schemaKey = "com.wizy.model.mdm.device.Device";
                device.required = true;
                operation.parameters.add(device);

                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);

                OperationParameterDescription task = new OperationParameterDescription();
                task.name = "task";
                task.paramType = OperationParameterDescription.ParamType.path;
                task.dataType = "string";
                task.schemaKey = "";
                task.required = true;
                operation.parameters.add(task);


                operation.responseClass = "HttpStatusResponse";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.device.Device";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.HttpStatusResponse";
                operation.sourceLocation = "com.wizy.restx.mdm.MdmRestApi#patchDeviceAndUpdateStatus(com.wizy.model.mdm.device.Device,java.lang.String,java.lang.String)";
            }
        },
        new StdEntityRoute<Void, com.wizy.json.response.oneObject.HttpStatusResponse>("default#MdmRestApi#lockDeviceAndUpdateStatus",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.HttpStatusResponse>build(com.wizy.json.response.oneObject.HttpStatusResponse.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/keyaccess/mdm/device/unlock/{id}/task/{task}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.HttpStatusResponse> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.lockDeviceAndUpdateStatus(
                        /* [PATH] id */ match.getPathParam("id"),
                        /* [PATH] task */ match.getPathParam("task")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);

                OperationParameterDescription task = new OperationParameterDescription();
                task.name = "task";
                task.paramType = OperationParameterDescription.ParamType.path;
                task.dataType = "string";
                task.schemaKey = "";
                task.required = true;
                operation.parameters.add(task);


                operation.responseClass = "HttpStatusResponse";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.HttpStatusResponse";
                operation.sourceLocation = "com.wizy.restx.mdm.MdmRestApi#lockDeviceAndUpdateStatus(java.lang.String,java.lang.String)";
            }
        },
        new StdEntityRoute<com.wizy.model.mdm.Message, com.wizy.json.response.oneObject.TaskWithHttpStatus>("default#MdmRestApi#addTaskToOneDevice",
                readerRegistry.<com.wizy.model.mdm.Message>build(com.wizy.model.mdm.Message.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.TaskWithHttpStatus>build(com.wizy.json.response.oneObject.TaskWithHttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/keyaccess/mdm/task/add/id/{id}/{action}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.TaskWithHttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.Message body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.addTaskToOneDevice(
                        /* [PATH] id */ match.getPathParam("id"),
                        /* [PATH] action */ match.getPathParam("action"),
                        /* [BODY] message */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);

                OperationParameterDescription action = new OperationParameterDescription();
                action.name = "action";
                action.paramType = OperationParameterDescription.ParamType.path;
                action.dataType = "string";
                action.schemaKey = "";
                action.required = true;
                operation.parameters.add(action);

                OperationParameterDescription message = new OperationParameterDescription();
                message.name = "message";
                message.paramType = OperationParameterDescription.ParamType.body;
                message.dataType = "Message";
                message.schemaKey = "com.wizy.model.mdm.Message";
                message.required = true;
                operation.parameters.add(message);


                operation.responseClass = "TaskWithHttpStatus";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.Message";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.TaskWithHttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.MdmRestApi#addTaskToOneDevice(java.lang.String,java.lang.String,com.wizy.model.mdm.Message)";
            }
        },
        new StdEntityRoute<Void, com.wizy.json.response.twoObjects.TaskDeviceHttpstatusResp>("default#MdmRestApi#GetAllTaskByDeviceId",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.twoObjects.TaskDeviceHttpstatusResp>build(com.wizy.json.response.twoObjects.TaskDeviceHttpstatusResp.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/keyaccess/mdm/task/{deviceid}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.twoObjects.TaskDeviceHttpstatusResp> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.GetAllTaskByDeviceId(
                        /* [PATH] deviceid */ match.getPathParam("deviceid")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription deviceid = new OperationParameterDescription();
                deviceid.name = "deviceid";
                deviceid.paramType = OperationParameterDescription.ParamType.path;
                deviceid.dataType = "string";
                deviceid.schemaKey = "";
                deviceid.required = true;
                operation.parameters.add(deviceid);


                operation.responseClass = "TaskDeviceHttpstatusResp";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.twoObjects.TaskDeviceHttpstatusResp";
                operation.sourceLocation = "com.wizy.restx.mdm.MdmRestApi#GetAllTaskByDeviceId(java.lang.String)";
            }
        },
        new StdEntityRoute<Void, com.wizy.json.response.oneObject.HttpStatusResponse>("default#MdmRestApi#updateTaskStatusById",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.HttpStatusResponse>build(com.wizy.json.response.oneObject.HttpStatusResponse.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/keyaccess/mdm/task/{id}/device/{deviceid}/status/{status}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.HttpStatusResponse> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.updateTaskStatusById(
                        /* [PATH] id */ match.getPathParam("id"),
                        /* [PATH] deviceid */ match.getPathParam("deviceid"),
                        /* [PATH] status */ match.getPathParam("status")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);

                OperationParameterDescription deviceid = new OperationParameterDescription();
                deviceid.name = "deviceid";
                deviceid.paramType = OperationParameterDescription.ParamType.path;
                deviceid.dataType = "string";
                deviceid.schemaKey = "";
                deviceid.required = true;
                operation.parameters.add(deviceid);

                OperationParameterDescription status = new OperationParameterDescription();
                status.name = "status";
                status.paramType = OperationParameterDescription.ParamType.path;
                status.dataType = "string";
                status.schemaKey = "";
                status.required = true;
                operation.parameters.add(status);


                operation.responseClass = "HttpStatusResponse";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.HttpStatusResponse";
                operation.sourceLocation = "com.wizy.restx.mdm.MdmRestApi#updateTaskStatusById(java.lang.String,java.lang.String,java.lang.String)";
            }
        },
        new StdEntityRoute<Void, com.wizy.json.response.oneObject.HttpStatusResponse>("default#MdmRestApi#registerDevice",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.HttpStatusResponse>build(com.wizy.json.response.oneObject.HttpStatusResponse.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/keyaccess/mdm/agency/register/{agencyid}/{agencycode}/device/{deviceid}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.HttpStatusResponse> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.registerDevice(
                        /* [PATH] agencyid */ match.getPathParam("agencyid"),
                        /* [PATH] agencycode */ match.getPathParam("agencycode"),
                        /* [PATH] deviceid */ match.getPathParam("deviceid")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription agencyid = new OperationParameterDescription();
                agencyid.name = "agencyid";
                agencyid.paramType = OperationParameterDescription.ParamType.path;
                agencyid.dataType = "string";
                agencyid.schemaKey = "";
                agencyid.required = true;
                operation.parameters.add(agencyid);

                OperationParameterDescription agencycode = new OperationParameterDescription();
                agencycode.name = "agencycode";
                agencycode.paramType = OperationParameterDescription.ParamType.path;
                agencycode.dataType = "string";
                agencycode.schemaKey = "";
                agencycode.required = true;
                operation.parameters.add(agencycode);

                OperationParameterDescription deviceid = new OperationParameterDescription();
                deviceid.name = "deviceid";
                deviceid.paramType = OperationParameterDescription.ParamType.path;
                deviceid.dataType = "string";
                deviceid.schemaKey = "";
                deviceid.required = true;
                operation.parameters.add(deviceid);


                operation.responseClass = "HttpStatusResponse";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.HttpStatusResponse";
                operation.sourceLocation = "com.wizy.restx.mdm.MdmRestApi#registerDevice(java.lang.String,java.lang.String,java.lang.String)";
            }
        },
        new StdEntityRoute<Void, com.wizy.json.response.oneObject.WifiListWithStatus>("default#MdmRestApi#getWifiByIgencyId",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.WifiListWithStatus>build(com.wizy.json.response.oneObject.WifiListWithStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/keyaccess/mdm/wifi/agency/{agencyid}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.WifiListWithStatus> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.getWifiByIgencyId(
                        /* [PATH] agencyid */ match.getPathParam("agencyid")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription agencyid = new OperationParameterDescription();
                agencyid.name = "agencyid";
                agencyid.paramType = OperationParameterDescription.ParamType.path;
                agencyid.dataType = "string";
                agencyid.schemaKey = "";
                agencyid.required = true;
                operation.parameters.add(agencyid);


                operation.responseClass = "WifiListWithStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.WifiListWithStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.MdmRestApi#getWifiByIgencyId(java.lang.String)";
            }
        },
        });
    }

}
