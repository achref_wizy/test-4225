package com.wizy.restx.mdm;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;

import restx.common.Types;
import restx.*;
import restx.entity.*;
import restx.http.*;
import restx.factory.*;
import restx.security.*;
import static restx.security.Permissions.*;
import restx.description.*;
import restx.converters.MainStringConverter;
import static restx.common.MorePreconditions.checkPresent;

import javax.validation.Validator;
import static restx.validation.Validations.checkValid;

import java.io.IOException;
import java.io.PrintWriter;

@Component(priority = 0)

public class AgencyResourcesRouter extends RestxRouter {

    public AgencyResourcesRouter(
                    final AgencyResources resource,
                    final EntityRequestBodyReaderRegistry readerRegistry,
                    final EntityResponseWriterRegistry writerRegistry,
                    final MainStringConverter converter,
                    final Validator validator,
                    final RestxSecurityManager securityManager) {
        super(
            "default", "AgencyResourcesRouter", new RestxRoute[] {
        new StdEntityRoute<com.wizy.model.mdm.agency.Agency, restx.http.HttpStatus>("default#AgencyResources#addAgency",
                readerRegistry.<com.wizy.model.mdm.agency.Agency>build(com.wizy.model.mdm.agency.Agency.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/agency/add"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.agency.Agency body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.addAgency(
                        /* [BODY] agency */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription agency = new OperationParameterDescription();
                agency.name = "agency";
                agency.paramType = OperationParameterDescription.ParamType.body;
                agency.dataType = "Agency";
                agency.schemaKey = "com.wizy.model.mdm.agency.Agency";
                agency.required = true;
                operation.parameters.add(agency);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.agency.Agency";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.AgencyResources#addAgency(com.wizy.model.mdm.agency.Agency)";
            }
        },
        new StdEntityRoute<com.wizy.model.mdm.agency.Agency, restx.http.HttpStatus>("default#AgencyResources#updateAgency",
                readerRegistry.<com.wizy.model.mdm.agency.Agency>build(com.wizy.model.mdm.agency.Agency.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("POST", "/secured/agency/update/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, com.wizy.model.mdm.agency.Agency body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.updateAgency(
                        /* [PATH] id */ match.getPathParam("id"),
                        /* [BODY] agency */ checkValid(validator, body)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);

                OperationParameterDescription agency = new OperationParameterDescription();
                agency.name = "agency";
                agency.paramType = OperationParameterDescription.ParamType.body;
                agency.dataType = "Agency";
                agency.schemaKey = "com.wizy.model.mdm.agency.Agency";
                agency.required = true;
                operation.parameters.add(agency);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "com.wizy.model.mdm.agency.Agency";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.AgencyResources#updateAgency(java.lang.String,com.wizy.model.mdm.agency.Agency)";
            }
        },
        new StdEntityRoute<Void, com.wizy.json.response.oneObject.AgencyWithStatus>("default#AgencyResources#getAgencyById",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<com.wizy.json.response.oneObject.AgencyWithStatus>build(com.wizy.json.response.oneObject.AgencyWithStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/agency/id/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<com.wizy.json.response.oneObject.AgencyWithStatus> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.getAgencyById(
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "AgencyWithStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "com.wizy.json.response.oneObject.AgencyWithStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.AgencyResources#getAgencyById(java.lang.String)";
            }
        },
        new StdEntityRoute<Void, java.lang.Boolean>("default#AgencyResources#CheckAgencyExistByCode",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<java.lang.Boolean>build(java.lang.Boolean.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/agency/code/{code}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<java.lang.Boolean> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.CheckAgencyExistByCode(
                        /* [PATH] code */ match.getPathParam("code")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription code = new OperationParameterDescription();
                code.name = "code";
                code.paramType = OperationParameterDescription.ParamType.path;
                code.dataType = "string";
                code.schemaKey = "";
                code.required = true;
                operation.parameters.add(code);


                operation.responseClass = "boolean";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.mdm.AgencyResources#CheckAgencyExistByCode(java.lang.String)";
            }
        },
        new StdEntityRoute<Void, java.util.List<com.wizy.model.mdm.agency.Agency>>("default#AgencyResources#listAgency",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<java.util.List<com.wizy.model.mdm.agency.Agency>>build(Types.newParameterizedType(java.util.List.class, com.wizy.model.mdm.agency.Agency.class), Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/secured/agency/list"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<java.util.List<com.wizy.model.mdm.agency.Agency>> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.listAgency(
                        
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                

                operation.responseClass = "Agency>";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.mdm.AgencyResources#listAgency()";
            }
        },
        new StdEntityRoute<Void, restx.http.HttpStatus>("default#AgencyResources#deleteAgency",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("DELETE", "/secured/agency/{id}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.deleteAgency(
                        /* [PATH] id */ match.getPathParam("id")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription id = new OperationParameterDescription();
                id.name = "id";
                id.paramType = OperationParameterDescription.ParamType.path;
                id.dataType = "string";
                id.schemaKey = "";
                id.required = true;
                operation.parameters.add(id);


                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.mdm.AgencyResources#deleteAgency(java.lang.String)";
            }
        },
        });
    }

}
