package com.wizy.restx.mdm;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.mdm.TaskResources;

@Machine
public class TaskResourcesFactoryMachine extends SingleNameFactoryMachine<TaskResources> {
    public static final Name<TaskResources> NAME = Name.of(TaskResources.class, "TaskResources");

    public TaskResourcesFactoryMachine() {
        super(0, new StdMachineEngine<TaskResources>(NAME, 0, BoundlessComponentBox.FACTORY) {


            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(

                ));
            }

            @Override
            protected TaskResources doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new TaskResources(

                );
            }
        });
    }

}
