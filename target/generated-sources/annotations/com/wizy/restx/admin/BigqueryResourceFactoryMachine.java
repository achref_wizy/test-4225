package com.wizy.restx.admin;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.admin.BigqueryResource;

@Machine
public class BigqueryResourceFactoryMachine extends SingleNameFactoryMachine<BigqueryResource> {
    public static final Name<BigqueryResource> NAME = Name.of(BigqueryResource.class, "BigqueryResource");

    public BigqueryResourceFactoryMachine() {
        super(0, new StdMachineEngine<BigqueryResource>(NAME, 0, BoundlessComponentBox.FACTORY) {


            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(

                ));
            }

            @Override
            protected BigqueryResource doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new BigqueryResource(

                );
            }
        });
    }

}
