package com.wizy.restx.admin;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.admin.PubSubResource;

@Machine
public class PubSubResourceFactoryMachine extends SingleNameFactoryMachine<PubSubResource> {
    public static final Name<PubSubResource> NAME = Name.of(PubSubResource.class, "PubSubResource");

    public PubSubResourceFactoryMachine() {
        super(0, new StdMachineEngine<PubSubResource>(NAME, 0, BoundlessComponentBox.FACTORY) {


            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(

                ));
            }

            @Override
            protected PubSubResource doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new PubSubResource(

                );
            }
        });
    }

}
