package com.wizy.restx.admin;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;

import restx.common.Types;
import restx.*;
import restx.entity.*;
import restx.http.*;
import restx.factory.*;
import restx.security.*;
import static restx.security.Permissions.*;
import restx.description.*;
import restx.converters.MainStringConverter;
import static restx.common.MorePreconditions.checkPresent;

import javax.validation.Validator;
import static restx.validation.Validations.checkValid;

import java.io.IOException;
import java.io.PrintWriter;

@Component(priority = 0)

public class CloudStorageResourceRouter extends RestxRouter {

    public CloudStorageResourceRouter(
                    final CloudStorageResource resource,
                    final EntityRequestBodyReaderRegistry readerRegistry,
                    final EntityResponseWriterRegistry writerRegistry,
                    final MainStringConverter converter,
                    final Validator validator,
                    final RestxSecurityManager securityManager) {
        super(
            "default", "CloudStorageResourceRouter", new RestxRoute[] {
        new StdEntityRoute<Void, restx.http.HttpStatus>("default#CloudStorageResource#StreamToGCS",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<restx.http.HttpStatus>build(restx.http.HttpStatus.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/gcs/pull"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<restx.http.HttpStatus> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.StreamToGCS(
                        
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                

                operation.responseClass = "HttpStatus";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "restx.http.HttpStatus";
                operation.sourceLocation = "com.wizy.restx.admin.CloudStorageResource#StreamToGCS()";
            }
        },
        new StdEntityRoute<Void, java.util.List<com.google.api.services.storage.model.StorageObject>>("default#CloudStorageResource#listBucket",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<java.util.List<com.google.api.services.storage.model.StorageObject>>build(Types.newParameterizedType(java.util.List.class, com.google.api.services.storage.model.StorageObject.class), Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/gcs/bucket/{bucketName}/files"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<java.util.List<com.google.api.services.storage.model.StorageObject>> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.listBucket(
                        /* [PATH] bucketName */ match.getPathParam("bucketName")
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription bucketName = new OperationParameterDescription();
                bucketName.name = "bucketName";
                bucketName.paramType = OperationParameterDescription.ParamType.path;
                bucketName.dataType = "string";
                bucketName.schemaKey = "";
                bucketName.required = true;
                operation.parameters.add(bucketName);


                operation.responseClass = "StorageObject>";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.admin.CloudStorageResource#listBucket(java.lang.String)";
            }
        },
        });
    }

}
