package com.wizy.restx.admin;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.admin.FileResource;

@Machine
public class FileResourceFactoryMachine extends SingleNameFactoryMachine<FileResource> {
    public static final Name<FileResource> NAME = Name.of(FileResource.class, "FileResource");

    public FileResourceFactoryMachine() {
        super(0, new StdMachineEngine<FileResource>(NAME, 0, BoundlessComponentBox.FACTORY) {


            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(

                ));
            }

            @Override
            protected FileResource doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new FileResource(

                );
            }
        });
    }

}
