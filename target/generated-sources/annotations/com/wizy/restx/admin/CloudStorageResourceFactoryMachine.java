package com.wizy.restx.admin;

import com.google.common.collect.ImmutableSet;
import restx.factory.*;
import com.wizy.restx.admin.CloudStorageResource;

@Machine
public class CloudStorageResourceFactoryMachine extends SingleNameFactoryMachine<CloudStorageResource> {
    public static final Name<CloudStorageResource> NAME = Name.of(CloudStorageResource.class, "CloudStorageResource");

    public CloudStorageResourceFactoryMachine() {
        super(0, new StdMachineEngine<CloudStorageResource>(NAME, 0, BoundlessComponentBox.FACTORY) {


            @Override
            public BillOfMaterials getBillOfMaterial() {
                return new BillOfMaterials(ImmutableSet.<Factory.Query<?>>of(

                ));
            }

            @Override
            protected CloudStorageResource doNewComponent(SatisfiedBOM satisfiedBOM) {
                return new CloudStorageResource(

                );
            }
        });
    }

}
