package com.wizy.restx.admin;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;

import restx.common.Types;
import restx.*;
import restx.entity.*;
import restx.http.*;
import restx.factory.*;
import restx.security.*;
import static restx.security.Permissions.*;
import restx.description.*;
import restx.converters.MainStringConverter;
import static restx.common.MorePreconditions.checkPresent;

import javax.validation.Validator;
import static restx.validation.Validations.checkValid;

import java.io.IOException;
import java.io.PrintWriter;

@Component(priority = 0)

public class PubSubResourceRouter extends RestxRouter {

    public PubSubResourceRouter(
                    final PubSubResource resource,
                    final EntityRequestBodyReaderRegistry readerRegistry,
                    final EntityResponseWriterRegistry writerRegistry,
                    final MainStringConverter converter,
                    final Validator validator,
                    final RestxSecurityManager securityManager) {
        super(
            "default", "PubSubResourceRouter", new RestxRoute[] {
        new StdEntityRoute<Void, Empty>("default#PubSubResource#createTopic",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<Empty>build(void.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/pubsub/topic"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<Empty> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                resource.createTopic(
                        
                );
                return Optional.of(Empty.EMPTY);
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                

                operation.responseClass = "void";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.admin.PubSubResource#createTopic()";
            }
        },
        new StdEntityRoute<Void, Empty>("default#PubSubResource#createSubscription",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<Empty>build(void.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/pubsub/subscription"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<Empty> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                resource.createSubscription(
                        
                );
                return Optional.of(Empty.EMPTY);
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                

                operation.responseClass = "void";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.admin.PubSubResource#createSubscription()";
            }
        },
        new StdEntityRoute<Void, Empty>("default#PubSubResource#publishMessage",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<Empty>build(void.class, Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/pubsub/publish"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<Empty> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                resource.publishMessage(
                        
                );
                return Optional.of(Empty.EMPTY);
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                

                operation.responseClass = "void";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.admin.PubSubResource#publishMessage()";
            }
        },
        new StdEntityRoute<Void, java.util.List<java.lang.String>>("default#PubSubResource#receiveMessages",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<java.util.List<java.lang.String>>build(Types.newParameterizedType(java.util.List.class, java.lang.String.class), Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/pubsub/receive/{maxmessage}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<java.util.List<java.lang.String>> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.receiveMessages(
                        /* [PATH] maxmessage */ converter.convert(match.getPathParam("maxmessage"), int.class)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription maxmessage = new OperationParameterDescription();
                maxmessage.name = "maxmessage";
                maxmessage.paramType = OperationParameterDescription.ParamType.path;
                maxmessage.dataType = "int";
                maxmessage.schemaKey = "int";
                maxmessage.required = true;
                operation.parameters.add(maxmessage);


                operation.responseClass = "String>";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.admin.PubSubResource#receiveMessages(int)";
            }
        },
        new StdEntityRoute<Void, java.util.List<java.lang.String>>("default#PubSubResource#receiveMessagesWithoutACK",
                readerRegistry.<Void>build(Void.class, Optional.<String>absent()),
                writerRegistry.<java.util.List<java.lang.String>>build(Types.newParameterizedType(java.util.List.class, java.lang.String.class), Optional.<String>absent()),
                new StdRestxRequestMatcher("GET", "/pubsub/receive/noack/{maxmessage}"),
                HttpStatus.OK, RestxLogLevel.DEFAULT) {
            @Override
            protected Optional<java.util.List<java.lang.String>> doRoute(RestxRequest request, RestxRequestMatch match, Void body) throws IOException {
                securityManager.check(request, open());
                return Optional.of(resource.receiveMessagesWithoutACK(
                        /* [PATH] maxmessage */ converter.convert(match.getPathParam("maxmessage"), int.class)
                ));
            }

            @Override
            protected void describeOperation(OperationDescription operation) {
                super.describeOperation(operation);
                                OperationParameterDescription maxmessage = new OperationParameterDescription();
                maxmessage.name = "maxmessage";
                maxmessage.paramType = OperationParameterDescription.ParamType.path;
                maxmessage.dataType = "int";
                maxmessage.schemaKey = "int";
                maxmessage.required = true;
                operation.parameters.add(maxmessage);


                operation.responseClass = "String>";
                operation.inEntitySchemaKey = "";
                operation.outEntitySchemaKey = "";
                operation.sourceLocation = "com.wizy.restx.admin.PubSubResource#receiveMessagesWithoutACK(int)";
            }
        },
        });
    }

}
