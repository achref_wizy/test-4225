package com.wizy.model.mdm.agency;

import java.util.Date;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
@Index
public class Agency {

	@Id
	private String id;

	private String name;
	private String code;
	private String managerName;
	private Date date;
	private String address;
	private String managerEmail;
	private String trigramme;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getManagerEmail() {
		return managerEmail;
	}

	public void setManagerEmail(String managerEmail) {
		this.managerEmail = managerEmail;
	}

	public String getTrigramme() {
		return trigramme;
	}

	public void setTrigramme(String trigramme) {
		this.trigramme = trigramme;
	}
	
	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public Agency() {
		super();
	}

	public Agency(String id, String name, String code, String managerName, Date date, String address,
			String managerEmail, String trigramme) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.managerName = managerName;
		this.date = date;
		this.address = address;
		this.managerEmail = managerEmail;
		this.trigramme = trigramme;
	}

}
