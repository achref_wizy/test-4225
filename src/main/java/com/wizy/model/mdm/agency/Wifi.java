package com.wizy.model.mdm.agency;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Wifi {

	@Id
	private String id;
	@Index
	private String agencyId;
	@Index
	private String agencyName;
	private String priorityOrder;
	@Index
	private String name;
	private String networkSsid;
	private String wifiMacAddress;
	private String maskAddress;

	private String security;
	private String password;
	private String epaMethod;
	private String phase2Auth;
	private String identity;
	private String proxySetting;
	private String proxyHostName;
	private String proxyPort;
	private String bypassProxyFor;
	private String ipSetting;
	private String ipAddress;
	private String gateway;
	private String networkPrefixLength;
	private String dns1;
	private String dns2;
	private String caCertificate;
	private String anonymousIdendity;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNetworkSsid() {
		return networkSsid;
	}

	public void setNetworkSsid(String networkSsid) {
		this.networkSsid = networkSsid;
	}

	public String getSecurity() {
		return security;
	}

	public void setSecurity(String security) {
		this.security = security;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEpaMethod() {
		return epaMethod;
	}

	public void setEpaMethod(String epaMethod) {
		this.epaMethod = epaMethod;
	}

	public String getPhase2Auth() {
		return phase2Auth;
	}

	public void setPhase2Auth(String phase2Auth) {
		this.phase2Auth = phase2Auth;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getProxySetting() {
		return proxySetting;
	}

	public void setProxySetting(String proxySetting) {
		this.proxySetting = proxySetting;
	}

	public String getProxyHostName() {
		return proxyHostName;
	}

	public void setProxyHostName(String proxyHostName) {
		this.proxyHostName = proxyHostName;
	}

	public String getProxyPort() {
		return proxyPort;
	}

	public void setProxyPort(String proxyPort) {
		this.proxyPort = proxyPort;
	}

	public String getBypassProxyFor() {
		return bypassProxyFor;
	}

	public void setBypassProxyFor(String bypassProxyFor) {
		this.bypassProxyFor = bypassProxyFor;
	}

	public String getIpSetting() {
		return ipSetting;
	}

	public void setIpSetting(String ipSetting) {
		this.ipSetting = ipSetting;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getNetworkPrefixLength() {
		return networkPrefixLength;
	}

	public void setNetworkPrefixLength(String networkPrefixLength) {
		this.networkPrefixLength = networkPrefixLength;
	}

	public String getDns1() {
		return dns1;
	}

	public void setDns1(String dns1) {
		this.dns1 = dns1;
	}

	public String getDns2() {
		return dns2;
	}

	public void setDns2(String dns2) {
		this.dns2 = dns2;
	}

	public String getCaCertificate() {
		return caCertificate;
	}

	public void setCaCertificate(String caCertificate) {
		this.caCertificate = caCertificate;
	}

	public String getAnonymousIdendity() {
		return anonymousIdendity;
	}

	public void setAnonymousIdendity(String anonymousIdendity) {
		this.anonymousIdendity = anonymousIdendity;
	}

	public String getWifiMacAddress() {
		return wifiMacAddress;
	}

	public void setWifiMacAddress(String wifiMacAddress) {
		this.wifiMacAddress = wifiMacAddress;
	}

	public String getMaskAddress() {
		return maskAddress;
	}

	public void setMaskAddress(String maskAddress) {
		this.maskAddress = maskAddress;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public String getPriorityOrder() {
		return priorityOrder;
	}

	public void setPriorityOrder(String priorityOrder) {
		this.priorityOrder = priorityOrder;
	}
	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public Wifi() {
		super();
	}

	public Wifi(String id, String agencyId, String agencyName, String priorityOrder, String name, String networkSsid,
			String wifiMacAddress, String maskAddress, String security, String password, String epaMethod,
			String phase2Auth, String identity, String proxySetting, String proxyHostName, String proxyPort,
			String bypassProxyFor, String ipSetting, String ipAddress, String gateway, String networkPrefixLength,
			String dns1, String dns2, String caCertificate, String anonymousIdendity) {
		super();
		this.id = id;
		this.agencyId = agencyId;
		this.agencyName = agencyName;
		this.priorityOrder = priorityOrder;
		this.name = name;
		this.networkSsid = networkSsid;
		this.wifiMacAddress = wifiMacAddress;
		this.maskAddress = maskAddress;
		this.security = security;
		this.password = password;
		this.epaMethod = epaMethod;
		this.phase2Auth = phase2Auth;
		this.identity = identity;
		this.proxySetting = proxySetting;
		this.proxyHostName = proxyHostName;
		this.proxyPort = proxyPort;
		this.bypassProxyFor = bypassProxyFor;
		this.ipSetting = ipSetting;
		this.ipAddress = ipAddress;
		this.gateway = gateway;
		this.networkPrefixLength = networkPrefixLength;
		this.dns1 = dns1;
		this.dns2 = dns2;
		this.caCertificate = caCertificate;
		this.anonymousIdendity = anonymousIdendity;
	}

}
