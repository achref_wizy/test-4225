package com.wizy.model.mdm;

import java.util.Date;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;
import com.wizy.model.mdm.device.Device;

@Entity
@Index
public class Task {

	@Id
	private String id;
	// private String deviceName;
	// private String deviceImei;
	// private String deviceToken;
	private String action;
	private String status;
	private Date creationDate;
	private Date takenDate;
	private Message message;

	@Parent
	private Ref<Device> deviceRef;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String isStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}
	

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getTakenDate() {
		return takenDate;
	}

	public void setTakenDate(Date takenDate) {
		this.takenDate = takenDate;
	}

	public Ref<Device> getDeviceRef() {
		return deviceRef;
	}

	public void setDeviceRef(Ref<Device> deviceRef) {
		this.deviceRef = deviceRef;
	}
	

	public void setDeviceRef(Device deviceRef) {
		this.deviceRef = Ref.create(deviceRef);
	}

	public String getStatus() {
		return status;
	}

	public Task() {
		super();
	}

	public Task(String id, String action, String status, Date creationDate, Date takenDate, Message message,
			Ref<Device> deviceRef) {
		super();
		this.id = id;
		this.action = action;
		this.status = status;
		this.creationDate = creationDate;
		this.takenDate = takenDate;
		this.message = message;
		this.deviceRef = deviceRef;
	}

	public Task(String id, String action, String status,Date creationDate, Date takenDate, Date date, Message message, String deviceId) {
		this(id, action, status,creationDate, takenDate, message, Ref.create(Key.create(Device.class, deviceId)));
	}

}
