package com.wizy.model.mdm.device;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
@Index
public class Device {

	@Id
	private String id;
	private String imei;
	private String agencyId;
	private String agencyName;
	private String name;
	private String modelNumber;
	private String batteryLevel;
	private String batteryStatus;
	private String batteryPlugType;
	private String batteryCyclesNbr;
	private String latitude;
	private String longitude;
	private String serialNumber;
	private String fcmToken;
	private String status;
	private Date date;
	private boolean lock;

	private String iccId;
	private String phoneNumber;
	private String memoryUsage;
	private String securityInformation;

	private String appVersion;
	private String androidVersion;
	
	private Date currentDate;
	private String lastRebootDate;
	private String lastActionDate;
	
	private NetworkInfo networkInfo; 

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getBatteryLevel() {
		return batteryLevel;
	}

	public void setBatteryLevel(String batteryLevel) {
		this.batteryLevel = batteryLevel;
	}

	public String getBatteryStatus() {
		return batteryStatus;
	}

	public void setBatteryStatus(String batteryStatus) {
		this.batteryStatus = batteryStatus;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public String getFcmToken() {
		return fcmToken;
	}

	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getSecurityInformation() {
		return securityInformation;
	}

	public void setSecurityInformation(String securityInformation) {
		this.securityInformation = securityInformation;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(String androidVersion) {
		this.androidVersion = androidVersion;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIccId() {
		return iccId;
	}

	public void setIccId(String iccId) {
		this.iccId = iccId;
	}

	public String getMemoryUsage() {
		return memoryUsage;
	}

	public void setMemoryUsage(String memoryUsage) {
		this.memoryUsage = memoryUsage;
	}

	public String getBatteryPlugType() {
		return batteryPlugType;
	}

	public void setBatteryPlugType(String batteryPlugType) {
		this.batteryPlugType = batteryPlugType;
	}

	public String getBatteryCyclesNbr() {
		return batteryCyclesNbr;
	}

	public void setBatteryCyclesNbr(String batteryCyclesNbr) {
		this.batteryCyclesNbr = batteryCyclesNbr;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public String getLastRebootDate() {
		return lastRebootDate;
	}

	public void setLastRebootDate(String lastRebootDate) {
		this.lastRebootDate = lastRebootDate;
	}

	public String getLastActionDate() {
		return lastActionDate;
	}

	public void setLastActionDate(String lastActionDate) {
		this.lastActionDate = lastActionDate;
	}
	
	public NetworkInfo getNetworkInfo() {
		return networkInfo;
	}

	public void setNetworkInfo(NetworkInfo networkInfo) {
		this.networkInfo = networkInfo;
	}
	public boolean isLock() {
		return lock;
	}

	public void setLock(boolean lock) {
		this.lock = lock;
	}
	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public Device() {
		super();
	}

	public Device(String id, String imei, String agencyId, String agencyName, String name, String modelNumber,
			String batteryLevel, String batteryStatus, String batteryPlugType, String batteryCyclesNbr, String latitude,
			String longitude, String serialNumber, String fcmToken, String status, Date date, boolean lock,
			String iccId, String phoneNumber, String memoryUsage, String securityInformation, String appVersion,
			String androidVersion, Date currentDate, String lastRebootDate, String lastActionDate,
			NetworkInfo networkInfo) {
		super();
		this.id = id;
		this.imei = imei;
		this.agencyId = agencyId;
		this.agencyName = agencyName;
		this.name = name;
		this.modelNumber = modelNumber;
		this.batteryLevel = batteryLevel;
		this.batteryStatus = batteryStatus;
		this.batteryPlugType = batteryPlugType;
		this.batteryCyclesNbr = batteryCyclesNbr;
		this.latitude = latitude;
		this.longitude = longitude;
		this.serialNumber = serialNumber;
		this.fcmToken = fcmToken;
		this.status = status;
		this.date = date;
		this.lock = lock;
		this.iccId = iccId;
		this.phoneNumber = phoneNumber;
		this.memoryUsage = memoryUsage;
		this.securityInformation = securityInformation;
		this.appVersion = appVersion;
		this.androidVersion = androidVersion;
		this.currentDate = currentDate;
		this.lastRebootDate = lastRebootDate;
		this.lastActionDate = lastActionDate;
		this.networkInfo = networkInfo;
	}
}
