package com.wizy.model.mdm.device;

public class NetworkInfo {

	private String connectionType;
	private String networkSsid;
	private String wifiMacAddress;
	private String maskAddress;
	private String security;
	private String ipAddress;
	private String gateway;
	private String dns1;
	private String dns2;
	
	public String getNetworkSsid() {
		return networkSsid;
	}
	public void setNetworkSsid(String networkSsid) {
		this.networkSsid = networkSsid;
	}
	public String getWifiMacAddress() {
		return wifiMacAddress;
	}
	public void setWifiMacAddress(String wifiMacAddress) {
		this.wifiMacAddress = wifiMacAddress;
	}
	public String getMaskAddress() {
		return maskAddress;
	}
	public void setMaskAddress(String maskAddress) {
		this.maskAddress = maskAddress;
	}
	public String getSecurity() {
		return security;
	}
	public void setSecurity(String security) {
		this.security = security;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}
	public String getDns1() {
		return dns1;
	}
	public void setDns1(String dns1) {
		this.dns1 = dns1;
	}
	public String getDns2() {
		return dns2;
	}
	public void setDns2(String dns2) {
		this.dns2 = dns2;
	}
	public String getConnectionType() {
		return connectionType;
	}
	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}

}
