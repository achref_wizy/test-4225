package com.wizy.model.mdm;

import java.util.Date;


public class Message {

	private String title;
	private String content;
	private String type;
	private Date date;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Message() {
		super();
	}

	public Message(String title, String content, String type, Date date) {
		super();
		this.title = title;
		this.content = content;
		this.type = type;
		this.date = date;
	}
}
