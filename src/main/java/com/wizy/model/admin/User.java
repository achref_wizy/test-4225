package com.wizy.model.admin;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Unindex;

@Entity
@Unindex
public class User implements Serializable {

	private static final long serialVersionUID = -1715246340564760801L;

	// Data coming directly from the OAuth token
	@Id
	private String id;
	@Index
	private String email;

	private boolean emailVerified;
	private Date signInDate;

	// Data coming from G+
	private String displayName;
	private String familyName;
	private String givenName;
	private String imageUrl;

	private String profil;
	private Date lastAccess;
	@Index
	private String role;
	private String status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEmailVerified() {
		return emailVerified;
	}

	public void setEmailVerified(boolean emailVerified) {
		this.emailVerified = emailVerified;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Date getSignInDate() {
		return signInDate;
	}

	public void setSignInDate(Date signInDate) {
		this.signInDate = signInDate;
	}

	public String getProfil() {
		return profil;
	}

	public void setProfil(String profil) {
		this.profil = profil;
	}

	public Date getLastAccess() {
		return lastAccess;
	}

	public void setLastAccess(Date lastAccess) {
		this.lastAccess = lastAccess;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User() {

	}

	private User(String id, String email, boolean emailVerified,
			String displayName, String familyName, String givenName,
			String imageUrl, Date signInDate, String profil, Date lastAccess,
			String role, String status) {
		this.id = id;
		this.email = email;
		this.emailVerified = emailVerified;
		this.displayName = displayName;
		this.familyName = familyName;
		this.givenName = givenName;
		this.imageUrl = imageUrl;
		this.signInDate = signInDate;
		this.lastAccess = lastAccess;
		this.role = role;
		this.status = status;
	}

	public static class Builder {
		private String id;
		private String email;
		private boolean emailVerified;
		private String displayName;
		private String familyName;
		private String givenName;
		private String imageUrl;
		private Date signInDate;
		private String profil;
		private Date lastAccess;
		private String role;
		private String status;

		public Builder setId(String id) {
			this.id = id;
			return this;
		}

		public Builder setEmail(String email) {
			this.email = email;
			return this;
		}

		public Builder setEmailVerified(boolean emailVerified) {
			this.emailVerified = emailVerified;
			return this;
		}

		public Builder setDisplayName(String displayName) {
			this.displayName = displayName;
			return this;
		}

		public Builder setFamilyName(String familyName) {
			this.familyName = familyName;
			return this;
		}

		public Builder setGivenName(String givenName) {
			this.givenName = givenName;
			return this;
		}

		public Builder setImageUrl(String imageUrl) {
			this.imageUrl = imageUrl;
			return this;
		}

		public Builder setSignInDate(Date signInDate) {
			this.signInDate = signInDate;
			return this;
		}

		public Builder setProfil(String profil) {
			this.profil = profil;
			return this;
		}

		public Builder setLastAccess(Date lastAccess) {
			this.lastAccess = lastAccess;
			return this;
		}

		public Builder setRole(String role) {
			this.role = role;
			return this;
		}

		public Builder setStatus(String status) {
			this.status = status;
			return this;
		}

		public User createUser() {
			return new User(id, email, emailVerified, displayName, familyName,
					givenName, imageUrl, signInDate, profil, lastAccess, role,
					status);
		}
	}
}
