package com.wizy.model.admin;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
@Index
public class Notify {

	@Id
	private String id;
	private String name;
	private boolean status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Notify() {
		super();
	}

	public Notify(String id, boolean status) {
		super();
		this.id = id;
		this.status = status;
	}

	public Notify(String id, String name, boolean status) {
		super();
		this.id = id;
		this.name = name;
		this.status = status;
	}

}
