package com.wizy.model.gcs;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Unindex;

@Entity
@Unindex
public class GcsConfigFile implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;
	@Index
	private String name;
	private String contentType;
	@Index
	private String userId;
	@Index
	private boolean access;
	@Index
	private Date uploadDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public boolean isAccess() {
		return access;
	}
	public void setAccess(boolean access) {
		this.access = access;
	}
	public Date getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}
	public GcsConfigFile() {
		super();
	}
	public GcsConfigFile(String id, String name, String contentType, String userId, boolean access, Date uploadDate) {
		super();
		this.id = id;
		this.name = name;
		this.contentType = contentType;
		this.userId = userId;
		this.access = access;
		this.uploadDate = uploadDate;
	}
	
}
