package com.wizy.ofy;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import com.wizy.model.admin.Notify;
import com.wizy.model.admin.User;
import com.wizy.model.gcs.GcsConfigFile;
import com.wizy.model.mdm.agency.Agency;
import com.wizy.model.mdm.agency.Wifi;
import com.wizy.model.mdm.device.Device;
import com.wizy.model.mdm.Task;

public class OfyService {

	static {
		ObjectifyFactory factory = ObjectifyService.factory();
		factory.register(User.class);
		factory.register(GcsConfigFile.class);
		factory.register(Device.class);
		factory.register(Task.class);
		factory.register(Notify.class);
		factory.register(Agency.class);
		factory.register(Wifi.class);
	}

	public static Objectify ofy() {
		return ObjectifyService.ofy();
	}
}
