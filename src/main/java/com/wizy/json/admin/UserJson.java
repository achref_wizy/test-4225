package com.wizy.json.admin;

import java.util.Date;

import com.wizy.model.admin.User;

public class UserJson {
	private String id;
	private String email;
	private String userId;
	private String hostedDomain;
	private boolean emailVerified;
	private String domainPrefered;
	private Date signInDate;

	private String displayName;
	private String familyName;
	private String givenName;
	private String imageUrl;

	private String profil;
	private Date lastAccess;
	private String role;
	private String status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHostedDomain() {
		return hostedDomain;
	}

	public void setHostedDomain(String hostedDomain) {
		this.hostedDomain = hostedDomain;
	}

	public boolean isEmailVerified() {
		return emailVerified;
	}

	public void setEmailVerified(boolean emailVerified) {
		this.emailVerified = emailVerified;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return the domainPrefered
	 */
	public String getDomainPrefered() {
		return domainPrefered;
	}

	/**
	 * @param domainPrefered
	 *            the domainPrefered to set
	 */
	public void setDomainPrefered(String domainPrefered) {
		this.domainPrefered = domainPrefered;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getProfil() {
		return profil;
	}

	public void setProfil(String profil) {
		this.profil = profil;
	}

	public Date getLastAccess() {
		return lastAccess;
	}

	public void setLastAccess(Date lastAccess) {
		this.lastAccess = lastAccess;
	}

	public Date getSignInDate() {
		return signInDate;
	}

	public void setSignInDate(Date signInDate) {
		this.signInDate = signInDate;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public static UserJson fromDatastore(User userFromDatastore) {
		if (userFromDatastore == null) {
			return null;
		}
		UserJson result = new UserJson();
		result.id = userFromDatastore.getId();
		result.email = userFromDatastore.getEmail();
		result.userId = userFromDatastore.getId();
		result.emailVerified = userFromDatastore.isEmailVerified();
		result.displayName = userFromDatastore.getDisplayName();
		result.familyName = userFromDatastore.getDisplayName();
		result.givenName = userFromDatastore.getGivenName();
		result.imageUrl = userFromDatastore.getImageUrl();
		result.profil = userFromDatastore.getProfil();
		result.lastAccess = userFromDatastore.getLastAccess();
		result.signInDate = userFromDatastore.getSignInDate();
		result.role = userFromDatastore.getRole();
		result.status = userFromDatastore.getStatus();
		return result;
	}

	public User asDatastoreUser() {
		User result = new User.Builder().setId(id).setEmail(email).setEmailVerified(emailVerified)
				.setDisplayName(displayName).setFamilyName(familyName).setGivenName(givenName).setImageUrl(imageUrl)
				.setId(userId).setProfil(profil).setSignInDate(signInDate).setLastAccess(lastAccess).setRole(role)
				.setStatus(status).createUser();
		return result;
	}
}