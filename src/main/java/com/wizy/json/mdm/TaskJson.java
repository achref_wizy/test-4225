package com.wizy.json.mdm;

import java.util.Date;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.wizy.model.mdm.Message;
import com.wizy.model.mdm.Task;
import com.wizy.model.mdm.device.Device;

public class TaskJson {

	private String id;
	private String action;
	private String status;
	private Date creationDate;
	private Date takenDate;
	private Message message;
	private String device;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String isStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getTakenDate() {
		return takenDate;
	}

	public void setTakenDate(Date takenDate) {
		this.takenDate = takenDate;
	}

	public String getStatus() {
		return status;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public static TaskJson fromDatastore(Task taskFromDatastore) {

		if (taskFromDatastore == null) {
			return null;
		}

		TaskJson result = new TaskJson();

		result.id = taskFromDatastore.getId();
		result.device = taskFromDatastore.getDeviceRef().getKey().getName();
		result.action = taskFromDatastore.getAction();
		result.status = taskFromDatastore.isStatus();
		result.creationDate = taskFromDatastore.getCreationDate();
		result.takenDate = taskFromDatastore.getTakenDate();
		result.message = taskFromDatastore.getMessage();

		return result;
	}

	public static Task asDatastore(TaskJson taskFromJson) {

		if (taskFromJson == null) {
			return null;
		}

		Task result = new Task();

		result.setId(taskFromJson.getId());
		result.setDeviceRef(Ref.create(Key.create(Device.class, taskFromJson.getDevice())));
		result.setAction(taskFromJson.getAction());
		result.setStatus(taskFromJson.isStatus());
		result.setCreationDate(taskFromJson.getCreationDate());
		result.setTakenDate(taskFromJson.getTakenDate());
		result.setMessage(taskFromJson.getMessage());

		return result;
	}
}
