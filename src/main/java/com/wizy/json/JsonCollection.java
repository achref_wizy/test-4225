package com.wizy.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.appengine.api.datastore.QueryResultIterator;

@SuppressWarnings("serial")
public class JsonCollection<T> implements Serializable {
    private List<T> items = new ArrayList<>();
    private String nextPageToken;

    public List<T> getItems() {
	return items;
    }

    public void setItems(List<T> items) {
	this.items = items;
    }

    public String getNextPageToken() {
	return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
	this.nextPageToken = nextPageToken;
    }

    public JsonCollection() {
    }

    public JsonCollection(QueryResultIterator<T> iterator) {
	boolean hasMore = iterator.hasNext();
	while (iterator.hasNext()) {
	    items.add(iterator.next());
	}
	if (hasMore) {
	    nextPageToken = iterator.getCursor().toWebSafeString();
	}
    }

    public JsonCollection(List<T> items, String nextPageToken) {
	this.nextPageToken = nextPageToken;
	this.items = items;
    }
}
