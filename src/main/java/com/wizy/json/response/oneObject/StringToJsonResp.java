package com.wizy.json.response.oneObject;

import restx.http.HttpStatus;

public class StringToJsonResp {

	private HttpStatus status;
	private String item;

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
}
