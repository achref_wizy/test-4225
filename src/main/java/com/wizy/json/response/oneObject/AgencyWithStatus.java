package com.wizy.json.response.oneObject;

import com.wizy.model.mdm.agency.Agency;

import restx.http.HttpStatus;

public class AgencyWithStatus {

	private HttpStatus status;
	private Agency agency;
	
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public Agency getAgency() {
		return agency;
	}
	public void setAgency(Agency agency) {
		this.agency = agency;
	}
}
