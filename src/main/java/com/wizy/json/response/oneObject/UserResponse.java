package com.wizy.json.response.oneObject;

import com.wizy.json.admin.UserJson;

import restx.http.HttpStatus;

public class UserResponse {

    private UserJson userJson;
    private HttpStatus status;

    public UserJson getUser() {
	return userJson;
    }

    public void setUser(UserJson userJson) {
	this.userJson = userJson;
    }

    public HttpStatus getStatus() {
	return status;
    }

    public void setStatus(HttpStatus status) {
	this.status = status;
    }

}
