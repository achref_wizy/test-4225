package com.wizy.json.response.oneObject;

import com.wizy.json.mdm.TaskJson;
import restx.http.HttpStatus;

public class TaskWithHttpStatus {

	private HttpStatus status;
	private TaskJson taskJson;
	
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public TaskJson getTask() {
		return taskJson;
	}
	public void setTask(TaskJson taskJson) {
		this.taskJson = taskJson;
	}
}
