package com.wizy.json.response.oneObject;

import restx.http.HttpStatus;

import com.wizy.json.admin.UserJson;

public class LoginResponse {
	
	private HttpStatus status;
	private UserJson userJson;

	public UserJson getUser() {
		return userJson;
	}

	public void setUser(UserJson userJson) {
		this.userJson = userJson;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
}
