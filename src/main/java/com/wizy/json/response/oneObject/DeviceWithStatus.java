package com.wizy.json.response.oneObject;

import com.wizy.model.mdm.device.Device;

import restx.http.HttpStatus;

public class DeviceWithStatus {

	private Device device ;
	private HttpStatus status;
	
	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}

}
