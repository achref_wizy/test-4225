package com.wizy.json.response.oneObject;

import java.util.List;

import com.wizy.model.mdm.agency.Wifi;

import restx.http.HttpStatus;

public class WifiListWithStatus {

	private HttpStatus status;
	private List<Wifi> wifiList;
	
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public List<Wifi> getWifiList() {
		return wifiList;
	}
	public void setWifiList(List<Wifi> wifiList) {
		this.wifiList = wifiList;
	}

}
