package com.wizy.json.response.oneObject;

import java.util.List;

import com.wizy.json.mdm.TaskJson;

import restx.http.HttpStatus;

public class TaskListWithHttpStatus {

	private HttpStatus status;
	private List<TaskJson> taskList;
	
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public List<TaskJson> getTaskList() {
		return taskList;
	}
	public void setTaskList(List<TaskJson> taskList) {
		this.taskList = taskList;
	}
		
}
