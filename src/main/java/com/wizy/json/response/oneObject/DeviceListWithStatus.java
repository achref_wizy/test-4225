package com.wizy.json.response.oneObject;

import java.util.List;

import com.wizy.model.mdm.device.Device;

import restx.http.HttpStatus;

public class DeviceListWithStatus {

	private HttpStatus status;
	private List<Device> deviceList;
	
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public List<Device> getDeviceList() {
		return deviceList;
	}
	public void setDeviceList(List<Device> deviceList) {
		this.deviceList = deviceList;
	}
}
