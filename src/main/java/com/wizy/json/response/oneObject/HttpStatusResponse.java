package com.wizy.json.response.oneObject;

import restx.http.HttpStatus;

public class HttpStatusResponse {

	private HttpStatus status;

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
}
