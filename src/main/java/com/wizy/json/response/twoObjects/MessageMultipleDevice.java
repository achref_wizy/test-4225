package com.wizy.json.response.twoObjects;

import java.util.List;

import com.wizy.model.mdm.Message;

public class MessageMultipleDevice {

	private List<String> ids ;
	private Message message;
	
	public List<String> getIds() {
		return ids;
	}
	public void setIds(List<String> ids) {
		this.ids = ids;
	}
	public Message getMessage() {
		return message;
	}
	public void setMessage(Message message) {
		this.message = message;
	}
	
}
