package com.wizy.json.response.twoObjects;

import java.util.List;

import com.wizy.json.mdm.TaskJson;
import restx.http.HttpStatus;

public class MultipleTaskListResp {

	private HttpStatus status;
	private List<TaskJson> success;
	private List<TaskJson> error;

	public List<TaskJson> getSuccess() {
		return success;
	}

	public void setSuccess(List<TaskJson> success) {
		this.success = success;
	}

	public List<TaskJson> getError() {
		return error;
	}

	public void setError(List<TaskJson> error) {
		this.error = error;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

}