package com.wizy.json.response.twoObjects;

import java.util.List;

import com.wizy.json.response.oneObject.DeviceWithStatus;
import restx.http.HttpStatus;

public class MultipleDeviceListResp {

	private HttpStatus status;
	private List<DeviceWithStatus> success;
	private List<DeviceWithStatus> error;
	
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public List<DeviceWithStatus> getSuccess() {
		return success;
	}
	public void setSuccess(List<DeviceWithStatus> success) {
		this.success = success;
	}
	public List<DeviceWithStatus> getError() {
		return error;
	}
	public void setError(List<DeviceWithStatus> error) {
		this.error = error;
	}
	
	
	
}
