package com.wizy.json.response.twoObjects;

import java.util.List;

import com.wizy.json.mdm.TaskJson;
import com.wizy.model.mdm.device.Device;

import restx.http.HttpStatus;

public class TaskDeviceHttpstatusResp {

	private Device device;
	private List<TaskJson> tasks;
	private HttpStatus status;
	
	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}
	public List<TaskJson> getTasks() {
		return tasks;
	}
	public void setTasks(List<TaskJson> tasks) {
		this.tasks = tasks;
	}
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
	
}
