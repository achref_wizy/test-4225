package com.wizy.json.response.gcs;

import com.google.api.services.storage.model.StorageObject;
import com.wizy.model.gcs.GcsCallFile;
import restx.http.HttpStatus;

public class CallFileResponse {

	private HttpStatus status;
	private GcsCallFile callFileInfo;
	private StorageObject File;
	
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public GcsCallFile getFileInfo() {
		return callFileInfo;
	}
	public void setFileInfo(GcsCallFile callFileInfo) {
		this.callFileInfo = callFileInfo;
	}
	public StorageObject getFile() {
		return File;
	}
	public void setFile(StorageObject file) {
		File = file;
	}
	

}
