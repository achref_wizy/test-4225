package com.wizy.json.response.gcs;

import com.google.api.services.storage.model.StorageObject;
import com.wizy.model.gcs.GcsConfigFile;

import restx.http.HttpStatus;

public class ConfFileResponse {

	private HttpStatus status;
	private GcsConfigFile configFileInfo;
	private StorageObject File;
	
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public GcsConfigFile getFileInfo() {
		return configFileInfo;
	}
	public void setFileInfo(GcsConfigFile configFileInfo) {
		this.configFileInfo = configFileInfo;
	}
	public StorageObject getFile() {
		return File;
	}
	public void setFile(StorageObject file) {
		File = file;
	}
	

}
