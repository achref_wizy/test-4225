package com.wizy.json.response.multiObjects;

import java.util.List;

import com.wizy.model.mdm.agency.Agency;
import com.wizy.model.mdm.agency.Wifi;
import com.wizy.model.mdm.device.Device;

import restx.http.HttpStatus;

public class DeviceAgencyWifiResp {

	private HttpStatus status;
	private Device device;
	private Agency agency;
	private List<Wifi> wifi;
	
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}
	public Agency getAgency() {
		return agency;
	}
	public void setAgency(Agency agency) {
		this.agency = agency;
	}
	public List<Wifi> getWifi() {
		return wifi;
	}
	public void setWifi(List<Wifi> wifi) {
		this.wifi = wifi;
	}
}
