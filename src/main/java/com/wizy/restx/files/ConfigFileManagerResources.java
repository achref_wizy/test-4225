package com.wizy.restx.files;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import com.google.api.services.storage.Storage;
import com.google.api.services.storage.model.StorageObject;
import com.google.appengine.repackaged.com.google.common.base.Optional;
import com.googlecode.objectify.SaveException;
import com.wizy.json.response.gcs.ConfFileResponse;
import com.wizy.model.gcs.GcsConfigFile;
import com.wizy.ofy.OfyService;
import com.wizy.restx.Utils.StorageFactory;
import com.wizy.security.SecurityAccess;

import restx.annotations.DELETE;
import restx.annotations.GET;
import restx.annotations.POST;
import restx.annotations.RestxResource;
import restx.factory.Component;
import restx.http.HttpStatus;
import restx.security.PermitAll;

@Component
@RestxResource("/secured/file/conf")
@PermitAll
public class ConfigFileManagerResources {
	
	final private String CGS_CONF_FILE_BUCKET = SecurityAccess.CGS_CONF_FILE_BUCKET;

	/**
	 * Decode a string with UTF-8
	 * 
	 * @param item
	 * @return
	 */
	public String decodeString(String item) {
		String decode_item = null;
		try {
			decode_item = URLDecoder.decode(item, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return decode_item;
	}

	@GET("/download/{userid}/file/{fileid}")
	public ConfFileResponse downloadFile(String userid, String fileid) {

		String userID = decodeString(userid);
		String fileID = decodeString(fileid);

		ConfFileResponse resp = new ConfFileResponse();
		GcsConfigFile fileInfo = Optional.fromNullable(OfyService.ofy().load().type(GcsConfigFile.class).id(fileID).now()).orNull();

		if (fileInfo != null) {
			if (fileInfo.getUserId().equals(userID) || fileInfo.isAccess()) {

				try {
					Storage storage = StorageFactory.getService();
					Storage.Objects.Get getObject = storage.objects().get(CGS_CONF_FILE_BUCKET, fileID);
					if (!getObject.isEmpty()) {
						StorageObject object = getObject.execute();
						resp.setFileInfo(fileInfo);
						resp.setFile(object);
						resp.setStatus(HttpStatus.OK);
					} else {
						resp.setStatus(HttpStatus.NOT_FOUND);
					}
				} catch (IOException e) {
					resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
					e.printStackTrace();
				} catch (GeneralSecurityException e) {
					resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
					e.printStackTrace();
				}
			} else {
				resp.setStatus(HttpStatus.FORBIDDEN);
			}
		} else {
			resp.setStatus(HttpStatus.NOT_FOUND);
		}
		return resp;

	}

	@GET("/patient/{id}")
	public List<GcsConfigFile> getFileListByPatient(String id) {

		String patientId = decodeString(id);
		return new ArrayList<>(OfyService.ofy().load().type(GcsConfigFile.class).filter("patientId =", patientId).list());

	}

	/**
	 * Update File
	 * 
	 * @param user
	 * @return HttpStatus.NO_CONTENT if success
	 */
	@POST("/user/{id}")
	public HttpStatus UpdateFile(String id, GcsConfigFile file) {

		String userID = decodeString(id);

		if (file.getUserId().equals(userID) || file.isAccess()) {
			GcsConfigFile fileDB = Optional.fromNullable(OfyService.ofy().load().type(GcsConfigFile.class).id(file.getId()).now())
					.orNull();

			if (fileDB != null) {
				try {
					file.setContentType(fileDB.getContentType());
					file.setUserId(fileDB.getUserId());

					OfyService.ofy().save().entity(file).now();
				} catch (SaveException e) {
					return HttpStatus.INTERNAL_SERVER_ERROR;
				}
				return HttpStatus.OK;
			} else {
				return HttpStatus.NOT_FOUND;
			}
		} else {
			return HttpStatus.FORBIDDEN;
		}
	}

	@DELETE("/{fileid}/user/{userid}")
	public HttpStatus deleteFile(String fileid, String userid) {

		String userID = decodeString(userid);
		String fileID = decodeString(fileid);

		GcsConfigFile fileInfo = Optional.fromNullable(OfyService.ofy().load().type(GcsConfigFile.class).id(fileID).now()).orNull();

		if (fileInfo != null) {
			if (fileInfo.getUserId().equals(userID) || fileInfo.isAccess()) {
				try {
					Storage storage = StorageFactory.getService();
					storage.objects().delete(CGS_CONF_FILE_BUCKET, fileID).execute();
					OfyService.ofy().delete().type(GcsConfigFile.class).id(fileID).now();
				} catch (GeneralSecurityException | IOException e) {
					return HttpStatus.INTERNAL_SERVER_ERROR;
				}

			} else {
				return HttpStatus.FORBIDDEN;
			}
		} else {
			return HttpStatus.NOT_FOUND;
		}

		return HttpStatus.OK;

	}
}
