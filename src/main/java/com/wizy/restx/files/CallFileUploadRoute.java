package com.wizy.restx.files;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.io.output.ByteArrayOutputStream;

import com.google.api.client.http.InputStreamContent;
import com.wizy.model.gcs.GcsCallFile;
import com.wizy.ofy.OfyService;
import com.wizy.restx.Utils.PartsReader;
import com.wizy.restx.Utils.StorageResource;
import com.wizy.restx.Utils.PartsReader.FilePart;
import com.wizy.restx.Utils.PartsReader.Part;
import com.wizy.restx.Utils.PartsReader.PartListener;
import com.wizy.restx.Utils.PartsReader.TextPart;
import com.wizy.security.SecurityAccess;

import restx.RestxContext;
import restx.RestxRequest;
import restx.RestxRequestMatch;
import restx.RestxResponse;
import restx.StdRestxRequestMatcher;
import restx.StdRoute;

import restx.factory.Component;
import restx.http.HttpStatus;

@Component
public class CallFileUploadRoute extends StdRoute {

	final private String CGS_CALL_LOG_BUCKET = SecurityAccess.CGS_CALL_LOG_BUCKET;
	
	public CallFileUploadRoute() {
		super("upload", new StdRestxRequestMatcher("POST", "/upload/call/{userid}/{device}/{access}"));
	}

	@Override
	public void handle(RestxRequestMatch match, RestxRequest req, RestxResponse resp, RestxContext ctx)
			throws IOException {
		final String access = match.getPathParam("access");
		final String userId = match.getPathParam("userid");
		final String deviceId = match.getPathParam("device");

		new PartsReader(req).readParts(new PartListener() {
			public void onPart(Part part) throws IOException {
				System.out.println("--------------------------------------------------");
				
				if (part instanceof FilePart) {
					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					FilePart filePart = (FilePart) part;
					filePart.readStreamTo(outputStream);
					String filename = filePart.getFilename();
					String contentType = filePart.getContentType();
					int size = outputStream.size();

					System.out.println("filename = " + filename);
					System.out.println("contentType = " + contentType);
					System.out.println("size = " + size);
					
					// convert ByteArrayOutputStream to InputStreamContent
					byte[] result = outputStream.toByteArray();
					ByteArrayInputStream bInput = new ByteArrayInputStream(result);
					InputStreamContent data = new InputStreamContent(contentType, bInput) ;
					
					//Upload File
					String fileId = UUID.randomUUID().toString();
					StorageResource storageResource = new StorageResource();
					storageResource.uploadFile(fileId, contentType, data, CGS_CALL_LOG_BUCKET);
					
					//Save file info in DataBase
					GcsCallFile callFile = new GcsCallFile();
					callFile.setId(fileId);
					callFile.setName(filename);
					callFile.setContentType(contentType);
					callFile.setUserId(userId);
					callFile.setAccess(Boolean.getBoolean(access));
					callFile.setUploadDate(new Date());
					
					OfyService.ofy().save().entity(callFile).now();
					
				} else if (part instanceof TextPart) {
					String name = part.getName();
					String value = ((TextPart) part).getValue();
					System.out.println("name = " + name);
					System.out.println("value = " + value);
				}
			}
		});
		resp.setStatus(HttpStatus.OK);
	}

}
