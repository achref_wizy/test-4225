package com.wizy.restx.Utils;

import static restx.servlet.ServletModule.SERVLET_PRINCIPAL_CONVERTER;

import java.security.Principal;

import javax.inject.Named;

import restx.admin.AdminModule;
import restx.factory.Module;
import restx.factory.Provides;
import restx.security.RestxPrincipal;
import restx.servlet.ServletPrincipalConverter;

import com.google.appengine.api.utils.SystemProperty;
import com.google.common.collect.ImmutableSet;

@Module
public class GAEModule {
    @Provides
    @Named("restx.activation::restx.security.RestxSessionCookieFilter::RestxSessionCookieFilter")
    public String deactivateCookieSessionFilter() {
	return "false";
    }

    @Provides
    @Named("restx.activation::restx.security.RestxSessionBareFilter::RestxSessionBareFilter")
    public String activateBareSessionFilter() {
	return "true";
    }

    @Provides
    @Named("restx.admin.password")
    public String restxAdminPassword() {
	return "juma";
    }

    @Provides
    @Named(SERVLET_PRINCIPAL_CONVERTER)
    public ServletPrincipalConverter gaeServletPrincipalConverter() {
	return new ServletPrincipalConverter() {
	    @Override
	    public RestxPrincipal toRestxPrincipal(final Principal principal) {
		return new RestxPrincipal() {
		    @Override
		    public ImmutableSet<String> getPrincipalRoles() {
			if (isDevelopment()) {
			    return ImmutableSet.of(AdminModule.RESTX_ADMIN_ROLE);
			} else {
			    return ImmutableSet.of();
			}
		    }

		    @Override
		    public String getName() {
			return null;
			// return MarketplaceUserService.getCurrentUser().getEmail();
		    }
		};
	    }
	};
    }

    private boolean isDevelopment() {
	return SystemProperty.environment.value() == SystemProperty.Environment.Value.Development;
    }
}