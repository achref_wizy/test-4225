package com.wizy.restx.Utils;

//[START all]
/*
 * Copyright (c) 2014 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

import com.google.api.client.http.InputStreamContent;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.model.ObjectAccessControl;
import com.google.api.services.storage.model.Objects;
import com.google.api.services.storage.model.StorageObject;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import restx.annotations.GET;

public class StorageResource {


	// [START list_bucket]
	/**
	 * Fetch a list of the objects within the given bucket.
	 *
	 * @param bucketName
	 *            the name of the bucket to list.
	 * @return a list of the contents of the specified bucket.
	 */
	@GET("/bucket/{bucketName}/files")
	public static List<StorageObject> listBucket(String bucketName) {
		Storage client;
		List<StorageObject> results = new ArrayList<StorageObject>();
		try {
			client = StorageFactory.getService();
			Storage.Objects.List listRequest;
			listRequest = client.objects().list(bucketName);

			Objects objects;

			// Iterate through each page of results, and add them to our results
			// list.
			do {
				objects = listRequest.execute();
				// Add the items in this page of results to the list we'll
				// return.
				results.addAll(objects.getItems());

				// Get the next page, in the next iteration of this loop.
				listRequest.setPageToken(objects.getNextPageToken());
			} while (null != objects.getNextPageToken());
		} catch (IOException | GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return results;
	}

	// [END list_bucket]

	// [START upload_stream]
	/**
	 * Uploads data to an object in a bucket.
	 *
	 * @param name
	 *            the name of the destination object.
	 * @param contentType
	 *            the MIME type of the data.
	 * @param file
	 *            the file to upload. IOException, GeneralSecurityException
	 * @param bucketName
	 *            the name of the bucket to create the object in.
	 */
	public void uploadFile(String name, String contentType, InputStreamContent file, String bucketName) {
		try {
		
			// Setting the length improves upload performance
			//	contentStream.setLength(file.getFD());
			StorageObject objectMetadata = new StorageObject()
			// Set the destination object name
					.setName(name)
					// Set the access control list to publicly read-only
					.setAcl(Arrays.asList(new ObjectAccessControl().setEntity(
							"allUsers").setRole("READER")));

			// Do the insert
			Storage client = StorageFactory.getService();
			Storage.Objects.Insert insertRequest = client.objects()
					.insert(bucketName, objectMetadata,
							file);

			insertRequest.execute();
		} catch (IOException | GeneralSecurityException e) {

		}
	}

	// [END upload_stream]

	// [START delete_object]
	/**
	 * Deletes an object in a bucket.
	 *
	 * @param path
	 *            the path to the object to delete.
	 * @param bucketName
	 *            the bucket the object is contained in.
	 */
	public static void deleteObject(String path, String bucketName) {
		try {
			Storage client = StorageFactory.getService();
			client.objects().delete(bucketName, path).execute();
		} catch (IOException | GeneralSecurityException e) {
			e.printStackTrace();
		}
	}

	// [END delete_object]
}
// [END all]