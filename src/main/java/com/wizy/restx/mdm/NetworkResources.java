package com.wizy.restx.mdm;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.UUID;

import com.google.common.base.Optional;
import com.googlecode.objectify.SaveException;
import com.wizy.json.response.oneObject.AgencyWithStatus;
import com.wizy.model.mdm.agency.Wifi;
import com.wizy.ofy.OfyService;

import restx.annotations.DELETE;
import restx.annotations.GET;
import restx.annotations.POST;
import restx.annotations.RestxResource;
import restx.factory.Component;
import restx.http.HttpStatus;
import restx.security.PermitAll;

@Component
@RestxResource("/secured/network")
@PermitAll
public class NetworkResources {

	/**
	 * Decode a string with UTF-8
	 * 
	 * @param item
	 * @return
	 */
	public String decodeString(String item) {
		String decode_item = null;
		try {
			decode_item = URLDecoder.decode(item, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return decode_item;
	}

	/******************************************************************************************************
	 *
	 * WIFI MANAGEMENT
	 *
	 ******************************************************************************************************/
	/**
	 * Add Wifi
	 * 
	 * @param agency
	 * @return HttpStatus
	 */
	@POST("/wifi/add")
	public HttpStatus addWifi(Wifi wifi) {

		if (wifi.getAgencyId() == null) {
			return HttpStatus.BAD_REQUEST;
		}

		AgencyResources agencyResources = new AgencyResources();

		AgencyWithStatus checkAgency = agencyResources.getAgencyById(wifi.getAgencyId());

		if (!checkAgency.getStatus().equals(HttpStatus.OK)) {
			return HttpStatus.NOT_FOUND;
		}

		wifi.setId(UUID.randomUUID().toString());

		try {
			OfyService.ofy().save().entity(wifi).now();
		} catch (SaveException e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return HttpStatus.OK;
	}

	/**
	 * update Wifi
	 * 
	 * @param wifi
	 *            , wifiId
	 * @return HttpStatus
	 */
	@POST("/wifi/update/{id}")
	public HttpStatus updateWifi(String id, Wifi wifi) {

		String wifiId = decodeString(id);

		if (!wifiId.equals(wifi.getId()) || wifi.getAgencyId() == null) {
			return HttpStatus.BAD_REQUEST;
		}

		AgencyResources agencyResources = new AgencyResources();

		AgencyWithStatus checkAgency = agencyResources.getAgencyById(wifi.getAgencyId());

		if (!checkAgency.getStatus().equals(HttpStatus.OK)) {
			return HttpStatus.NOT_FOUND;
		}

		try {
			OfyService.ofy().save().entity(wifi).now();
		} catch (SaveException e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return HttpStatus.OK;
	}

	/**
	 * Get Wifi By Id
	 * 
	 * @param id
	 * @return
	 */
	@GET("/wifi/{id}")
	public Wifi getWifiById(String id) {

		String wifiId = decodeString(id);
		return Optional.fromNullable(OfyService.ofy().load().type(Wifi.class).id(wifiId).now()).orNull();

	}

	/**
	 * return wifiList
	 * 
	 * @return
	 */
	@GET("/list")
	public List<Wifi> listAllWifi() {
		return OfyService.ofy().load().type(Wifi.class).list();
	}

	/**
	 * return wifiList by AgnecyId
	 * 
	 * @return
	 */
	@GET("/list/agency/{agencyid}")
	public List<Wifi> listWifiByAgencyId(String agencyid) {
		
		String id = decodeString(agencyid);
		return OfyService.ofy().load().type(Wifi.class).filter("agencyId =", id).list();
	}

	/**
	 * Delete Wifi By Id
	 * 
	 * @param id
	 * @return
	 */
	@DELETE("/{id}")
	public HttpStatus deleteWifi(String id) {
		String wifiId = decodeString(id);
		try {
			OfyService.ofy().delete().type(Wifi.class).id(wifiId).now();
		} catch (Exception e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return HttpStatus.OK;
	}

}
