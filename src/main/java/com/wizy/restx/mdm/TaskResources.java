package com.wizy.restx.mdm;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.appengine.api.search.DeleteException;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Collections2;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.SaveException;
import com.wizy.json.mdm.TaskJson;
import com.wizy.json.response.oneObject.DeviceListWithStatus;
import com.wizy.json.response.oneObject.DeviceWithStatus;
import com.wizy.json.response.oneObject.TaskWithHttpStatus;
import com.wizy.json.response.twoObjects.MessageMultipleDevice;
import com.wizy.json.response.twoObjects.MultipleTaskListResp;
import com.wizy.json.response.twoObjects.TaskDeviceHttpstatusResp;
import com.wizy.model.mdm.Message;
import com.wizy.model.mdm.Task;
import com.wizy.model.mdm.device.Device;
import com.wizy.ofy.OfyService;

import restx.annotations.DELETE;
import restx.annotations.GET;
import restx.annotations.POST;
import restx.annotations.RestxResource;
import restx.factory.Component;
import restx.http.HttpStatus;
import restx.security.PermitAll;

@Component
@RestxResource("/secured/task")
@PermitAll
public class TaskResources {

	/**
	 * Decode a string with UTF-8
	 * 
	 * @param item
	 * @return
	 */
	public String decodeString(String item) {
		String decode_item = null;
		try {
			decode_item = URLDecoder.decode(item, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return decode_item;
	}

	/**
	 * Add new Task to One Device
	 * 
	 * @param task
	 * @return HttpStatus.NO_CONTENT if success
	 */
	@POST("/add/id/{id}/{action}")
	public TaskWithHttpStatus addTaskToOneDevice(String id, String action, Message message) {

		TaskWithHttpStatus resp = new TaskWithHttpStatus();
		Date today = new Date();
		String deviceId = decodeString(id);
		DeviceResources deviceResources = new DeviceResources();
		DeviceWithStatus deviceFromDb = deviceResources.getDeviceById(deviceId);

		if (deviceFromDb.getStatus().equals(HttpStatus.FOUND)) {

			if (!deviceFromDb.getDevice().getStatus().equals("active")) {
				resp.setStatus(HttpStatus.FORBIDDEN);
				return resp;
			}

			if (!action.equals("message")) {
				List<Task> existTaskList = OfyService.ofy().load().type(Task.class)
						.ancestor(Key.create(Device.class, deviceId)).filter("status =", "requested")
						.filter("action =", action).list();

				if (existTaskList.size() != 0) {
					resp.setStatus(HttpStatus.TOO_MANY_REQUESTS);
					return resp;
				}

				if (action.equals("lock")) {
					message.setTitle("lock");
					message.setType("pin");
					SecureRandom random = new SecureRandom();
					int num = random.nextInt(10000);
					String pin = String.format("%04d", num);
					message.setContent(pin);
					deviceFromDb.getDevice().setSecurityInformation(pin);
					try {
						OfyService.ofy().save().entity(deviceFromDb.getDevice()).now();
					} catch (SaveException e) {
						resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
						return resp;
					}

				}
			}

			Task task = new Task();
			task.setId(UUID.randomUUID().toString());
			task.setCreationDate(today);
			task.setAction(action.toLowerCase());
			task.setDeviceRef(deviceFromDb.getDevice());
			task.setStatus("requested");

			if (message != null) {
				message.setDate(today);
				task.setMessage(message);
			}

			try {
				Key<Task> task_key = OfyService.ofy().save().entity(task).now();
				resp.setTask(TaskJson.fromDatastore(OfyService.ofy().load().key(task_key).now()));
				resp.setStatus(HttpStatus.OK);
				return resp;
			} catch (SaveException e) {
				resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
				return resp;
			}
		} else {
			resp.setStatus(HttpStatus.NOT_FOUND);
			return resp;
		}
	}

	/**
	 * Add new Task to Multi Devices
	 * 
	 * @param task
	 * @return HttpStatus.NO_CONTENT if success
	 */
	@POST("/add/ids/{action}")
	public MultipleTaskListResp addTaskToMultiDevicesByIds(String action, MessageMultipleDevice payload) {

		MultipleTaskListResp resp = new MultipleTaskListResp();
		List<TaskJson> successList = new ArrayList<>();
		List<TaskJson> failList = new ArrayList<>();
		Date today = new Date();
		DeviceResources deviceResources = new DeviceResources();

		Message message = payload.getMessage();
		DeviceListWithStatus deviceListFromDb = deviceResources.getDeviceByListId(payload.getIds());

		resp.setStatus(HttpStatus.NOT_FOUND);
		if (deviceListFromDb.getStatus().equals(HttpStatus.FOUND)) {

			for (Device device : deviceListFromDb.getDeviceList()) {

				Task task = new Task();
				task.setId(UUID.randomUUID().toString());
				task.setCreationDate(today);
				task.setAction(action.toLowerCase());
				task.setDeviceRef(device);
				task.setStatus("requested");

				if (device.getStatus().equals("active")) {

					if (!action.equals("message")) {
						List<Task> existTaskList = OfyService.ofy().load().type(Task.class)
								.ancestor(Key.create(Device.class, device.getId())).filter("status =", "requested")
								.filter("action =", action).list();

						if (existTaskList.size() != 0) {
							failList.add(TaskJson.fromDatastore(task));
						} else {

							if (action.equals("lock")) {
								message = new Message();
								message.setDate(today);
								message.setTitle("lock");
								message.setType("pin");
								task.setMessage(message);
								SecureRandom random = new SecureRandom();
								int num = random.nextInt(10000);
								String pin = String.format("%04d", num);
								message.setContent(pin);
								device.setSecurityInformation(pin);
								try {
									OfyService.ofy().save().entity(device).now();
								} catch (SaveException e) {
									resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
									return resp;
								}
							}

							try {
								Key<Task> task_key = OfyService.ofy().save().entity(task).now();
								successList.add(TaskJson.fromDatastore(OfyService.ofy().load().key(task_key).now()));
							} catch (SaveException e) {
								failList.add(TaskJson.fromDatastore(task));
								continue;
							}
						}
					} else {

						message.setDate(today);
						task.setMessage(message);

						try {
							Key<Task> task_key = OfyService.ofy().save().entity(task).now();
							successList.add(TaskJson.fromDatastore(OfyService.ofy().load().key(task_key).now()));
						} catch (SaveException e) {
							failList.add(TaskJson.fromDatastore(task));
							continue;
						}
					}
				} else {
					failList.add(TaskJson.fromDatastore(task));
				}
			}
			resp.setSuccess(successList);
			resp.setError(failList);
			resp.setStatus(HttpStatus.OK);
		}
		if (!failList.isEmpty()) {
			resp.setStatus(HttpStatus.PARTIAL_CONTENT);
			if (successList.isEmpty()) {
				resp.setStatus(HttpStatus.CONFLICT);
			}
		}

		return resp;
	}

	/**
	 * Add new Task to Multi Devices
	 * 
	 * @param task
	 * @return HttpStatus.NO_CONTENT if success
	 */
	public MultipleTaskListResp addTaskToMultiDevicesByDevice(String action, Message message, List<Device> devices) {

		MultipleTaskListResp resp = new MultipleTaskListResp();
		List<TaskJson> successList = new ArrayList<>();
		List<TaskJson> failList = new ArrayList<>();
		Date today = new Date();

		for (Device device : devices) {

			Task task = new Task();
			task.setId(UUID.randomUUID().toString());
			task.setCreationDate(today);
			task.setAction(action.toLowerCase());
			task.setDeviceRef(device);
			task.setStatus("requested");
			if (message != null) {
				message.setDate(today);
				task.setMessage(message);
			}

			if (device.getStatus().equals("active")) {
				if (!action.equals("message")) {
					List<Task> existTaskList = OfyService.ofy().load().type(Task.class)
							.ancestor(Key.create(Device.class, device.getId())).filter("status =", "requested")
							.filter("action =", action).list();

					if (existTaskList.size() != 0) {
						failList.add(TaskJson.fromDatastore(task));
					} else {

						if (action.equals("lock")) {
							message = new Message();
							message.setDate(today);
							message.setTitle("lock");
							message.setType("pin");
							task.setMessage(message);
							SecureRandom random = new SecureRandom();
							int num = random.nextInt(10000);
							String pin = String.format("%04d", num);
							message.setContent(pin);
							device.setSecurityInformation(pin);
							try {
								OfyService.ofy().save().entity(device).now();
							} catch (SaveException e) {
								resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
								return resp;
							}
						}

						try {
							Key<Task> task_key = OfyService.ofy().save().entity(task).now();
							successList.add(TaskJson.fromDatastore(OfyService.ofy().load().key(task_key).now()));
						} catch (SaveException e) {
							failList.add(TaskJson.fromDatastore(task));
							continue;
						}
					}
				} else {

					message.setDate(today);
					task.setMessage(message);

					try {
						Key<Task> task_key = OfyService.ofy().save().entity(task).now();
						successList.add(TaskJson.fromDatastore(OfyService.ofy().load().key(task_key).now()));
					} catch (SaveException e) {
						failList.add(TaskJson.fromDatastore(task));
						continue;
					}
				}
			} else {
				failList.add(TaskJson.fromDatastore(task));
			}
		}
		resp.setSuccess(successList);
		resp.setError(failList);
		resp.setStatus(HttpStatus.OK);

		if (!failList.isEmpty()) {
			resp.setStatus(HttpStatus.PARTIAL_CONTENT);
			if (successList.isEmpty()) {
				resp.setStatus(HttpStatus.CONFLICT);
			}
		}

		return resp;
	}

	/**
	 * Add new Task to All Devices
	 * 
	 * @param task
	 * @return HttpStatus.NO_CONTENT if success
	 */
	@POST("/add/all/{action}")
	public MultipleTaskListResp addTaskToAllDevice(String action, Message message) {

		MultipleTaskListResp resp = new MultipleTaskListResp();
		DeviceResources DeviceResources = new DeviceResources();
		List<TaskJson> successList = new ArrayList<>();
		List<TaskJson> failList = new ArrayList<>();
		Date today = new Date();

		List<Device> devices = DeviceResources.listDevices();

		for (Device device : devices) {

			Task task = new Task();
			task.setId(UUID.randomUUID().toString());
			task.setCreationDate(today);
			task.setAction(action.toLowerCase());
			task.setDeviceRef(device);
			task.setStatus("requested");
			if (message != null) {
				message.setDate(today);
				task.setMessage(message);
			}

			if (device.getStatus().equals("active")) {
				if (!action.equals("message")) {
					List<Task> existTaskList = OfyService.ofy().load().type(Task.class)
							.ancestor(Key.create(Device.class, device.getId())).filter("status =", "requested")
							.filter("action =", action).list();

					if (existTaskList.size() != 0) {
						failList.add(TaskJson.fromDatastore(task));
					} else {

						if (action.equals("lock")) {
							message = new Message();
							message.setDate(today);
							message.setTitle("lock");
							message.setType("pin");
							task.setMessage(message);
							SecureRandom random = new SecureRandom();
							int num = random.nextInt(10000);
							String pin = String.format("%04d", num);
							message.setContent(pin);
							device.setSecurityInformation(pin);
							try {
								OfyService.ofy().save().entity(device).now();
							} catch (SaveException e) {
								resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
								return resp;
							}
						}

						try {
							Key<Task> task_key = OfyService.ofy().save().entity(task).now();
							successList.add(TaskJson.fromDatastore(OfyService.ofy().load().key(task_key).now()));
						} catch (SaveException e) {
							failList.add(TaskJson.fromDatastore(task));
							continue;
						}
					}
				} else {

					message.setDate(today);
					task.setMessage(message);

					try {
						Key<Task> task_key = OfyService.ofy().save().entity(task).now();
						successList.add(TaskJson.fromDatastore(OfyService.ofy().load().key(task_key).now()));
					} catch (SaveException e) {
						failList.add(TaskJson.fromDatastore(task));
						continue;
					}
				}
			} else {
				failList.add(TaskJson.fromDatastore(task));
			}
		}
		resp.setSuccess(successList);
		resp.setError(failList);
		resp.setStatus(HttpStatus.OK);

		if (!failList.isEmpty()) {
			resp.setStatus(HttpStatus.PARTIAL_CONTENT);
			if (successList.isEmpty()) {
				resp.setStatus(HttpStatus.CONFLICT);
			}
		}

		return resp;
	}

	/**
	 * Get Task By ID
	 * 
	 * @param id
	 * @return Device
	 */
	@GET("/{taskid}/device/{deviceid}")
	public TaskJson getTaskById(String taskid, String deviceid) {
		String taskId = decodeString(taskid);
		String deviceId = decodeString(deviceid);
		return TaskJson.fromDatastore(
				OfyService.ofy().load().type(Task.class).parent(Key.create(Device.class, deviceId)).id(taskId).now());
	}

	/**
	 * Update Task Status by Id
	 * 
	 * @param task
	 * @return HttpStatus.NO_CONTENT if success
	 */
	@GET("/id/{id}/device/{deviceid}/status/{status}")
	public HttpStatus updateTaskStatusById(String id, String deviceid, String status) {

		String taskId = decodeString(id);
		String deviceId = decodeString(deviceid);

		Task task = Optional.fromNullable(
				OfyService.ofy().load().type(Task.class).parent(Key.create(Device.class, deviceId)).id(taskId).now())
				.orNull();
		if (task == null) {
			return HttpStatus.NOT_FOUND;
		}
		task.setStatus(status);
		try {

			OfyService.ofy().save().entity(task).now();
		} catch (SaveException e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return HttpStatus.OK;
	}

	/**
	 * List All Tasks (without pagination)
	 * 
	 * @return List<Task>
	 */
	@GET("")
	public List<TaskJson> listTasks() {
		return new ArrayList<>(
				Collections2.transform(OfyService.ofy().load().type(Task.class).list(), new Function<Task, TaskJson>() {
					@Nullable
					@Override
					public TaskJson apply(Task task) {
						return TaskJson.fromDatastore(task);
					}
				}));
	}

	/**
	 * Delete Task By ID
	 * 
	 * @param id
	 * @return
	 */
	@DELETE("/id/{taskid}/device/{device}")
	public HttpStatus deleteTask(String taskid, String device) {
		String taskId = decodeString(taskid);
		String deviceId = decodeString(device);
		try {
			OfyService.ofy().delete().type(Task.class).parent(Key.create(Device.class, deviceId)).id(taskId).now();
		} catch (DeleteException e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return HttpStatus.OK;
	}

	/**
	 * Get and Update Task Status by device IMEI
	 * 
	 * @param task
	 * @return HttpStatus.NO_CONTENT if success
	 */
	@GET("/device/{id}")
	public TaskDeviceHttpstatusResp getAllTaskByDeviceId(String id) {

		String deviceId = decodeString(id);
		TaskDeviceHttpstatusResp resp = new TaskDeviceHttpstatusResp();
		Date today = new Date();

		if (deviceId == null) {
			resp.setStatus(HttpStatus.BAD_REQUEST);
			return resp;
		}

		DeviceResources deviceResources = new DeviceResources();
		DeviceWithStatus device = deviceResources.getDeviceById(deviceId);
		resp.setDevice(device.getDevice());

		if (device.getStatus() != null && device.getStatus().equals("inactive")) {
			resp.setStatus(HttpStatus.NOT_FOUND);
			resp.setTasks(null);
			return resp;
		}

		List<TaskJson> taskList = new ArrayList<>(Collections2.transform(OfyService.ofy().load().type(Task.class)
				.ancestor(Key.create(Device.class, deviceId)).filter("status =", "requested").list(),
				new Function<Task, TaskJson>() {
					@Nullable
					@Override
					public TaskJson apply(Task task) {
						return TaskJson.fromDatastore(task);
					}
				}));
		
		//Set Taken Date
		List<Task> takenTaskList = new ArrayList<>();
		for(TaskJson task:taskList){
			task.setTakenDate(today);
			task.setStatus("taken");
			takenTaskList.add(TaskJson.asDatastore(task));
		}
		OfyService.ofy().save().entities(takenTaskList).now();

		resp.setTasks(taskList);
		resp.setStatus(HttpStatus.OK);
		return resp;
	}

	/**********************************************************************************************
	 * 
	 * UPDATE ALL DEVICES
	 * 
	 **********************************************************************************************/

	/**
	 * updateAllDevices
	 * 
	 * @param action
	 * @param message
	 * @return
	 */
	@GET("/all/action/{action}")
	public HttpStatus updateAllDevices(String action, Message message) {

		List<Task> taskList = new ArrayList<>();

		List<Device> devicesList = OfyService.ofy().load().type(Device.class).filter("status =", "active").list();

		for (int i = 0; i < devicesList.size(); i++) {
			Task task = new Task();
			task.setId(UUID.randomUUID().toString());
			task.setCreationDate(new Date());
			task.setAction(action.toLowerCase());
			task.setDeviceRef(devicesList.get(i));
			task.setStatus("requested");
			task.setMessage(message);

			taskList.add(task);
		}
		if (taskList.isEmpty()) {
			return HttpStatus.NOT_FOUND;
		}
		try {
			OfyService.ofy().save().entities(taskList);
		} catch (SaveException e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return HttpStatus.OK;
	}
}
