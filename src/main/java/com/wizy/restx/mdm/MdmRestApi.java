package com.wizy.restx.mdm;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import com.wizy.json.response.multiObjects.DeviceAgencyWifiResp;
import com.wizy.json.response.oneObject.AgencyWithStatus;
import com.wizy.json.response.oneObject.DeviceWithStatus;
import com.wizy.json.response.oneObject.HttpStatusResponse;
import com.wizy.json.response.oneObject.StringToJsonResp;
import com.wizy.json.response.oneObject.TaskWithHttpStatus;
import com.wizy.json.response.oneObject.WifiListWithStatus;
import com.wizy.json.response.twoObjects.TaskDeviceHttpstatusResp;
import com.wizy.model.mdm.Message;
import com.wizy.model.mdm.agency.Wifi;
import com.wizy.model.mdm.device.Device;
import com.wizy.restx.mdm.UserResource.Invitation;

import restx.annotations.GET;
import restx.annotations.POST;
import restx.annotations.RestxResource;
import restx.factory.Component;
import restx.http.HttpStatus;
import restx.security.PermitAll;

@Component
@RestxResource("/keyaccess/mdm")
@PermitAll
public class MdmRestApi {

	TaskResources TaskResources = new TaskResources();
	DeviceResources DeviceResources = new DeviceResources();
	UserResource UserResource = new UserResource();

	/**
	 * Decode a string with UTF-8
	 * 
	 * @param item
	 * @return
	 */
	public String decodeString(String item) {
		String decode_item = null;
		try {
			decode_item = URLDecoder.decode(item, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return decode_item;
	}

	/******************************************************************************************************
	 *
	 * MULTI CLASS MANAGEMENT
	 *
	 ******************************************************************************************************/
	/**
	 * Get Device / Agency / Wifi By deviceID
	 * 
	 * @param id
	 * @return Device
	 */
	@GET("/synchronize/device/{id}")
	public DeviceAgencyWifiResp synchronizeByDeviceId(String id) {

		DeviceAgencyWifiResp resp = new DeviceAgencyWifiResp();
		DeviceResources deviceResources = new DeviceResources();
		AgencyResources agencyResources = new AgencyResources();
		NetworkResources networkResources = new NetworkResources();

		DeviceWithStatus deviceResp = deviceResources.getDeviceById(id);
		if (!deviceResp.getStatus().equals(HttpStatus.FOUND)) {
			resp.setStatus(HttpStatus.BAD_REQUEST);
			return resp;
		}

		resp.setDevice(deviceResp.getDevice());

		String agencyId = deviceResp.getDevice().getAgencyId();
		if (agencyId == null) {
			resp.setStatus(HttpStatus.NOT_FOUND);
			return resp;
		}

		AgencyWithStatus agencyResp = agencyResources.getAgencyById(agencyId);
		if (!agencyResp.getStatus().equals(HttpStatus.OK)) {
			resp.setStatus(HttpStatus.PRECONDITION_FAILED);
			return resp;
		}

		resp.setAgency(agencyResp.getAgency());

		List<Wifi> wifiList = networkResources.listWifiByAgencyId(agencyId);

		if (wifiList.isEmpty()) {
			resp.setStatus(HttpStatus.NOT_IMPLEMENTED);
			return resp;
		}

		resp.setWifi(wifiList);
		resp.setStatus(HttpStatus.OK);
		return resp;

	}

	/******************************************************************************************************
	 * 
	 * USER MANAGEMENT
	 * 
	 ******************************************************************************************************/

	/**
	 * Add Admin to application
	 * 
	 * @param email
	 * @return
	 */
	@GET("/admin/{email}")
	public HttpStatus InviteUser(String email) {
		Invitation invi = new Invitation();
		invi.setDisplayName("Admin Name");
		invi.setRole("Admin");
		invi.setUserId(email);
		return UserResource.inviteUser(invi);
	}

	/******************************************************************************************************
	 * 
	 * DEVICE MANAGEMENT
	 * 
	 ******************************************************************************************************/

	/**
	 * Get Device By ID
	 * 
	 * @param id
	 * @return Device
	 */
	@GET("/device/{id}")
	public DeviceWithStatus getDeviceById(String id) {
		DeviceResources deviceResources = new DeviceResources();
		return deviceResources.getDeviceById(id);
	}

	/**
	 * Get AgencyId By deviceId
	 * 
	 * @param id
	 * @return Device
	 */
	@GET("/device/{id}/agencyid")
	public StringToJsonResp getAgencyIdByDeviceId(String id) {
		StringToJsonResp resp = new StringToJsonResp();

		DeviceWithStatus deviceResp = getDeviceById(id);
		if (!deviceResp.getStatus().equals(HttpStatus.FOUND)) {
			resp.setStatus(HttpStatus.NOT_FOUND);
			return resp;
		}
		if (deviceResp.getDevice().getAgencyId() == null) {
			resp.setStatus(HttpStatus.NO_CONTENT);
			return resp;
		}
		resp.setItem(deviceResp.getDevice().getAgencyId());
		resp.setStatus(HttpStatus.OK);

		return resp;
	}

	/**
	 * masterise Device
	 * 
	 * @param device
	 * @return HttpStatus.NO_CONTENT if success
	 */
	@POST("/device/masterise")
	public HttpStatusResponse masteriseDevice(Device device) {
		return DeviceResources.masteriseDevice(device);
	}

	/**
	 * Patch devise
	 * 
	 * @param device
	 * @param id
	 * @return
	 */
	@POST("/device/patch/{id}")
	public HttpStatusResponse patchDevice(Device device, String id) {
		return DeviceResources.patchDevice(device, id);
	}

	/**
	 * update device & taskStatus
	 * 
	 * @param device
	 * @param id
	 * @return
	 */
	@POST("/device/update/{id}/task/{task}")
	public HttpStatusResponse patchDeviceAndUpdateStatus(Device device, String id, String task) {

		HttpStatusResponse resp = new HttpStatusResponse();
		HttpStatusResponse update = DeviceResources.patchDevice(device, id);
		if (update.getStatus().equals(HttpStatus.OK)) {
			HttpStatus statusUpdate = TaskResources.updateTaskStatusById(task, id, "done");
			resp.setStatus(statusUpdate);
			return resp;
		} else {
			return update;
		}
	}

	/**
	 * Lock device & updare taskStatus
	 * 
	 * @param device
	 * @param id
	 * @return
	 */
	@POST("/device/unlock/{id}/task/{task}")
	public HttpStatusResponse lockDeviceAndUpdateStatus(String id, String task) {

		HttpStatusResponse resp = new HttpStatusResponse();
		DeviceWithStatus device = getDeviceById(id);
		if (!device.getStatus().equals(HttpStatus.FOUND)) {
			resp.setStatus(device.getStatus());
			return resp;
		}
		Device lockDevice = device.getDevice();
		lockDevice.setLock(false);
		HttpStatusResponse update = DeviceResources.patchDevice(lockDevice, id);
		if (update.getStatus().equals(HttpStatus.OK)) {
			HttpStatus statusUpdate = TaskResources.updateTaskStatusById(task, id, "done");
			resp.setStatus(statusUpdate);
			return resp;
		} else {
			return update;
		}
	}

	/******************************************************************************************************
	 *
	 * TASK MANAGEMENT
	 *
	 ******************************************************************************************************/

	/**
	 * Add new Task to One Device
	 * 
	 * @param task
	 * @return HttpStatus.NO_CONTENT if success
	 */
	@POST("/task/add/id/{id}/{action}")
	public TaskWithHttpStatus addTaskToOneDevice(String id, String action, Message message) {
		return TaskResources.addTaskToOneDevice(id, action, message);
	}

	/**
	 * Get and Update Task Status by device IMEI
	 * 
	 * @param task
	 * @return HttpStatus.NO_CONTENT if success
	 */
	@GET("/task/{deviceid}")
	public TaskDeviceHttpstatusResp GetAllTaskByDeviceId(String deviceid) {
		return TaskResources.getAllTaskByDeviceId(deviceid);
	}

	/**
	 * Update Task Status by Id
	 * 
	 * @param task
	 * @return HttpStatus.NO_CONTENT if success
	 */
	@GET("/task/{id}/device/{deviceid}/status/{status}")
	public HttpStatusResponse updateTaskStatusById(String id, String deviceid, String status) {
		HttpStatusResponse resp = new HttpStatusResponse();
		resp.setStatus(TaskResources.updateTaskStatusById(id, deviceid, status));
		return resp;

	}

	/******************************************************************************************************
	 *
	 * AGENCY MANAGEMENT
	 *
	 ******************************************************************************************************/
	@GET("/agency/register/{agencyid}/{agencycode}/device/{deviceid}")
	public HttpStatusResponse registerDevice(String agencyid, String agencycode, String deviceid) {

		HttpStatusResponse resp = new HttpStatusResponse();
		DeviceResources DeviceResources = new DeviceResources();

		String agencyId = decodeString(agencyid);
		String deviceId = decodeString(deviceid);
		Device device = new Device();
		device.setAgencyId(agencyId);
		device.setId(deviceId);

		resp.setStatus(DeviceResources.AcitivateDevice(agencycode, device));
		return resp;

	}

	/******************************************************************************************************
	 *
	 * WIFI MANAGEMENT
	 *
	 ******************************************************************************************************/
	@GET("/wifi/agency/{agencyid}")
	public WifiListWithStatus getWifiByIgencyId(String agencyid) {

		WifiListWithStatus resp = new WifiListWithStatus();
		NetworkResources NetworkResources = new NetworkResources();

		String agencyId = decodeString(agencyid);

		List<Wifi> list = NetworkResources.listWifiByAgencyId(agencyId);

		if (list.isEmpty()) {
			resp.setStatus(HttpStatus.NOT_FOUND);
			return resp;
		}

		resp.setWifiList(list);
		resp.setStatus(HttpStatus.OK);
		return resp;

	}

}
