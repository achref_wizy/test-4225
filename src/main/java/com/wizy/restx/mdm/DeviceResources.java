package com.wizy.restx.mdm;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.google.appengine.api.search.DeleteException;
import com.google.common.base.Optional;
import com.googlecode.objectify.SaveException;
import com.googlecode.objectify.cmd.Query;
import com.wizy.json.JsonCollection;
import com.wizy.json.response.oneObject.AgencyWithStatus;
import com.wizy.json.response.oneObject.DeviceListWithStatus;
import com.wizy.json.response.oneObject.DeviceWithStatus;
import com.wizy.json.response.oneObject.HttpStatusResponse;
import com.wizy.json.response.twoObjects.MultipleDeviceListResp;
import com.wizy.model.mdm.device.Device;
import com.wizy.ofy.OfyService;

import restx.annotations.DELETE;
import restx.annotations.GET;
import restx.annotations.POST;
import restx.annotations.RestxResource;
import restx.factory.Component;
import restx.http.HttpStatus;
import restx.security.PermitAll;

@Component
@RestxResource("/secured/device")
@PermitAll
public class DeviceResources {

	/**
	 * Decode a string with UTF-8
	 * 
	 * @param item
	 * @return
	 */
	public String decodeString(String item) {
		String decode_item = null;
		try {
			decode_item = URLDecoder.decode(item, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return decode_item;
	}

	/**
	 * Add Device
	 * 
	 * @param device
	 * @return
	 */
	@POST("/add")
	public HttpStatusResponse addDevice(Device device) {

		HttpStatusResponse resp = new HttpStatusResponse();

		if (device.getImei() == null) {
			resp.setStatus(HttpStatus.BAD_REQUEST);
			return resp;
		}

		device.setId(device.getImei());
		DeviceWithStatus deviceFromDb = getDeviceById(device.getId());

		if (deviceFromDb.getStatus().equals(HttpStatus.FOUND)) {
			resp.setStatus(HttpStatus.FOUND);
			return resp;
		}

		device.setDate(new Date());
		try {
			OfyService.ofy().save().entity(device).now();
		} catch (SaveException e) {
			resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			return resp;
		}

		resp.setStatus(HttpStatus.CREATED);
		return resp;
	}

	/**
	 * Add Device
	 * 
	 * @param device
	 * @return
	 */
	@POST("/add/list")
	public MultipleDeviceListResp addListDevice(List<Device> devices) {

		MultipleDeviceListResp resp = new MultipleDeviceListResp();
		List<DeviceWithStatus> success = new ArrayList<>();
		List<DeviceWithStatus> error = new ArrayList<>();
		List<Device> deviceDB = new ArrayList<>();

		for (Device device : devices) {

			if (device.getImei() == null) {
				DeviceWithStatus deviceWithStatus = new DeviceWithStatus();
				deviceWithStatus.setStatus(HttpStatus.BAD_REQUEST);
				deviceWithStatus.setDevice(device);
				error.add(deviceWithStatus);
			}

			device.setId(device.getImei());
			DeviceWithStatus deviceFromDb = getDeviceById(device.getId());

			if (deviceFromDb.getStatus().equals(HttpStatus.FOUND)) {
				DeviceWithStatus deviceWithStatus = new DeviceWithStatus();
				deviceWithStatus.setStatus(HttpStatus.FOUND);
				deviceWithStatus.setDevice(device);
				error.add(deviceWithStatus);
			}

			device.setDate(new Date());
			DeviceWithStatus deviceWithStatus = new DeviceWithStatus();
			deviceWithStatus.setDevice(device);
			success.add(deviceWithStatus);
			deviceDB.add(device);
		}

		try {
			OfyService.ofy().save().entity(deviceDB).now();
		} catch (SaveException e) {
			resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			return resp;
		}

		if (!error.isEmpty()) {
			if (success.isEmpty()) {
				resp.setStatus(HttpStatus.EXPECTATION_FAILED);
			}
			resp.setStatus(HttpStatus.PARTIAL_CONTENT);
			resp.setError(error);
		}

		resp.setSuccess(success);
		resp.setStatus(HttpStatus.OK);
		return resp;
	}

	/**
	 * masteriseDevice
	 * 
	 * @param device
	 * @return
	 */
	public HttpStatusResponse masteriseDevice(Device device) {

		HttpStatusResponse resp = new HttpStatusResponse();

		if (device.getImei() == null) {
			resp.setStatus(HttpStatus.BAD_REQUEST);
			return resp;
		}

		device.setId(device.getImei());
		DeviceWithStatus deviceFromDb = getDeviceById(device.getId());

		if (!deviceFromDb.getStatus().equals(HttpStatus.FOUND)) {
			resp.setStatus(HttpStatus.FORBIDDEN);
			return resp;
		}
		device.setStatus("masterised");
		try {
			OfyService.ofy().save().entity(device).now();
		} catch (SaveException e) {
			resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			return resp;
		}

		resp.setStatus(HttpStatus.ACCEPTED);
		return resp;
	}

	/**
	 * Get Device By IMEI
	 * 
	 * @param id
	 * @return Device
	 */
	@GET("/id/{id}")
	public DeviceWithStatus getDeviceById(String id) {

		DeviceWithStatus resp = new DeviceWithStatus();
		String deviceId = decodeString(id);

		Device device = Optional.fromNullable(OfyService.ofy().load().type(Device.class).id(deviceId).now()).orNull();

		if (device == null) {
			resp.setStatus(HttpStatus.NOT_FOUND);
			return resp;
		}

		resp.setStatus(HttpStatus.FOUND);
		resp.setDevice(device);
		return resp;
	}

	/**
	 * Get Device By List<IMEI>
	 * 
	 * @param id
	 * @return Device
	 */
	@POST("/ids")
	public DeviceListWithStatus getDeviceByListId(List<String> ids) {

		DeviceListWithStatus resp = new DeviceListWithStatus();

		Map<String, Device> deviceList = OfyService.ofy().load().type(Device.class).ids(ids);

		if (deviceList.isEmpty()) {
			resp.setStatus(HttpStatus.NOT_FOUND);
			return resp;
		}

		resp.setStatus(HttpStatus.FOUND);
		resp.setDeviceList(new ArrayList<Device>(deviceList.values()));
		return resp;
	}

	/**
	 * List All Devices (without pagination)
	 * 
	 * @return List<Device>
	 */
	@GET("")
	public List<Device> listDevices() {
		return OfyService.ofy().load().type(Device.class).list();
	}

	/**
	 * List All Devices By filter(without pagination)
	 * 
	 * @return List<Device>
	 */
	@GET("/filter/name/{name}/value/{value}")
	public List<Device> listDevicesByFilter(String name, String value) {

		String filterName = decodeString(name);
		String filterValue = decodeString(value);

		return OfyService.ofy().load().type(Device.class).filter(filterName + " = ", filterValue).list();
	}

	/**
	 * List All Devices By 2 filters(without pagination)
	 * 
	 * @return List<Device>
	 */
	@GET("/filter/name1/{name1}/value1/{value1}/name2/{name2}/value2/{value2}")
	public List<Device> listDevicesBy2Filters(String name1, String value1, String name2, String value2) {

		String filterName1 = decodeString(name1);
		String filterValue1 = decodeString(value1);
		String filterName2 = decodeString(name2);
		String filterValue2 = decodeString(value2);

		return OfyService.ofy().load().type(Device.class).filter(filterName1 + " = ", filterValue1)
				.filter(filterName2 + " = ", filterValue2).list();
	}

	/**
	 * Count All device
	 * 
	 * @return
	 */
	@GET("/countall")
	public Integer countAllDevices() {
		return OfyService.ofy().load().type(Device.class).count();
	}

	/**
	 * Get all Device with pagination (no filter)
	 *
	 * @param maxResults
	 * @param pageToken
	 * @return List<UserJson>
	 */
	@GET("/pagination")
	public JsonCollection<Device> listDevicesPagination(Optional<Integer> maxResults, Optional<String> pageToken) {

		if (!maxResults.isPresent()) {
			maxResults = Optional.of(50);
		}

		Query<Device> query = OfyService.ofy().load().type(Device.class).limit(maxResults.get());

		if (pageToken.isPresent()) {
			query = query.startAt(Cursor.fromWebSafeString(pageToken.get()));
		}
		QueryResultIterator<Device> iterator = query.iterator();
		List<Device> items = new ArrayList<>(maxResults.get());
		boolean hasMore = iterator.hasNext();
		while (iterator.hasNext()) {
			items.add(iterator.next());
		}
		return new JsonCollection<>(items, hasMore ? iterator.getCursor().toWebSafeString() : null);
	}

	/**
	 * Get Devices by Filter
	 *
	 * @param id
	 */
	@GET("/filter/name/{name}/value/{value}")
	public JsonCollection<Device> listDevicesByFilterPagination(String name, String value, Optional<Integer> maxResults,
			Optional<String> pageToken) {
		if (!maxResults.isPresent()) {
			maxResults = Optional.of(50);
		}

		String filterName = decodeString(name);
		String filterValue = decodeString(value);

		Query<Device> query = OfyService.ofy().load().type(Device.class).filter(filterName + " = ", filterValue)
				.limit(maxResults.get());
		if (pageToken.isPresent()) {
			query = query.startAt(Cursor.fromWebSafeString(pageToken.get()));
		}
		QueryResultIterator<Device> iterator = query.iterator();
		List<Device> items = new ArrayList<>(maxResults.get());
		boolean hasMore = iterator.hasNext();
		while (iterator.hasNext()) {
			items.add(iterator.next());
		}
		return new JsonCollection<>(items, hasMore ? iterator.getCursor().toWebSafeString() : null);
	}

	/**
	 * update Device
	 * 
	 * @param device
	 * @return HttpStatus.NO_CONTENT if success
	 */
	@POST("/update/{id}")
	public HttpStatusResponse updateDevice(Device device, String id) {

		HttpStatusResponse resp = new HttpStatusResponse();
		String deviceId = decodeString(id);
		DeviceWithStatus deviceFromDBResp = getDeviceById(deviceId);
		if (deviceFromDBResp.getStatus().equals(HttpStatus.NOT_FOUND)) {
			resp.setStatus(HttpStatus.NOT_FOUND);
			return resp;
		}
		device.setId(deviceId);
		device.setDate(new Date());
		try {
			OfyService.ofy().save().entity(device).now();
		} catch (SaveException e) {
			resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			return resp;
		}
		resp.setStatus(HttpStatus.OK);
		return resp;
	}

	/**
	 * patch Device (longitude, Latitude, battery)
	 * 
	 * @param device
	 * @return HttpStatus.NO_CONTENT if success
	 */
	@POST("/patch/{id}")
	public HttpStatusResponse patchDevice(Device device, String id) {

		HttpStatusResponse resp = new HttpStatusResponse();
		String deviceId = decodeString(id);
		DeviceWithStatus deviceFromDBResp = getDeviceById(deviceId);
		if (deviceFromDBResp.getStatus().equals(HttpStatus.NOT_FOUND)) {
			resp.setStatus(HttpStatus.NOT_FOUND);
			return resp;
		}
		boolean modif = false;
		if (device.getName() != null) {
			deviceFromDBResp.getDevice().setName(device.getName());
			modif = true;
		}
		if (device.getFcmToken() != null) {
			deviceFromDBResp.getDevice().setFcmToken(device.getFcmToken());
			modif = true;
		}
		if (device.getIccId() != null) {
			deviceFromDBResp.getDevice().setIccId(device.getIccId());
			modif = true;
		}
		if (device.getPhoneNumber() != null) {
			deviceFromDBResp.getDevice().setPhoneNumber(device.getPhoneNumber());
			modif = true;
		}
		if (device.getMemoryUsage() != null) {
			deviceFromDBResp.getDevice().setMemoryUsage(device.getMemoryUsage());
			modif = true;
		}
		if (device.getSecurityInformation() != null) {
			deviceFromDBResp.getDevice().setSecurityInformation(device.getSecurityInformation());
			modif = true;
		}
		if (device.getAppVersion() != null) {
			deviceFromDBResp.getDevice().setAppVersion(device.getAppVersion());
			modif = true;
		}
		if (device.getAndroidVersion() != null) {
			deviceFromDBResp.getDevice().setAndroidVersion(device.getAndroidVersion());
			modif = true;
		}
		if (device.getBatteryLevel() != null) {
			deviceFromDBResp.getDevice().setBatteryLevel(device.getBatteryLevel());
			modif = true;
		}
		if (device.getBatteryStatus() != null) {
			deviceFromDBResp.getDevice().setBatteryStatus(device.getBatteryStatus());
			modif = true;
		}
		if (device.getBatteryCyclesNbr() != null) {
			deviceFromDBResp.getDevice().setBatteryCyclesNbr(device.getBatteryCyclesNbr());
			modif = true;
		}
		if (device.getLongitude() != null) {
			deviceFromDBResp.getDevice().setLongitude(device.getLongitude());
			modif = true;
		}
		if (device.getLatitude() != null) {
			deviceFromDBResp.getDevice().setLatitude(device.getLatitude());
			modif = true;
		}
		if (modif == true) {
			deviceFromDBResp.getDevice().setDate(new Date());
		}
		try {
			OfyService.ofy().save().entity(deviceFromDBResp.getDevice()).now();
		} catch (SaveException e) {
			resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			return resp;
		}
		resp.setStatus(HttpStatus.OK);
		return resp;
	}

	/**
	 * 
	 * @param agency_code
	 * @param device
	 * @return
	 */
	@POST("/register/agency/{agencycode}")
	public HttpStatus AcitivateDevice(String agencycode, Device device) {

		String agencyCode = decodeString(agencycode);

		if (device.getId() == null || device.getAgencyId() == null) {
			return HttpStatus.BAD_REQUEST;
		}

		AgencyResources AgencyResources = new AgencyResources();
		AgencyWithStatus agencyResp = AgencyResources.getAgencyById(device.getAgencyId());

		if (!agencyResp.getStatus().equals(HttpStatus.OK)) {
			return HttpStatus.NOT_FOUND;
		}

		DeviceResources DeviceResources = new DeviceResources();
		DeviceWithStatus deviceToUpdateResp = DeviceResources.getDeviceById(device.getId());

		if (!deviceToUpdateResp.getStatus().equals(HttpStatus.FOUND)) {
			return HttpStatus.NOT_FOUND;
		}

		if (!agencyResp.getAgency().getCode().equals(agencyCode)) {
			return HttpStatus.FORBIDDEN;
		}

		try {
			deviceToUpdateResp.getDevice().setStatus("active");
			OfyService.ofy().save().entity(deviceToUpdateResp.getDevice()).now();
		} catch (SaveException e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return HttpStatus.OK;

	}

	/**
	 * Patch list of devices status
	 * 
	 * @param status
	 * @param ids
	 * @return
	 */
	@POST("/list/patch/status/{status}")
	public HttpStatus changeDeviceListStatus(String status, List<String> ids) {

		DeviceListWithStatus devicesList = getDeviceByListId(ids);
		List<Device> SaveListDevice = new ArrayList<>();
		if (!devicesList.getStatus().equals(HttpStatus.FOUND)) {
			return HttpStatus.NOT_FOUND;
		}

		for (Device device : devicesList.getDeviceList()) {
			device.setStatus(status);
			SaveListDevice.add(device);
		}

		try {
			OfyService.ofy().save().entities(SaveListDevice).now();
		} catch (SaveException e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return HttpStatus.OK;
	}

	/**
	 * Delete Device By ID
	 * 
	 * @param id
	 * @return
	 */
	@DELETE("/id/{id}")
	public HttpStatus deleteDevice(String id) {
		try {
			OfyService.ofy().delete().type(Device.class).id(id).now();
		} catch (DeleteException e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return HttpStatus.OK;
	}
}
