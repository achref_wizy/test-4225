package com.wizy.restx.mdm;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.google.common.base.Optional;
import com.googlecode.objectify.SaveException;
import com.wizy.json.response.oneObject.AgencyWithStatus;
import com.wizy.model.mdm.agency.Agency;
import com.wizy.ofy.OfyService;

import restx.annotations.DELETE;
import restx.annotations.GET;
import restx.annotations.POST;
import restx.annotations.RestxResource;
import restx.factory.Component;
import restx.http.HttpStatus;
import restx.security.PermitAll;

@Component
@RestxResource("/secured/agency")
@PermitAll
public class AgencyResources {

	/**
	 * Decode a string with UTF-8
	 * 
	 * @param item
	 * @return
	 */
	public String decodeString(String item) {
		String decode_item = null;
		try {
			decode_item = URLDecoder.decode(item, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return decode_item;
	}

	/**
	 * Add Agency
	 * 
	 * @param agency
	 * @return HttpStatus
	 */
	@POST("/add")
	public HttpStatus addAgency(Agency agency) {

		if(agency.getCode() == null ){
			return  HttpStatus.BAD_REQUEST;
		}
		
		if(CheckAgencyExistByCode(agency.getCode())){
			return HttpStatus.FOUND;
		}
		
		agency.setId(UUID.randomUUID().toString());
		agency.setDate(new Date());
		try {
			OfyService.ofy().save().entity(agency).now();
		} catch (SaveException e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return HttpStatus.OK;
	}

	/**
	 * update Agency
	 * 
	 * @param agency
	 * @return HttpStatus
	 */
	@POST("/update/{id}")
	public HttpStatus updateAgency(String id, Agency agency) {

		String agencyId = decodeString(id);

		if (!agencyId.equals(agency.getId())) {
			return HttpStatus.BAD_REQUEST;
		}

		agency.setDate(new Date());

		try {
			OfyService.ofy().save().entity(agency).now();
		} catch (SaveException e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return HttpStatus.OK;
	}

	/**
	 * Get agency By Id
	 * 
	 * @param id
	 * @return
	 */
	@GET("/id/{id}")
	public AgencyWithStatus getAgencyById(String id) {

		AgencyWithStatus resp = new AgencyWithStatus();

		String agencyId = decodeString(id);
		Agency agency = Optional.fromNullable(OfyService.ofy().load().type(Agency.class).id(agencyId).now()).orNull();
		if (agency == null) {
			resp.setStatus(HttpStatus.NOT_FOUND);
			return resp;
		}
		resp.setAgency(agency);
		resp.setStatus(HttpStatus.OK);
		return resp;

	}
	
	/**
	 * Get agency By Code
	 * 
	 * @param id
	 * @return
	 */
	@GET("/code/{code}")
	public Boolean CheckAgencyExistByCode(String code) {

		String agencyCode = decodeString(code);
		return !(OfyService.ofy().load().type(Agency.class).filter("code",agencyCode).list().isEmpty());

	}

	/**
	 * return AgencyList
	 * 
	 * @return
	 */
	@GET("/list")
	public List<Agency> listAgency() {
		return OfyService.ofy().load().type(Agency.class).list();
	}

	/**
	 * Delete Agency By Id
	 * @param id
	 * @return
	 */
	@DELETE("/{id}")
	public HttpStatus deleteAgency(String id) {
		String agencyId = decodeString(id);
		try {
			OfyService.ofy().delete().type(Agency.class).id(agencyId).now();
		} catch (Exception e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return HttpStatus.OK;
	}

}
