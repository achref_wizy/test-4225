package com.wizy.restx.mdm;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Nullable;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Collections2;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.SaveException;
import com.googlecode.objectify.cmd.Query;
import com.wizy.json.JsonCollection;
import com.wizy.json.admin.UserJson;
import com.wizy.json.response.oneObject.LoginResponse;
import com.wizy.json.response.oneObject.UserResponse;
import com.wizy.model.admin.User;
import com.wizy.ofy.OfyService;
import com.wizy.security.MarketplaceUserService;

import restx.annotations.DELETE;
import restx.annotations.GET;
import restx.annotations.POST;
import restx.annotations.RestxResource;
import restx.factory.Component;
import restx.http.HttpStatus;
import restx.security.PermitAll;

@Component
@RestxResource("/secured/users")
@PermitAll
public class UserResource {

	public static class Invitation {

		private String userId;
		private String displayName;
		private String role;

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getDisplayName() {
			return displayName;
		}

		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}

		public String getRole() {
			return role;
		}

		public void setRole(String role) {
			this.role = role;
		}

		public Invitation() {
		}

	}

	private static final String USER_ME = "me";

	/**
	 * Decode a string with UTF-8
	 * 
	 * @param item
	 * @return
	 */
	public String decodeString(String item) {
		String decode_item = null;
		try {
			decode_item = URLDecoder.decode(item, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return decode_item;
	}
	
	/**
	 * Get user from datastore by id
	 * 
	 * @param id
	 * @return User (entity in datastore)
	 */
	User getUserFromDatastore(String id) {
		String userId = null;
		try {
			userId = URLDecoder.decode(id, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return OfyService.ofy().load().type(User.class).id(userId).now();
	}

	/**
	 * Get User By ID
	 * 
	 * @param id
	 * @return UserJson
	 */
	@GET("/{id}")
	public UserResponse getUser(String id) {

		UserResponse resp = new UserResponse();
		User user = new User();
		String userId = decodeString(id);

		if (USER_ME.equals(userId)) {
			user = getUserFromDatastore(MarketplaceUserService.getCurrentUser()
					.getId());

		} else {
			user = getUserFromDatastore(userId);
		}

		if (user != null) {
			resp.setStatus(HttpStatus.OK);
			resp.setUser(UserJson.fromDatastore(user));
		} else {
			resp.setStatus(HttpStatus.NOT_FOUND);
		}

		return resp;
	}

	/**
	 * save Or Update User
	 * 
	 * @param user
	 * @return HttpStatus.NO_CONTENT if success
	 */
	@POST("")
	public HttpStatus saveOrUpdateUser(User user) {
		try {
			OfyService.ofy().save().entity(user).now();
		} catch (SaveException e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return HttpStatus.OK;
	}
	
	/**
	 * update UserJson
	 * 
	 * @param user
	 * @return HttpStatus.NO_CONTENT if success
	 */
	@POST("/update")
	public HttpStatus updateUserJson(UserJson user) {
		try {
			OfyService.ofy().save().entity(user.asDatastoreUser()).now();
		} catch (SaveException e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return HttpStatus.OK;
	}

	/**
     * List All user (without pagination)
     * 
     * @return List<UserJson>
     */
    @GET("")
    public List<UserJson> listUsers() {

	return new ArrayList<>(
		Collections2.transform(OfyService.ofy().load().type(User.class).list(), new Function<User, UserJson>() {
		    @Nullable
		    @Override
		    public UserJson apply(User user) {
			return UserJson.fromDatastore(user);
		    }
		}));
    }


	/**
	 * Get all users with pagination
	 *
	 * @param maxResults
	 * @param pageToken
	 * @return List<User>
	 */
	@GET("/all/pagination")
	public JsonCollection<User> listUsersPagination(
			Optional<Integer> maxResults, Optional<String> pageToken) {

		if (!maxResults.isPresent()) {
			maxResults = Optional.of(50);
		}

		Query<User> query = OfyService.ofy().load().type(User.class)
				.limit(maxResults.get());

		if (pageToken.isPresent()) {
			query = query.startAt(Cursor.fromWebSafeString(pageToken.get()));
		}
		QueryResultIterator<User> iterator = query.iterator();
		List<User> items = new ArrayList<>(maxResults.get());
		boolean hasMore = iterator.hasNext();
		while (iterator.hasNext()) {
			items.add(iterator.next());
		}
		return new JsonCollection<>(items, hasMore ? iterator.getCursor()
				.toWebSafeString() : null);
	}

	/**
	 * Login to App
	 * 
	 * @return
	 */
	@GET("/login/{email}")
	public LoginResponse login(String email) {

		LoginResponse resp = new LoginResponse();
		Date toDay = new Date();

		int userCount = OfyService.ofy().load().type(User.class).count();
		if (userCount > 0) {
			String userId = decodeString(email);
			UserResponse userResp = getUser(userId);
			resp.setUser(userResp.getUser());
			resp.setStatus(userResp.getStatus());
		} else {
			User user = new User();
			user.setId(email);
			user.setEmail(email);
			user.setSignInDate(toDay);
			user.setLastAccess(toDay);
			user.setRole("ADMIN");
			Key<User> userKey = OfyService.ofy().save().entity(user).now();
			User userCreated = OfyService.ofy().load().key(userKey).now();
			resp.setUser(UserJson.fromDatastore(userCreated));
			resp.setStatus(HttpStatus.OK);
		}
		return resp;
	}

	/**
	 * 
	 * @param invit
	 * @return
	 */
	@POST("/invite")
	public HttpStatus inviteUser(Invitation invit) {

		User invitedUser = new User();
		invitedUser.setId(invit.getUserId());
		invitedUser.setEmail(invit.getUserId());
		invitedUser.setDisplayName(invit.getDisplayName());
		invitedUser.setRole(invit.getRole());
		invitedUser.setStatus("INVITED");
		invitedUser.setSignInDate(new Date());
		return saveOrUpdateUser(invitedUser);
	}

	/**
	 * Delete User By ID
	 * 
	 * @param id
	 * @return
	 */
	@DELETE("/id/{id}")
	public HttpStatus deleteUser(String id) {

		UserResponse resp = getUser(id);

		if (resp.getStatus().equals(HttpStatus.OK)) {
			if (!resp.getUser().getRole().equals("ADMIN")) {
				OfyService.ofy().delete().type(User.class).id(id).now();
				return HttpStatus.OK;
			} else {
				return HttpStatus.FORBIDDEN;
			}
		}
		return HttpStatus.NOT_FOUND;
	}
}