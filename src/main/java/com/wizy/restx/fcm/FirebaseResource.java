package com.wizy.restx.fcm;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;

import com.wizy.json.mdm.TaskJson;
import com.wizy.json.response.oneObject.TaskWithHttpStatus;
import com.wizy.json.response.twoObjects.MessageMultipleDevice;
import com.wizy.model.mdm.Message;
import com.wizy.model.mdm.device.Device;
import com.wizy.restx.mdm.DeviceResources;
import com.wizy.restx.mdm.TaskResources;
import com.wizy.security.SecurityAccess;

import restx.annotations.POST;
import restx.annotations.RestxResource;
import restx.factory.Component;
import restx.http.HttpStatus;
import restx.security.PermitAll;

@Component
@RestxResource("/fcm")
@PermitAll
public class FirebaseResource {

	final private String FCM_SERVER_KEY = SecurityAccess.FCM_SERVER_KEY;

	/**
	 * Decode a string with UTF-8
	 * 
	 * @param item
	 * @return
	 */
	public String decodeString(String item) {
		String decode_item = null;
		try {
			decode_item = URLDecoder.decode(item, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return decode_item;
	}

	/**
	 * Send Task by Token
	 * 
	 * @param token
	 */
	@POST("/device/{imei}/token/{token}/action/{action}")
	public HttpStatus sendTaskToOneDeviceByToken(String imei, String token, String action, Message message) {

		TaskResources taskResources = new TaskResources();
		String deviceToken = decodeString(token);
		String deviceImei = decodeString(imei);

		TaskWithHttpStatus taskDB = taskResources.addTaskToOneDevice(deviceImei, action, message);

		if (!taskDB.getStatus().equals(HttpStatus.OK)) {
			return taskDB.getStatus();
		}

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
		post.setHeader("Content-type", "application/json");
		// server key
		post.setHeader("Authorization", "key=" + FCM_SERVER_KEY);

		JSONObject msg = new JSONObject();

		// Device Token
		msg.put("to", deviceToken);
		msg.put("priority", "high");

		JSONObject taskPayload = new JSONObject();

		JSONObject task = new JSONObject();
		task.put("id", taskDB.getTask().getId());
		// task.put("deviceName", taskDB.getTask().getDeviceName());
		task.put("action", taskDB.getTask().getAction());
		task.put("status", taskDB.getTask().isStatus());
		taskPayload.put("task", task);

		if (taskDB.getTask().getAction().equals("message")) {
			// payload message
			JSONObject dataMessage = new JSONObject();
			dataMessage.put("title", message.getTitle());
			dataMessage.put("content", message.getContent());
			dataMessage.put("type", message.getType());
			taskPayload.put("message", dataMessage);

		}
		msg.put("data", taskPayload);
		post.setEntity(new StringEntity(msg.toString(), "UTF-8"));
		HttpResponse response = null;
		try {
			response = client.execute(post);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(response);
		System.out.println(msg);
		return HttpStatus.OK;
	}

	/**
	 * Send Task by List of registration_ids
	 * 
	 * @param token
	 */
	@POST("/devices/action/{action}")
	public HttpStatus sendTaskToMultiDevicesByRegistrationIds(String action, MessageMultipleDevice payload) {

		TaskResources taskResources = new TaskResources();
		List<String> ids = new ArrayList<>();

		Message message = payload.getMessage();
		com.wizy.json.response.twoObjects.MultipleTaskListResp taskDB = taskResources.addTaskToMultiDevicesByIds(action, payload);

		if (taskDB.getStatus().equals(HttpStatus.NOT_FOUND)) {
			return taskDB.getStatus();
		}

		for (TaskJson task : taskDB.getSuccess()) {
		//	ids.add(task.getDeviceToken());
		}
		ids.add("ec_1C0ETRyI:APA91bHSjqLi1ynE_-BRn2OExUeHrepp7TbA_rQAlZwL5LYrT8RQs_CbYDY3Yrfdkwrg4Z-f-lUH5KZ-0wYpThFLaAKZHPVS2yJJ17Y2AuLlIKJniI90pQ4tVc9amsMOIb3-zydStPa5");
		ids.add("ciPGcvpfa88:APA91bHe1OjzmAsz-hL_dtPpis-Ti_eyrZPWHT7hY5wDBFzM4Ev5T6DqrgzJikHqcw9OJDQuZmz71V_e7fP-jsLe6VLEKlnJK6SRoCp-TrPgG6C2Q12dQjdTlsig_n9NGqpAJHplYTX7");
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
		post.setHeader("Content-type", "application/json");
		// server key
		post.setHeader("Authorization", "key=" + FCM_SERVER_KEY);

		JSONObject msg = new JSONObject();

		// Device Token
		msg.put("registration_ids", ids);
		msg.put("priority", "high");

		JSONObject taskPayload = new JSONObject();

		JSONObject task = new JSONObject();
		task.put("id", taskDB.getSuccess().get(0).getId());
		task.put("action", taskDB.getSuccess().get(0).getAction());
		task.put("status", taskDB.getSuccess().get(0).isStatus());
		taskPayload.put("task", task);

		if (taskDB.getSuccess().get(0).getAction().equals("message")) {
			// payload message
			JSONObject dataMessage = new JSONObject();
			dataMessage.put("title", message.getTitle());
			dataMessage.put("content", message.getContent());
			dataMessage.put("type", message.getType());
			taskPayload.put("message", dataMessage);

		}
		msg.put("data", taskPayload);
		post.setEntity(new StringEntity(msg.toString(), "UTF-8"));
		HttpResponse response = null;
		try {
			response = client.execute(post);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(response);
		System.out.println(msg);
		return HttpStatus.OK;
	}
	
	/**
	 * Send Task by List of registration_ids
	 * 
	 * @param token
	 */
	@POST("/devices/filter/{name}/value/{value}/action/{action}")
	public HttpStatus sendTaskToAllDevicesByFilter( String name, String value, String action, Message message) {

		TaskResources taskResources = new TaskResources();
		DeviceResources deviceResources = new DeviceResources();
		
		List<String> ids = new ArrayList<>();
		List<String> imeiList =  new ArrayList<>();
		
		List<Device> deviceList = deviceResources.listDevicesByFilter(name, value);
		for(Device device: deviceList){
			imeiList.add(device.getImei());
		}
		

		
		com.wizy.json.response.twoObjects.MultipleTaskListResp taskDB = taskResources.addTaskToMultiDevicesByDevice(action, message, deviceList);

		if (taskDB.getStatus().equals(HttpStatus.NOT_FOUND)) {
			return taskDB.getStatus();
		}

		for (TaskJson task : taskDB.getSuccess()) {
		//	ids.add(task.getDeviceToken());
		}
		
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
		post.setHeader("Content-type", "application/json");
		// server key
		post.setHeader("Authorization", "key=" + FCM_SERVER_KEY);

		JSONObject msg = new JSONObject();

		// Device Token
		msg.put("registration_ids", ids);
		msg.put("priority", "high");

		JSONObject taskPayload = new JSONObject();

		JSONObject task = new JSONObject();
		task.put("id", taskDB.getSuccess().get(0).getId());
		task.put("action", taskDB.getSuccess().get(0).getAction());
		task.put("status", taskDB.getSuccess().get(0).isStatus());
		taskPayload.put("task", task);

		if (taskDB.getSuccess().get(0).getAction().equals("message")) {
			// payload message
			JSONObject dataMessage = new JSONObject();
			dataMessage.put("title", message.getTitle());
			dataMessage.put("content", message.getContent());
			dataMessage.put("type", message.getType());
			taskPayload.put("message", dataMessage);

		}
		msg.put("data", taskPayload);
		post.setEntity(new StringEntity(msg.toString(), "UTF-8"));
		HttpResponse response = null;
		try {
			response = client.execute(post);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(response);
		System.out.println(msg);
		return HttpStatus.OK;
	}

	/**
	 * Send Task by Topic
	 * 
	 * @param token
	 */
	@POST("/devices/topic/{topic}/action/{action}")
	public HttpStatus sendTaskToMultiDevicesByTopic(String topic, String action, Message message) {

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
		post.setHeader("Content-type", "application/json");
		// server key
		post.setHeader("Authorization", "key=" + FCM_SERVER_KEY);

		JSONObject msg = new JSONObject();

		// Device Token
		msg.put("to", "/topics/" + topic);
		msg.put("priority", "high");

		JSONObject taskPayload = new JSONObject();

		JSONObject task = new JSONObject();
		task.put("action", action);
		task.put("status", false);
		taskPayload.put("task", task);

		if (action.equals("message")) {
			// payload message
			JSONObject dataMessage = new JSONObject();
			dataMessage.put("title", message.getTitle());
			dataMessage.put("content", message.getContent());
			dataMessage.put("type", message.getType());
			taskPayload.put("message", dataMessage);

		}
		msg.put("data", taskPayload);
		post.setEntity(new StringEntity(msg.toString(), "UTF-8"));
		HttpResponse response = null;
		try {
			response = client.execute(post);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(response);
		System.out.println(msg);
		return HttpStatus.OK;
	}
}
