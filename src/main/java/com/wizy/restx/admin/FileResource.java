package com.wizy.restx.admin;

import restx.annotations.GET;
import restx.annotations.RestxResource;
import restx.factory.Component;
import restx.security.PermitAll;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.UUID;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.wizy.model.admin.Notify;
import com.wizy.ofy.OfyService;
import com.wizy.security.SecurityAccess;

@Component
@RestxResource("/file")
@PermitAll
public class FileResource {

	private static final String CGS_BUCKET = SecurityAccess.CGS_APP_LOG_BUCKET;
	private static final GcsService gcsService = GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());

	@GET("")
	public static void pushCreateFile() {
		// Build a task using the TaskOptions Builder pattern from ** above
		Queue queue = QueueFactory.getDefaultQueue();
		queue.add(TaskOptions.Builder.withUrl("/api/gcs/pull").method(TaskOptions.Method.GET));

	}

	@GET("/notify/{value}")
	public void notifyFile(boolean value) {
		String id = UUID.randomUUID().toString();
		Notify notif = new Notify();
		notif.setId(id);
		notif.setStatus(value);
		OfyService.ofy().save().entity(notif).now();
	}
	
	@GET("/count/{value}")
	public Integer countNotify (boolean value) {
		return OfyService.ofy().load().type(Notify.class).filter("status =",value).count();
	}

	@GET("/create")
	public static void CreateFile() {

		int number_of_lines = 3000;

		String body = "id,first_name,last_name,email,gender\n";
		StringBuilder builder = new StringBuilder();

		int i = 0;

		do {
			i++;
			builder.append(body);
		} while (i < number_of_lines);

		try {
			// prepare file to save or update into cloud storage
			GcsFilename filename = new GcsFilename(CGS_BUCKET, "TEST" + UUID.randomUUID().toString() + ".csv");
			GcsFileOptions options = new GcsFileOptions.Builder().mimeType("text/csv").acl("public-read").build();
			// save or update into cloud storage
			GcsOutputChannel writeChannel = gcsService.createOrReplace(filename, options);
			writeChannel.waitForOutstandingWrites();
			writeChannel.write(ByteBuffer.wrap(builder.toString().getBytes("UTF-8")));
			writeChannel.close();

		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}
}
