package com.wizy.restx.admin;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import com.google.api.client.http.InputStreamContent;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.model.ObjectAccessControl;
import com.google.api.services.storage.model.Objects;
import com.google.api.services.storage.model.StorageObject;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.wizy.restx.Utils.StorageFactory;
import com.wizy.security.SecurityAccess;

import restx.annotations.GET;
import restx.annotations.RestxResource;
import restx.factory.Component;
import restx.http.HttpStatus;
import restx.security.PermitAll;

@Component
@RestxResource("/gcs")
@PermitAll
public class CloudStorageResource {

	private static final String CGS_BUCKET = SecurityAccess.CGS_APP_LOG_BUCKET;
	private static final String DELIMITER = ",";

	/**
	 * This is the service from which all requests are initiated. The retry and
	 * exponential backoff settings are configured here.
	 */
	private final GcsService gcsService = GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());

	/**
	 * Stream To CloudStorage
	 * 
	 * @return
	 */
	@GET("/pull")
	public HttpStatus StreamToGCS() {

		final int fileSizeMax = 250000;
		PubSubResource pubSubResource = new PubSubResource();
		List<String> messageFromPS = new ArrayList<>(fileSizeMax);
		StringBuilder builder = new StringBuilder();
		builder.append("id,first_name,last_name,email,gender\n");
		
		
		long start = System.currentTimeMillis();
		long end = start + 10*1000; // N seconds * 1000 ms/sec
		while (System.currentTimeMillis() < end)
		{
			messageFromPS = pubSubResource.receiveMessagesWithoutACK(fileSizeMax);
			if (messageFromPS != null) {
				for (String message : messageFromPS) {
					builder.append(UUID.randomUUID().toString() + DELIMITER + message + DELIMITER + "" + DELIMITER + ""
							+ DELIMITER + "" + "\n");
					
				}
			} 
		}
		
		if (builder.toString().equals("id,first_name,last_name,email,gender\n")) {
			return HttpStatus.NOT_FOUND;
		}

		try {
			// prepare file to save or update into cloud storage
			GcsFilename filename = new GcsFilename(CGS_BUCKET, UUID.randomUUID().toString() + ".csv");
			GcsFileOptions options = new GcsFileOptions.Builder().mimeType("text/csv").acl("public-read").build();
			// save or update into cloud storage
			GcsOutputChannel writeChannel = gcsService.createOrReplace(filename, options);
			writeChannel.waitForOutstandingWrites();
			writeChannel.write(ByteBuffer.wrap(builder.toString().getBytes("UTF-8")));
			writeChannel.close();

		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return HttpStatus.OK;
	}

	// [START list_bucket]
	/**
	 * Fetch a list of the objects within the given bucket.
	 *
	 * @param bucketName
	 *            the name of the bucket to list.
	 * @return a list of the contents of the specified bucket.
	 */
	@GET("/bucket/{bucketName}/files")
	public static List<StorageObject> listBucket(String bucketName) {
		Storage client;
		List<StorageObject> results = new ArrayList<StorageObject>();
		try {
			client = StorageFactory.getService();
			Storage.Objects.List listRequest;
			listRequest = client.objects().list(bucketName);

			Objects objects;

			// Iterate through each page of results, and add them to our results
			// list.
			do {
				objects = listRequest.execute();
				// Add the items in this page of results to the list we'll
				// return.
				results.addAll(objects.getItems());

				// Get the next page, in the next iteration of this loop.
				listRequest.setPageToken(objects.getNextPageToken());
			} while (null != objects.getNextPageToken());
		} catch (IOException | GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return results;
	}

	// [END list_bucket]

	// [START upload_stream]
	/**
	 * Uploads data to an object in a bucket.
	 *
	 * @param name
	 *            the name of the destination object.
	 * @param contentType
	 *            the MIME type of the data.
	 * @param file
	 *            the file to upload. IOException, GeneralSecurityException
	 * @param bucketName
	 *            the name of the bucket to create the object in.
	 */
	public void uploadFile(String name, String contentType, InputStreamContent file, String bucketName) {
		try {

			// Setting the length improves upload performance
			// contentStream.setLength(file.getFD());
			StorageObject objectMetadata = new StorageObject()
					// Set the destination object name
					.setName(name)
					// Set the access control list to publicly read-only
					.setAcl(Arrays.asList(new ObjectAccessControl().setEntity("allUsers").setRole("READER")));

			// Do the insert
			Storage client = StorageFactory.getService();
			Storage.Objects.Insert insertRequest = client.objects().insert(bucketName, objectMetadata, file);

			insertRequest.execute();
		} catch (IOException | GeneralSecurityException e) {

		}
	}

	// [END upload_stream]

	// [START delete_object]
	/**
	 * Deletes an object in a bucket.
	 *
	 * @param path
	 *            the path to the object to delete.
	 * @param bucketName
	 *            the bucket the object is contained in.
	 */
	public static void deleteObject(String path, String bucketName) {
		try {
			Storage client = StorageFactory.getService();
			client.objects().delete(bucketName, path).execute();
		} catch (IOException | GeneralSecurityException e) {
			e.printStackTrace();
		}
	}

	// [END delete_object]
	/**
	 * copy object
	 * 
	 * @param object
	 */
	public void copyObject(String sourceBucket, String sourceObject, String destinationBucket, String destinationObject,
			StorageObject object) {
		try {
			Storage client = StorageFactory.getService();
			Storage.Objects.Copy request = client.objects().copy(sourceBucket, sourceObject, destinationBucket,
					destinationObject, object);
			request.execute();
		} catch (IOException | GeneralSecurityException e) {
			e.printStackTrace();
		}
	}
}
