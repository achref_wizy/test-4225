package com.wizy.restx.admin;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import restx.annotations.GET;
import restx.annotations.RestxResource;
import restx.factory.Component;
import restx.security.PermitAll;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.model.TableDataInsertAllRequest;
import com.google.api.services.bigquery.model.TableDataInsertAllRequest.Rows;
import com.google.api.services.bigquery.model.TableFieldSchema;
import com.wizy.restx.Utils.BigQueryUtils;
import com.wizy.security.SecurityAccess;
import com.google.api.services.bigquery.model.Job;
import com.google.api.services.bigquery.model.JobConfiguration;
import com.google.api.services.bigquery.model.JobConfigurationLoad;
import com.google.api.services.bigquery.model.TableReference;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.api.services.storage.model.StorageObject;

@Component
@RestxResource("/usage")
@PermitAll
public class BigqueryResource {

	// BIGQUERY CONFIG PARAM
	private static final String PROJECT_ID = SecurityAccess.PROJECT_ID;
	private static final String BIGQUERY_DATASET = SecurityAccess.BIGQUERY_DATASET;
	private static final String APP_TABLE = SecurityAccess.APP_TABLE;
	private static final String SOURCE_BUCKET = "mdm-log";
	private static final String DESTINATION_BUCKET = "mdm-log-processed";
	final String datasetId = "users";
	final String tableId = "users";
	final long interval = new Long(1);

	public String decodeString(String item) {
		String decode_item = null;
		try {
			decode_item = URLDecoder.decode(item, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return decode_item;
	}

	public Bigquery createBigQueryClient() throws IOException, GeneralSecurityException {

		HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();

		// Build service account credential.
		GoogleCredential credential = GoogleCredential.fromStream(new FileInputStream("WEB-INF/key.json"));
		System.out.println(credential);

		if (credential.createScopedRequired()) {
			credential = credential
					.createScoped(Collections.singletonList("https://www.googleapis.com/auth/cloud-platform"));
		}

		return new Bigquery.Builder(httpTransport, jsonFactory, credential).setApplicationName("WIZY_MDM").build();
	}

	/**
	 * Stream Data into Bigquery
	 */
	@GET("/all")
	public void StreamBigQueryAppLogV3() {

		List<Rows> rowList = new ArrayList<Rows>();
		PubSubResource pubSubResource = new PubSubResource();
		List<String> messageFromPS = new ArrayList<>(8000);

		do {

			messageFromPS = pubSubResource.receiveMessages(8000);
			if (messageFromPS != null) {

				for (String message : messageFromPS) {

					HashMap<String, Object> map = new HashMap<String, Object>();
					TableDataInsertAllRequest.Rows rows = new TableDataInsertAllRequest.Rows();
					map.put("id", UUID.randomUUID().toString());
					map.put("first_name", message);
					rows.setJson(map);
					rowList.add(rows);

				}
			}
		} while (messageFromPS.size() > 0 && rowList.size() < 10000);

		try {
			Bigquery bigqueryService = createBigQueryClient();
			TableDataInsertAllRequest content = new TableDataInsertAllRequest().setRows(rowList);
			bigqueryService.tabledata().insertAll(PROJECT_ID, BIGQUERY_DATASET, APP_TABLE, content).execute();

		} catch (IOException | GeneralSecurityException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Load Csv File from cloud storage
	 */
	@GET("/load")
	public void loadSingle() {

		Bigquery bigquery;
		CloudStorageResource cloudStorageResource = new CloudStorageResource();

		TableSchema schema = new TableSchema();
		List<TableFieldSchema> tableFieldSchema = new ArrayList<TableFieldSchema>();

		TableFieldSchema schemaEntry0 = new TableFieldSchema();
		schemaEntry0.setName("id");
		schemaEntry0.setType("STRING");
		TableFieldSchema schemaEntry1 = new TableFieldSchema();
		schemaEntry1.setName("first_name");
		schemaEntry1.setType("STRING");
		TableFieldSchema schemaEntry2 = new TableFieldSchema();
		schemaEntry2.setName("last_name");
		schemaEntry2.setType("STRING");
		TableFieldSchema schemaEntry3 = new TableFieldSchema();
		schemaEntry3.setName("email");
		schemaEntry3.setType("STRING");
		TableFieldSchema schemaEntry4 = new TableFieldSchema();
		schemaEntry4.setName("gender");
		schemaEntry4.setType("STRING");

		tableFieldSchema.add(schemaEntry0);
		tableFieldSchema.add(schemaEntry1);
		tableFieldSchema.add(schemaEntry2);
		tableFieldSchema.add(schemaEntry3);
		tableFieldSchema.add(schemaEntry4);
		schema.setFields(tableFieldSchema);

		List<StorageObject> objects = CloudStorageResource.listBucket(SOURCE_BUCKET);

		for (StorageObject object : objects) {

			String cloudStoragePath = "gs://" + SOURCE_BUCKET + "/" + object.getName();

			try {
				bigquery = createBigQueryClient();

				Job loadJob = loadJob(bigquery, cloudStoragePath,
						new TableReference().setProjectId(PROJECT_ID).setDatasetId(datasetId).setTableId(tableId),
						schema);

				Bigquery.Jobs.Get getJob = bigquery.jobs().get(loadJob.getJobReference().getProjectId(),
						loadJob.getJobReference().getJobId());

				BigQueryUtils.pollJob(getJob, interval);

				cloudStorageResource.copyObject(SOURCE_BUCKET, object.getName(), DESTINATION_BUCKET, object.getName(),
						object);
				
				CloudStorageResource.deleteObject(object.getName(), SOURCE_BUCKET);
				
			} catch (IOException | GeneralSecurityException | InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * A job that extracts data from a table.
	 * 
	 * @param bigquery
	 *            Bigquery service to use
	 * @param cloudStoragePath
	 *            Cloud storage bucket we are inserting into
	 * @param table
	 *            Table to extract from
	 * @param schema
	 *            The schema of the table we are loading into
	 * @return The job to extract data from the table
	 * @throws IOException
	 *             Thrown if error connceting to Bigtable
	 */
	// [START load_job]
	public static Job loadJob(final Bigquery bigquery, final String cloudStoragePath, final TableReference table,
			final TableSchema schema) throws IOException {

		JobConfigurationLoad load = new JobConfigurationLoad().setDestinationTable(table).setSchema(schema)
				.setSourceUris(Collections.singletonList(cloudStoragePath));

		return bigquery.jobs()
				.insert(table.getProjectId(), new Job().setConfiguration(new JobConfiguration().setLoad(load)))
				.execute();
	}
	// [END load_job]

}