package com.wizy.restx.admin;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpBackOffIOExceptionHandler;
import com.google.api.client.http.HttpBackOffUnsuccessfulResponseHandler;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpStatusCodes;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.HttpUnsuccessfulResponseHandler;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.client.util.Sleeper;
import com.google.api.services.pubsub.Pubsub;
import com.google.api.services.pubsub.model.AcknowledgeRequest;
import com.google.api.services.pubsub.model.PublishRequest;
import com.google.api.services.pubsub.model.PubsubMessage;
import com.google.api.services.pubsub.model.PullRequest;
import com.google.api.services.pubsub.model.PullResponse;
import com.google.api.services.pubsub.model.PushConfig;
import com.google.api.services.pubsub.model.ReceivedMessage;
import com.google.api.services.pubsub.model.Subscription;
import com.google.api.services.pubsub.model.Topic;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.wizy.security.SecurityAccess;

import restx.annotations.GET;
import restx.annotations.RestxResource;
import restx.factory.Component;
import restx.security.PermitAll;

@Component
@RestxResource("/pubsub")
@PermitAll
public class PubSubResource {

	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static final String PROJECT_ID = SecurityAccess.PROJECT_ID;
	private static final String TOPIC_NAME = SecurityAccess.TOPIC_NAME;
	private static final String SUBSCRIPTION_NAME = SecurityAccess.SUBSCRIPTION_NAME;

	class RetryHttpInitializerWrapper implements HttpRequestInitializer {

		private Logger logger = Logger.getLogger(RetryHttpInitializerWrapper.class.getName());

		// Intercepts the request for filling in the "Authorization"
		// header field, as well as recovering from certain unsuccessful
		// error codes wherein the Credential must refresh its token for a
		// retry.
		private final GoogleCredential wrappedCredential;

		// A sleeper; you can replace it with a mock in your test.
		private final Sleeper sleeper;

		public RetryHttpInitializerWrapper(GoogleCredential wrappedCredential) {
			this(wrappedCredential, Sleeper.DEFAULT);
		}

		// Use only for testing.
		RetryHttpInitializerWrapper(GoogleCredential wrappedCredential, Sleeper sleeper) {
			this.wrappedCredential = Preconditions.checkNotNull(wrappedCredential);
			this.sleeper = sleeper;
		}

		@Override
		public void initialize(HttpRequest request) {
			final HttpUnsuccessfulResponseHandler backoffHandler = new HttpBackOffUnsuccessfulResponseHandler(
					new ExponentialBackOff()).setSleeper(sleeper);
			request.setInterceptor(wrappedCredential);
			request.setUnsuccessfulResponseHandler(new HttpUnsuccessfulResponseHandler() {
				@Override
				public boolean handleResponse(HttpRequest request, HttpResponse response, boolean supportsRetry)
						throws IOException {
					if (wrappedCredential.handleResponse(request, response, supportsRetry)) {
						// If credential decides it can handle it, the
						// return code or message indicated something
						// specific to authentication, and no backoff is
						// desired.
						return true;
					} else if (backoffHandler.handleResponse(request, response, supportsRetry)) {
						// Otherwise, we defer to the judgement of our
						// internal backoff handler.
						logger.info("Retrying " + request.getUrl());
						return true;
					} else {
						return false;
					}
				}
			});
			request.setIOExceptionHandler(
					new HttpBackOffIOExceptionHandler(new ExponentialBackOff()).setSleeper(sleeper));
		}
	}

	/**
	 * Creates a Cloud Pub/Sub client.
	 */
	public Pubsub createPubsubClient() {
		HttpTransport transport = null;
		GoogleCredential credential = null;
		try {
			transport = GoogleNetHttpTransport.newTrustedTransport();
			credential = GoogleCredential.fromStream(new FileInputStream("WEB-INF/key.json"));
		} catch (GeneralSecurityException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (credential.createScopedRequired()) {
			credential = credential
					.createScoped(Collections.singletonList("https://www.googleapis.com/auth/cloud-platform"));
		}
		HttpRequestInitializer initializer = new RetryHttpInitializerWrapper(credential);
		return new Pubsub.Builder(transport, JSON_FACTORY, initializer).setApplicationName("WIZY_MDM").build();
	}

	/**
	 * Creates a Cloud Pub/Sub topic if it doesn't exist.
	 * 
	 * @param client
	 *            Pubsub client object.
	 * @throws IOException
	 *             when API calls to Cloud Pub/Sub fails.
	 */
	@GET("/topic")
	public void createTopic() {

		Pubsub client = createPubsubClient();
		String fullName = null;

		try {
			fullName = String.format("projects/%s/topics/%s", PROJECT_ID, TOPIC_NAME);
			client.projects().topics().get(fullName).execute();
		} catch (GoogleJsonResponseException e) {
			if (e.getStatusCode() == HttpStatusCodes.STATUS_CODE_NOT_FOUND) {
				// Create the topic if it doesn't exist
				try {
					client.projects().topics().create(fullName, new Topic()).execute();

				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creates a Cloud Pub/Sub subscription if it doesn't exist.
	 * 
	 * @param client
	 *            Pubsub client object.
	 * @throws IOException
	 *             when API calls to Cloud Pub/Sub fails.
	 */
	@GET("/subscription")
	public void createSubscription() {

		Pubsub client = createPubsubClient();
		String fullName = null;

		try {
			fullName = String.format("projects/%s/subscriptions/%s", PROJECT_ID, SUBSCRIPTION_NAME);
			client.projects().subscriptions().get(fullName).execute();
		} catch (GoogleJsonResponseException e) {
			if (e.getStatusCode() == HttpStatusCodes.STATUS_CODE_NOT_FOUND) {
				// Create the subscription if it doesn't exist
				String fullTopicName = String.format("projects/%s/topics/%s", PROJECT_ID, TOPIC_NAME);
				PushConfig pushConfig = new PushConfig();
				Subscription subscription = new Subscription().setTopic(fullTopicName).setPushConfig(pushConfig);
				try {
					client.projects().subscriptions().create(fullName, subscription).execute();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@GET("/publish")
	public void publishMessage() {

		Pubsub client = createPubsubClient();
		String message = "Bonjour" + UUID.randomUUID().toString();
		if (!"".equals(message)) {
			String fullTopicName = String.format("projects/%s/topics/%s", PROJECT_ID, TOPIC_NAME);
			PubsubMessage pubsubMessage = new PubsubMessage();
			try {
				pubsubMessage.encodeData(message.getBytes("UTF-8"));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PublishRequest publishRequest = new PublishRequest();
			publishRequest.setMessages(ImmutableList.of(pubsubMessage));

			try {
				client.projects().topics().publish(fullTopicName, publishRequest).execute();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@GET("/receive/{maxmessage}")
	public List<String> receiveMessages(int maxmessage) {
		Pubsub client = createPubsubClient();
		String subscriptionName = String.format("projects/%s/subscriptions/%s", PROJECT_ID, SUBSCRIPTION_NAME);
		PullRequest pullRequest = new PullRequest().setReturnImmediately(true).setMaxMessages(maxmessage);
		List<String> resp = new ArrayList<>(pullRequest.getMaxMessages());
		PullResponse pullResponse = null;
		try {
			pullResponse = client.projects().subscriptions().pull(subscriptionName, pullRequest).execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		List<String> ackIds = new ArrayList<>(pullRequest.getMaxMessages());
		List<ReceivedMessage> receivedMessages = pullResponse.getReceivedMessages();
		if (receivedMessages != null) {
			for (ReceivedMessage receivedMessage : receivedMessages) {
				PubsubMessage pubsubMessage = receivedMessage.getMessage();
				if (pubsubMessage != null && pubsubMessage.decodeData() != null) {
					try {
						resp.add(new String(pubsubMessage.decodeData(), "UTF-8"));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}
				ackIds.add(receivedMessage.getAckId());
			}
			AcknowledgeRequest ackRequest = new AcknowledgeRequest();
			ackRequest.setAckIds(ackIds);
			try {
				client.projects().subscriptions().acknowledge(subscriptionName, ackRequest).execute();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return resp;
	}
	
	@GET("/receive/noack/{maxmessage}")
	public List<String> receiveMessagesWithoutACK(int maxmessage) {
		Pubsub client = createPubsubClient();
		String subscriptionName = String.format("projects/%s/subscriptions/%s", PROJECT_ID, SUBSCRIPTION_NAME);
		PullRequest pullRequest = new PullRequest().setReturnImmediately(true).setMaxMessages(maxmessage);
		List<String> resp = new ArrayList<>(pullRequest.getMaxMessages());
		
		PullResponse pullResponse = null;
		try {
			pullResponse = client.projects().subscriptions().pull(subscriptionName, pullRequest).execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		List<ReceivedMessage> receivedMessages = pullResponse.getReceivedMessages();
		if (receivedMessages != null) {
			for (ReceivedMessage receivedMessage : receivedMessages) {
				PubsubMessage pubsubMessage = receivedMessage.getMessage();
				if (pubsubMessage != null && pubsubMessage.decodeData() != null) {
					try {
						resp.add(new String(pubsubMessage.decodeData(), "UTF-8"));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return resp;
	}

}
