package com.wizy.security;

import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.api.client.extensions.appengine.http.UrlFetchTransport;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.plus.Plus;
import com.google.api.services.plus.model.Person;
import com.wizy.model.admin.User;
import com.wizy.ofy.OfyService;

import restx.http.HttpStatus;

public abstract class MarketplaceUserService {
	private static final ThreadLocal<User> userThreadLocal = new ThreadLocal<>();
	private static final ThreadLocal<HttpSession> sessionThreadLocal = new ThreadLocal<>();
	private static final Logger log = Logger.getLogger(MarketplaceUserService.class.getName());

	private static final String USER_SESSION_PROPERTY = "email";
	private static final String STATE_SESSION_PROPERTY = "state";

	private static final String CLIENT_ID = SecurityAccess.CLIENT_ID;
	private static final String CLIENT_SECRET = SecurityAccess.CLIENT_SECRET;

	// This is the only scope that triggers the seamless SSO behavior
	private static final String AUTH_SCOPE = "email profile";
	private static final String USER_SCOPE ="https://www.googleapis.com/auth/admin.directory.user.readonly";
	private static final java.util.List<String> SCOPES = Arrays.asList(AUTH_SCOPE,USER_SCOPE);
	private static final String REDIRECT_SESSION_PROPERTY = "redirect";
	private static final String OAUTH_CALLBACK_URL = "oauth2callback";
	private static final String OAUTH_LOGIN_URL = "oauth2login";
	private static final HttpTransport transport = new UrlFetchTransport();
	private static final JsonFactory jsonFactory = new GsonFactory();

	public static User getCurrentUser() {
		return userThreadLocal.get();
	}

	public static HttpStatus logOut() {

		HttpSession session = sessionThreadLocal.get();
		if (!session.isNew()) {
			userThreadLocal.remove();
			sessionThreadLocal.remove();
			session.invalidate();
			return HttpStatus.OK;
		} else {
			return HttpStatus.NOT_FOUND;
		}

	}

	public static void clearSession(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession(false);
		userThreadLocal.remove();
		sessionThreadLocal.remove();
		if (session != null) {
			synchronized (session) {
				// invalidating a session destroys it
				session.invalidate();
			}
		}
		return;
	}

	protected static void redirectToInternalAuthenticationPage(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		storeRedirectInfoInSession(request);
		response.sendRedirect(getLoginUrl());
	}

	public static String getLoginUrl() {
		return "/" + OAUTH_LOGIN_URL;
	}

	public static void updateCurrentUser(User user) {
		userThreadLocal.set(user);
		if (sessionThreadLocal.get() != null) {
			storeUserInSession(user, sessionThreadLocal.get());
		}
	}

	protected static void redirectToGoogleAuthenticationPage(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		String state = UUID.randomUUID().toString();
		setOAuthStateInSession(request, state);
		String url = buildOAuthUrl(request, state);
		// String gotoParam = request.getParameter(REDIRECT_QUERY_PARAM);
		String gotoParam = request.getPathInfo();
		if (gotoParam != null) {
			if (gotoParam.startsWith("/")) {
				storeRedirectInfoInSession(request, "/#" + gotoParam);
			} else {
				log.warning("Requested a redirection to a URL not starting with /, will ignore for safety reasons.");
			}
		}
		response.sendRedirect(url);
	}

	private static String buildRedirectUri(HttpServletRequest request) {
		return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/"
				+ OAUTH_CALLBACK_URL;
	}

	protected static void validateOAuthResponseAndRedirect(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String state = getMandatoryParameter("state", req);
		String code = getMandatoryParameter("code", req);

		validateState(state, req);
		GoogleTokenResponse tokenResponse = getTokenResponseFromOauthCode(code, req);
		User userFromSession = getUserPrincipalFromOAuthCode(tokenResponse);
		User userFromDatastore = synchronizeUserWithDatastore(userFromSession, tokenResponse);
		if (userFromDatastore != null) {
			storeUserInSession(userFromDatastore, req.getSession(true));
			String redirectUrlFromSession = getRedirectUrlFromSession(req);
			resp.sendRedirect(redirectUrlFromSession == null ? "/" : redirectUrlFromSession);
		} else {
			resp.sendRedirect("/contactadmin");
		}
	}

	private static GoogleTokenResponse getTokenResponseFromOauthCode(String code, HttpServletRequest request)
			throws IOException {
		GoogleAuthorizationCodeFlow googleAuthorizationCodeFlow = getGoogleAuthorizationCodeFlow();
		GoogleTokenResponse tokenResponse = null;
		for (int i = 0; i < 5; i++) {
			try {
				tokenResponse = googleAuthorizationCodeFlow.newTokenRequest(code)
						.setRedirectUri(buildRedirectUri(request)).execute();
				break;
			} catch (Throwable t) {
				if (i == 4) {
					throw t;
				}
			}
		}
		return tokenResponse;
	}

	private static User synchronizeUserWithDatastore(User userFromSession, GoogleTokenResponse tokenResponse)
			throws IOException {

		User userFromDatastore = OfyService.ofy().load().type(User.class).id(userFromSession.getEmail()).now();
		Person person = null;
		for (int i = 0; i < 5; i++) {
			try {
				person = getPersonDataFromGPlus(tokenResponse);
				break;
			} catch (Throwable t) {
				if (i == 4) {
					throw t;
				}
			}
		}

		if (userFromDatastore == null) {
			log.info("The connection was denied because the user account" + userFromSession.getEmail()
					+ " is not authorized for login");

		} else {
			log.info("User " + userFromSession.getEmail()
					+ " was found in the datastore, getting data from G+ to update user");
			userFromDatastore.setDisplayName(
					person.getDisplayName() != null ? person.getDisplayName() : userFromDatastore.getDisplayName());
			userFromDatastore.setGivenName(
					person.getName() != null ? person.getName().getGivenName() : userFromDatastore.getGivenName());
			userFromDatastore.setFamilyName(
					person.getName() != null ? person.getName().getFamilyName() : userFromDatastore.getFamilyName());
			userFromDatastore.setImageUrl(
					person.getImage() != null ? person.getImage().getUrl() : userFromDatastore.getImageUrl());
			userFromDatastore.setProfil(userFromDatastore.getProfil() == null ? userFromSession.getProfil()
					: userFromDatastore.getProfil());
			userFromDatastore.setId(userFromSession.getEmail());
			userFromDatastore.setEmailVerified(userFromSession.isEmailVerified());
			OfyService.ofy().save().entity(userFromDatastore);
		}

		return userFromDatastore;
	}

	protected static void initForRequest(HttpServletRequest httpServletRequest) {
		User userPrincipal = (User) httpServletRequest.getSession(true).getAttribute(USER_SESSION_PROPERTY);
		sessionThreadLocal.set(httpServletRequest.getSession());
		userThreadLocal.set(userPrincipal);
	}

	private static void setOAuthStateInSession(HttpServletRequest request, String state) {
		request.getSession(true).setAttribute(STATE_SESSION_PROPERTY, state);
	}

	private static void storeRedirectInfoInSession(HttpServletRequest request) {
		storeRedirectInfoInSession(request,
				request.getRequestURL() + (request.getQueryString() != null ? "?" + request.getQueryString() : ""));
	}

	private static void storeRedirectInfoInSession(HttpServletRequest request, String url) {
		request.getSession(true).setAttribute(REDIRECT_SESSION_PROPERTY, url);
	}

	private static String buildOAuthUrl(HttpServletRequest request, String state) {
		GoogleAuthorizationCodeFlow flow = getGoogleAuthorizationCodeFlow();
		return flow.newAuthorizationUrl().setState(state).setRedirectUri(buildRedirectUri(request)).build();
	}

	private static GoogleAuthorizationCodeFlow getGoogleAuthorizationCodeFlow() {
		return new GoogleAuthorizationCodeFlow.Builder(transport, jsonFactory, CLIENT_ID, CLIENT_SECRET, SCOPES)
				.build();
	}

	/**
	 * Build and return an authorized Admin SDK Directory client service.
	 * 
	 * @return an authorized Directory client service
	 * @throws IOException
	 */
	private static Directory getDirectoryService(GoogleTokenResponse tokenResponse) throws IOException {
		return new Directory.Builder(transport, jsonFactory, new GoogleCredential().setFromTokenResponse(tokenResponse))
				.setApplicationName("wizy").build();
	}

	private static String getUsersProfil(GoogleTokenResponse tokenResponse) throws IOException {

		String profil = "USER-GLOBAL";
		Directory service = null;
		for (int i = 0; i < 5; i++) {
			try {
				service = getDirectoryService(tokenResponse);
				break;
			} catch (Throwable t) {
				if (i == 4) {
					throw t;
				}
			}
		}
		try {

			service.users().list().setCustomer("my_customer").setMaxResults(1).setViewType("admin_view").execute();
			profil = "ADMIN";

		} catch (GoogleJsonResponseException e) {
			if (e.getStatusCode() == 404) {
				profil = "USER-GLOBAL";
			} else {
				profil = "USER-APPS";
			}
			return profil;
		}
		return profil;
	}

	private static User getUserPrincipalFromOAuthCode(GoogleTokenResponse tokenResponse) throws IOException {

		GoogleIdToken.Payload payload = tokenResponse.parseIdToken().getPayload();
		String profil = getUsersProfil(tokenResponse);
		return new User.Builder().setId(payload.getEmail()).setEmail(payload.getEmail())
				.setEmailVerified(payload.getEmailVerified() == null ? false : payload.getEmailVerified())
				.setProfil(profil).createUser();
	}

	private static Person getPersonDataFromGPlus(GoogleTokenResponse tokenResponse) throws IOException {
		Plus plus = new Plus.Builder(transport, jsonFactory, new GoogleCredential().setFromTokenResponse(tokenResponse))
				.setApplicationName("wizy").build();
		return plus.people().get("me").execute();
	}

	private static void validateState(String state, HttpServletRequest req) {
		if (!state.equals(req.getSession().getAttribute(STATE_SESSION_PROPERTY))) {

			throw new IllegalArgumentException("The provided state does not match the one in session");
		}
	}

	private static void storeUserInSession(User userPrincipal, HttpSession session) {
		session.setAttribute(USER_SESSION_PROPERTY, userPrincipal);
	}

	private static String getRedirectUrlFromSession(HttpServletRequest req) {
		return (String) req.getSession(true).getAttribute(REDIRECT_SESSION_PROPERTY);
	}

	private static String getMandatoryParameter(String paramName, HttpServletRequest req) throws ServletException {
		String param = req.getParameter(paramName);
		if (param == null) {
			throw new ServletException("Unable to get the parameter " + paramName);
		}
		return param;
	}
}