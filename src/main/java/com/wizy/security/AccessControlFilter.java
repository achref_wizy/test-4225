package com.wizy.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class AccessControlFilter implements Filter {

	private static final String SECRET_TOKEN = SecurityAccess.SECRET_TOKEN;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// Nothing to do
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		String pwd = null;
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		pwd = httpRequest.getHeader("W-chronopost-token");

		if (pwd != null) {
			if (pwd.equals(SECRET_TOKEN)) {
				chain.doFilter(request, response);
			}
		}
	}

	@Override
	public void destroy() {
		// Nothing to do
	}
}