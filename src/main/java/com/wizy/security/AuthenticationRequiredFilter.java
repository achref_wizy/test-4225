package com.wizy.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class AuthenticationRequiredFilter implements Filter {
	private Gson gson = new Gson();

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// Nothing to do
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		String cron = null;
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		cron = httpRequest.getHeader("X-Appengine-Cron");

		if (cron != null) {

			if (cron.equals("true")) {
				chain.doFilter(request, response);
			}
		} else {

			if (MarketplaceUserService.getCurrentUser() == null) {

				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("redirectUrl", MarketplaceUserService.getLoginUrl());
				response.getWriter().write(gson.toJson(jsonObject));
				response.setContentType("application/json; charset=UTF8");
				((HttpServletResponse) response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			} else {

				chain.doFilter(request, response);
			}
		}
	}

	@Override
	public void destroy() {
		// Nothing to do
	}
}